using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GraphGame;
using GraphGame.Models;
using GraphGame.Views;

namespace GraphGame.Controllers
{
    public abstract class SessionControllerBase : IUpdateListener
    {
        protected bool consoleModeNoView;
        protected GameSession session;
        protected MapView mapView;
        protected string lastOpenedMapName = string.Empty;

        public SessionControllerBase(bool consoleModeNoView)
        {
            this.consoleModeNoView = consoleModeNoView;
        }

        public virtual void CreateSession(string mapName)
        {
            lastOpenedMapName = mapName;
        }

        public string GetLastOpenedMapName()
        {
            return lastOpenedMapName;
        }

        public virtual void RemoveSession()
        {
            if (!consoleModeNoView && mapView != null)
                mapView.DestroyMap();

            session = null;
        }

        public bool IsSessionCreated()
        {
            return session != null;
        }

        public virtual void SaveMap(string fileName)
        {
            var map = session.GetMap();
            MapSaveLoadHelper.Save(map, fileName);
            lastOpenedMapName = fileName;
        }

        public virtual void Update(float elapsedSec)
        {
            if (!IsSessionCreated())
                return;

            session.Update(elapsedSec);

            //int fps = (int)(1.0f / elapsedSec);
            //if (fps < 40)
            //Debug.LogWarning("FPS:  " + fps);
        }
    }
}