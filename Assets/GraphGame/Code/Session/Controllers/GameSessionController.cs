using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using GraphGame.Models;
using GraphGame.Views;

namespace GraphGame.Controllers
{
    public class GameSessionController : SessionControllerBase
    {
        public int CurrentArmyMoveOrderPercentage = Constants.FirstArmyMovePercentage;

        private Player localPlayer;
        private Node moveCommandStartNode;
        private bool disableLocalPlayer;
        private Node autoSendingUnitsStart, autoSendingUnitsFinish;

        private bool campaignSession = false;
        private TextAsset lastLoadedMapAsset;

        private static GameSessionController instance;

        public GameSessionController(bool consoleModeNoView = false, bool disableLocalPlayer = false)
            : base(consoleModeNoView)
        {
            instance = this;
            this.disableLocalPlayer = disableLocalPlayer;
        }

        public static GameSessionController GetInstance()
        {
            return instance;
        }

        //===Session creation
        public void RestartSession()
        {
            if (campaignSession)
                CampaignController.GetInstance().RunMissionWithThisMap(lastLoadedMapAsset);
            else if (lastOpenedMapName != string.Empty)
                CreateSession(lastOpenedMapName);
            else
                Debug.LogError("Cant restart session!");
        }

        public void CreateCampaignSession(TextAsset mapAsset)
        {
            var map = MapSaveLoadHelper.Load(mapAsset);
            lastLoadedMapAsset = mapAsset;
            createGameSession(map);
            campaignSession = true;
            base.CreateSession(string.Empty);
        }

        public void CreateSession(GameMap map)
        {
            createGameSession(map);
            campaignSession = false;
            base.CreateSession(string.Empty);
        }

        public override void CreateSession(string mapName)
        {
            var map = MapSaveLoadHelper.Load(mapName);
            createGameSession(map);
            campaignSession = false;
            base.CreateSession(mapName);
        }

        private void createGameSession(GameMap map)
        {
            if (IsSessionCreated())
                RemoveSession();

            session = new GameSession();

            var players = generatePlayers(map);
            session.Init(map, players);
            if (localPlayer != null && players.Count > 1)
                map.VictoryConditions.StartChecking(localPlayer, map, delegate { onPlayerWon(); }, delegate { onPlayerLost(); });
            session.SetGameState(GameState.Running);

            foreach (var point in map.ArmyLocationPoints)
                point.OnSessionStarted();

            //gui
            if (!consoleModeNoView)
            {
                mapView = new MapView(map);

                foreach (var nodePoint in map.ArmyLocationPoints.Where(x => x is Node))
                {
                    (nodePoint as Node).OnOwnerChanged += delegate (Node node, Player oldOwner, Player newOwner)
                    {
                        if (UIController.GetInstance().GamePanel.HUDMenu.IsHUDShownFor(node))
                            ShowHUDForNode(node);
                    };
                }

                if (localPlayer != null)
                    localPlayer.AddListener(UIController.GetInstance().GamePanel.PowerUpsMenu);

                UIController.GetInstance().GamePanel.Show(true);
            }
        }

        private List<Player> generatePlayers(GameMap map)
        {
            List<Player> players = new List<Player>();

            for (int i = 0; i < map.StartLocations.Count; ++i)
            {
                if (!disableLocalPlayer && i == 0)
                {
                    localPlayer = new Player(Constants.LocalPlayerName, UISettings.GetInstance().LocalPlayerColor);
                    players.Add(localPlayer);
                }
                else
                {
                    Color color = new Color();
                    if (Constants.GetInstance().AiSettings.AIColors.Count - 1 > i)
                        color = Constants.GetInstance().AiSettings.AIColors[i];
                    else
                        color = new Color(UnityEngine.Random.Range(0.0f, 1.0f), UnityEngine.Random.Range(0.0f, 1.0f),
                            UnityEngine.Random.Range(0.0f, 1.0f));

                    var AIPlayer = new AIPlayer("AI", color, map, map.StartLocations[i].AIBehaviour,
                        map.StartLocations[i].AIDifficulty);
                    players.Add(AIPlayer);
                }
            }

            return players;
        } 

        //=== end game
        private void onPlayerLost()
        {
            RemoveSession();

            UIController.GetInstance().PlayerLostPanel.Show(false);
        }

        private void onPlayerWon()
        {
            if (campaignSession)
                CampaignController.GetInstance().CompleteMap(lastLoadedMapAsset);
            else
                UIController.GetInstance().PlayerWonPanel.SetUpAndShow(false);

            RemoveSession();
        }
        

        //===Session states
        public bool IsPaused()
        {
            return session.GetGameState() == GameState.Paused;
        }
        public void Pause()
        {
            session.SetGameState(GameState.Paused);
        }
        public void UnPause()
        {
            session.SetGameState(GameState.Running);
        }

        /*
        //Players
        public Player GetLocalPlayer()
        {
            if (IsSessionCreated())
                return localPlayer;

            return null;
        }
        */

        //===Move commands
        public bool TryStartCreatingMoveCommandFrom(Node node)
        {
            if (!node.HasOwner() || node.GetOwner() != localPlayer)
                return false;

            moveCommandStartNode = node;
            return true;
        }
        public void FinishCreatingMoveCommandTo(Node node)
        {
            if (moveCommandStartNode == null || node == moveCommandStartNode)
                return;

            moveCommandStartNode.OrderArmyToMove(node, CurrentArmyMoveOrderPercentage);
        }
        public bool IsMoveCommandCreationInProgress()
        {
            return moveCommandStartNode != null;
        }
        public void CancelMoveCommandCreation()
        {
            moveCommandStartNode = null;
        }

        //===HUD
        public void ShowHUDForNode(Node node)
        {
            List<HUDMenu.NodeHudOption> HUDOptions = new List<HUDMenu.NodeHudOption>();

            //enemy end neutoral
            if (!node.HasOwner() || (node.HasOwner() && node.GetOwner() != localPlayer))
            {
                if (localPlayer.GetPowerUps().ContainsKey(PowerUpType.Reset))
                {
                    HUDMenu.NodeHudOption option = new HUDMenu.NodeHudOption(!node.IsPowerUpApplied(), NodeGameAction.UseResetPowerUp, HUDMenu.HudButtonPlace.Left,
                            delegate { localPlayer.UsePowerUp(PowerUpType.Reset, node, session.GetMap().PowerUpsDurationSec); });
                    HUDOptions.Add(option);
                }
            }

            if (node.IsPowerUpApplied())
            {
                if (localPlayer.GetPowerUps().ContainsKey(PowerUpType.RemovePowerUps))
                {
                    HUDMenu.NodeHudOption option = new HUDMenu.NodeHudOption(true, NodeGameAction.UseRemovePowerUpsPowerUp, HUDMenu.HudButtonPlace.Bottom,
                            delegate { localPlayer.UsePowerUp(PowerUpType.RemovePowerUps, node, session.GetMap().PowerUpsDurationSec); });
                    HUDOptions.Add(option);
                }
            }

            if (!node.HasOwner())
            {
                UIController.GetInstance().GamePanel.HUDMenu.ShowHUDFor(node, HUDOptions.ToArray());
                return;
            }

            //player nodes power ups
            if (node.GetOwner() == localPlayer)
            {
                //Waypoint options
                var waypointOption = getWaypointOption(node);
                HUDOptions.Add(waypointOption);
                
                //Power Ups
                foreach (var powerUpPair in localPlayer.GetPowerUps())
                {
                    PowerUpType powerUp = powerUpPair.Key;
                    switch (powerUp)
                    {
                        case PowerUpType.Fortify:
                        {
                            HUDMenu.NodeHudOption option = new HUDMenu.NodeHudOption(!node.IsPowerUpApplied(),
                                NodeGameAction.UseFortifyPowerUp, HUDMenu.HudButtonPlace.Left,
                                delegate { localPlayer.UsePowerUp(PowerUpType.Fortify, node, session.GetMap().PowerUpsDurationSec); });
                            HUDOptions.Add(option);
                            break;
                        }
                        case PowerUpType.LockOwner:
                        {
                            if (!(node is FactoryNode))
                                break;
                            HUDMenu.NodeHudOption option = new HUDMenu.NodeHudOption(!node.IsPowerUpApplied(),
                                NodeGameAction.UseLockOwnerPowerUp, HUDMenu.HudButtonPlace.Right,
                                delegate { localPlayer.UsePowerUp(PowerUpType.LockOwner, node, session.GetMap().PowerUpsDurationSec); });
                            HUDOptions.Add(option);
                            break;
                        }
                    }
                }
            }
            //enemy nodes power ups
            else
            {
                foreach (var powerUpPair in localPlayer.GetPowerUps())
                {
                    PowerUpType powerUp = powerUpPair.Key;
                    switch (powerUp)
                    {
                        case PowerUpType.StopProduction:
                        {
                            if (!(node is FactoryNode))
                                break;
                            HUDMenu.NodeHudOption option = new HUDMenu.NodeHudOption(!node.IsPowerUpApplied(),
                                NodeGameAction.UseStopProductionPowerUp, HUDMenu.HudButtonPlace.Top,
                                delegate { localPlayer.UsePowerUp(PowerUpType.StopProduction, node, session.GetMap().PowerUpsDurationSec); });
                            HUDOptions.Add(option);
                            break;
                        }
                    }
                }
            }

            UIController.GetInstance().GamePanel.HUDMenu.ShowHUDFor(node, HUDOptions.ToArray());
        }

        private HUDMenu.NodeHudOption getWaypointOption(Node node)
        {
            if (node.GetWaypoint() == null)
            {
                var setWaypointHudOption = new HUDMenu.NodeHudOption(true, NodeGameAction.SetWaypoint, HUDMenu.HudButtonPlace.Top,
                    delegate
                    {
                        SetAutoSendingUnitsStart(node);

                        //show panel with cancellation option and note
                        UIController.GetInstance().SmallMessageWindow.SetupAndShow(UISettings.GetInstance().UiTexts.WaypointActionPopupMessage,
                            UISettings.GetInstance().UiTexts.CancelWaypointCreationText,
                            delegate
                            {
                                CancelAutoSendingUnitsCommand();
                                UIController.GetInstance().SmallMessageWindow.Hide();
                            });
                    });

                return setWaypointHudOption;
            }
            else
            {
                var removeWaypointHudOption = new HUDMenu.NodeHudOption(true, NodeGameAction.RemoveWaypoint, HUDMenu.HudButtonPlace.Top,
                    delegate
                    {
                        node.RemoveWaypoint();
                    });

                return removeWaypointHudOption;
            }
        }
        public bool IsAutoSendingUnitsStartSettedUp()
        {
            return autoSendingUnitsStart != null;
        }
        public void SetAutoSendingUnitsStart(Node node)
        {
            autoSendingUnitsStart = node;
        }
        public void SetAutoSendingUnitsFinish(Node node)
        {
            autoSendingUnitsFinish = node;
        }
        public void ApplyAutoSendingUnitsCommand()
        {
            if (autoSendingUnitsStart == null || autoSendingUnitsFinish == null)
            {
                throw new Exception("Start or finish is not set!");
            }

            if (autoSendingUnitsFinish != autoSendingUnitsStart)
                autoSendingUnitsStart.SetWaypoint(autoSendingUnitsFinish);

            UIController.GetInstance().SmallMessageWindow.Hide();

            CancelAutoSendingUnitsCommand();
        }
        public void CancelAutoSendingUnitsCommand()
        {
            autoSendingUnitsStart = autoSendingUnitsFinish = null;
        }
        public void HideHUDForNode()
        {
            UIController.GetInstance().GamePanel.HUDMenu.HideHud();
        }
    }

    public enum NodeGameAction
    {
        NotSpecified,

        SetWaypoint, RemoveWaypoint,

        UseFortifyPowerUp, UseStopProductionPowerUp, UseResetPowerUp, UseLockOwnerPowerUp, UseRemovePowerUpsPowerUp
    }
}