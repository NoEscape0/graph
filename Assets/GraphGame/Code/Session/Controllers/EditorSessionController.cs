using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using GraphGame.Models;
using GraphGame.Views;

namespace GraphGame.Controllers
{
    public class EditorSessionController : SessionControllerBase
    {
        public OnSelectedNodesModifiedDelegate OnSelectedNodesModified;
        public NoVariablesDelegate OnNewSessionCreated;

        private List<Node> selectedNodes = new List<Node>();
        private EditorMapView editorMapView;

        private bool snapToGrid = true;

        private static EditorSessionController instance;

        public EditorSessionController() : base(false)
        {
            instance = this;
        }

        public static EditorSessionController GetInstance()
        {
            return instance;
        }

        public void CreateSession()
        {
            createEditorSession(new GameMap());
            base.CreateSession(string.Empty);
        }

        public override void CreateSession(string mapName)
        {
            var map = MapSaveLoadHelper.Load(mapName);
            createEditorSession(map);
            base.CreateSession(mapName);
        }

        private void createEditorSession(GameMap map)
        {
            if (IsSessionCreated())
                RemoveSession();

            //create session
            session = new GameSession();
            List<Player> players = new List<Player>();

            session.Init(map, players, false);
            session.SetGameState(GameState.Running);

            if (!consoleModeNoView)
            {
                //gui
                mapView = editorMapView = new EditorMapView(map);
            }

            selectedNodesModified();

            if (OnNewSessionCreated != null)
                OnNewSessionCreated();
        }

        public override void RemoveSession()
        {
            DeselectAll();
            editorMapView.DestroyMap();
        }

        public void AddArmyLocationPoint(ArmyLocationPointBase point)
        {
            session.GetMap().ArmyLocationPoints.Add(point);

            editorMapView.RebuildView();
        }

        //====Nodes Selection and removal
        public void SelectNode(Node node)
        {
            if (!selectedNodes.Contains(node))
            {
                selectedNodes.Add(node);


                editorMapView.DrawSelection(selectedNodes);

                selectedNodesModified();
            }
        }
        public bool IsNodeSelected(Node node)
        {
            return selectedNodes.Contains(node);
        }
        public void DeselectNode(Node node)
        {
            if (!selectedNodes.Contains(node))
                return;

            selectedNodes.Remove(node);
            editorMapView.DrawSelection(selectedNodes);

            selectedNodesModified();
        }
        public void DeselectAll()
        {
            selectedNodes.Clear();
            editorMapView.RemoveSelectionBoxes();

            selectedNodesModified();
        }
        public void RemoveSelectedNodes()
        {
            var map = session.GetMap();

            //remove all links
            foreach (var node in selectedNodes)
            {
                var links = node.GetLinks();
                foreach (var link in links)
                {
                    if (map.ArmyLocationPoints.Contains(link))
                        map.ArmyLocationPoints.Remove(link);
                }
            }

            //remove nodes
            foreach (var node in selectedNodes)
                map.ArmyLocationPoints.Remove(node);

            selectedNodes.Clear();
            editorMapView.RemoveSelectionBoxes();
            editorMapView.RebuildView();

            selectedNodesModified();
        }
        public void ConnectSelectedNodes()
        {
            setConnectivityBetweenSelectedNodes(connect: true);
        }
        public void DisconnectSelectedNodes()
        {
            setConnectivityBetweenSelectedNodes(connect: false);
        }
        private void setConnectivityBetweenSelectedNodes(bool connect)
        {
            var map = session.GetMap();
            List<Link> createdLinksThisRun = new List<Link>();
            List<Link> linksToChangeCapacity = new List<Link>();

            //connect every node with every node
            for (int i = 0; i < selectedNodes.Count; ++i)
            {
                var node = selectedNodes[i];

                for (int i2 = 0; i2 < selectedNodes.Count; ++i2)
                {
                    var node2 = selectedNodes[i2];
                    if (node2 == node)
                        continue;

                    if (connect)
                    {
                        if (!node.IsConnectedTo(node2))
                        {
                            node.ConnectTo(node2);
                            var link = node.GetLinkWith(node2);
                            map.ArmyLocationPoints.Add(link);
                            createdLinksThisRun.Add(link);
                        }
                        else
                        {
                            var link = node.GetLinkWith(node2);
                            if (!linksToChangeCapacity.Contains(link) && !createdLinksThisRun.Contains(link))
                                linksToChangeCapacity.Add(link);
                        }
                    }
                    else if (node.IsConnectedTo(node2))
                    {
                        var link = node.GetLinkWith(node2);

                        if (link.GetLinkPiecesCapcity() == LinkPieceCapacity.Normal)
                        {
                            map.ArmyLocationPoints.Remove(link);
                            node.DisconnectFrom(node2);
                        }
                        else
                        {
                            //-1 link capacity
                            if (!linksToChangeCapacity.Contains(link))
                                linksToChangeCapacity.Add(link);
                        }
                    }
                }
            }


            if (linksToChangeCapacity.Count > 0)
            {
                foreach (var link in linksToChangeCapacity)
                {
                    if (connect && link.GetLinkPiecesCapcity() != LinkPieceCapacity.Tripple)
                        link.SetLinkPieceCapacity((LinkPieceCapacity)((int)link.GetLinkPiecesCapcity() + 1));
                    else if (!connect)
                        link.SetLinkPieceCapacity((LinkPieceCapacity)((int)link.GetLinkPiecesCapcity() - 1));
                }
            }
            
            editorMapView.RebuildView();
        }
        public void TranslateSelectedNodes(Vector2 amount)
        {
            foreach (var node in selectedNodes)
                node.SetCoordinate(node.GetCoordinate() + amount);

            List<Link> links = new List<Link>();
            foreach (var node in selectedNodes)
                foreach (var link in node.GetLinks())
                    if (!links.Contains(link))
                        links.Add(link);

            foreach (var link in links)
                link.UpdateLinkLenght();

            editorMapView.RebuildView();
            editorMapView.DrawSelection(selectedNodes);
        }

        //====Snap
        public void SnapSelectedNodesToGrid()
        {
            if (!IsSnapToGridEnabled())
                return;

            foreach (var node in selectedNodes)
            {
                var newCoordinate =
                    new Vector2(Mathf.Round(node.GetCoordinate().x/(float) Constants.GridStep)*Constants.GridStep,
                        Mathf.Round(node.GetCoordinate().y/(float) Constants.GridStep)*Constants.GridStep);

                node.SetCoordinate(newCoordinate);
            }

            editorMapView.RebuildView();
            editorMapView.DrawSelection(selectedNodes);
        }
        public bool IsSnapToGridEnabled()
        {
            return snapToGrid;
        }
        public void SetSnapToGrid(bool active)
        {
            snapToGrid = active;
        }

        //====Start locations 
        public void CreateNewStartLocationGroup()
        {
            session.GetMap().StartLocations.Add(new StartLocationsSet());
        }

        public void RemoveSelectedLocationGroup(int selectedGroupIndex)
        {
            if (selectedGroupIndex < session.GetMap().StartLocations.Count)
            {
                session.GetMap().StartLocations.RemoveAt(selectedGroupIndex);
                mapView.RebuildView();

                selectedNodesModified();
            }
        }

        public void AddSelectedFactoryNodeToLocationGroup(int selectedGroupIndex)
        {
            if (selectedGroupIndex >= session.GetMap().StartLocations.Count)
                return;
            var locationFactories = session.GetMap().StartLocations[selectedGroupIndex].FactoryNodes;
            RemoveSelectedFactoryNodeFromLocationGroup();

            foreach (var node in selectedNodes)
            {
                if (!(node is FactoryNode))
                    continue;

                locationFactories.Add(node as FactoryNode);
            }

            selectedNodesModified();

            mapView.RebuildView();
        }

        public AIDifficulty GetSelectedLocationGroupAIDifficulty(int selectedGroupIndex)
        {
            if (selectedGroupIndex < session.GetMap().StartLocations.Count)
                return session.GetMap().StartLocations[selectedGroupIndex].AIDifficulty;

            return 0;
        }

        public AIBehaviourPattern GetSelectedLocationGroupAIBehaviour(int selectedGroupIndex)
        {
            if (selectedGroupIndex < session.GetMap().StartLocations.Count)
                return session.GetMap().StartLocations[selectedGroupIndex].AIBehaviour;

            return 0;
        }

        public void SetSelectedLocationGroupAIDifficulty(int selectedGroupIndex, AIDifficulty newDifficulty)
        {
            if (selectedGroupIndex < session.GetMap().StartLocations.Count)
                session.GetMap().StartLocations[selectedGroupIndex].AIDifficulty = newDifficulty;
        }

        public void SetSelectedLocationGroupAIBehaviour(int selectedGroupIndex, AIBehaviourPattern newBehaviour)
        {
            if (selectedGroupIndex < session.GetMap().StartLocations.Count)
                session.GetMap().StartLocations[selectedGroupIndex].AIBehaviour = newBehaviour;
        }

        public void RemoveSelectedFactoryNodeFromLocationGroup()
        {
            var startLocations = session.GetMap().StartLocations;
            foreach (var node in selectedNodes)
            {
                if (!(node is FactoryNode))
                    continue;

                FactoryNode factory = (FactoryNode) node;
                foreach (var location in startLocations)
                {
                    if (location.FactoryNodes.Contains(factory))
                        location.FactoryNodes.Remove(factory);
                }
            }

            mapView.RebuildView();
        }

        public bool IsFactoryNodeInAStartLocationGroup(FactoryNode factory, out int startLocationGroupIndex)
        {
            var startLocations = session.GetMap().StartLocations;
            for (int i = 0; i < startLocations.Count; ++i)
            {
                var location = startLocations[i];
                if (location.FactoryNodes.Contains(factory))
                {
                    startLocationGroupIndex = i;
                    return true;
                }
            }

            startLocationGroupIndex = -1;
            return false;
        }

        public int GetStartLocationsCount()
        {
            return session.GetMap().StartLocations.Count;
        }

        //====Power Ups
        public void SetPowerUpOfSelectedPowerUpNodes(PowerUpType powerUpType)
        {
            foreach (var selectedNode in selectedNodes)
            {
                if (!(selectedNode is PowerUpNode))
                    continue;

                (selectedNode as PowerUpNode).SetPowerUpType(powerUpType);
            }

            selectedNodesModified();
        }

        public int GetPowerUpsDuration()
        {
            return session.GetMap().PowerUpsDurationSec;
        }
        public void SetPowerUpsDuration(int newDuration)
        {
            if (newDuration >= 0)
                session.GetMap().PowerUpsDurationSec = newDuration;
        }

        //=== start army amount
        public void SetStartGenerateRangeOfSelectedNodes(int min, int max)
        {
            foreach (var selectedNode in selectedNodes)
                selectedNode.SetStartUnitsGenerationRange(min, max);

            selectedNodesModified();
        }

        private void selectedNodesModified()
        {
            if (OnSelectedNodesModified != null)
                OnSelectedNodesModified(selectedNodes);
        }
    }
}