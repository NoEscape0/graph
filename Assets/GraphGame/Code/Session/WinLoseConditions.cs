using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace GraphGame.Models
{
    [Serializable]
    public class WinLoseConditions : IUpdateListener
    {
        private float conditionsCheckRateSec = 3;
        private ConditionsType conditionsType;


        [NonSerialized]
        private bool checkingStarted = false;
        [NonSerialized]
        private float totalElapsedSec = 0;
        [NonSerialized]
        private Player targetPlayer;
        [NonSerialized]
        private GameMap targetMap;
        [NonSerialized]
        private NoVariablesDelegate onPlayerWon;
        [NonSerialized]
        private NoVariablesDelegate onPlayerLost;

        private WinLoseConditions()
        {
        }

        public static WinLoseConditions CreateConditions_EluminateAllEnemyPlayers()
        {
            WinLoseConditions conditions = new WinLoseConditions();
            conditions.conditionsType = ConditionsType.EluminateAllOtherPlayers;
            return conditions;
        }

        public void StartChecking(Player targetPlayer, GameMap map, NoVariablesDelegate OnPlayerWon, NoVariablesDelegate OnPlayerLost)
        {
            this.targetMap = map;
            this.targetPlayer = targetPlayer;
            onPlayerWon = OnPlayerWon;
            onPlayerLost = OnPlayerLost;
            checkingStarted = true;
        }

        public void Update(float elapsedSec)
        {
            if (!checkingStarted)
                return;

            totalElapsedSec += elapsedSec;
            if (totalElapsedSec < conditionsCheckRateSec)
                return;
            totalElapsedSec -= conditionsCheckRateSec;

            switch (conditionsType)
            {
                case ConditionsType.EluminateAllOtherPlayers:
                {
                    bool playerWon = targetMap.GetCashedResources().FactoryNodes().Count(x => x.HasOwner() && x.GetOwner() != targetPlayer) == 0;
                    if (playerWon)
                    {
                        onPlayerWon();
                        checkingStarted = false;
                    }
                    else
                    {
                        bool playerLost = targetMap.GetCashedResources().FactoryNodes().Count(x => x.HasOwner() && x.GetOwner() == targetPlayer) == 0;
                        if (playerLost)
                        {
                            onPlayerLost();
                            checkingStarted = false;
                        }
                    }

                    break;
                }
            }
        }

        public void FixedUpdate(float elapsedSec)
        {
            
        }

        private enum ConditionsType
        {
            EluminateAllOtherPlayers
        }
    }
}