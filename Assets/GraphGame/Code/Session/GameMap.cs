using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace GraphGame.Models
{
    [Serializable]
    public class GameMap
    {
        public List<ArmyLocationPointBase> ArmyLocationPoints;
        public List<StartLocationsSet> StartLocations;
        public int PowerUpsDurationSec = 0; //0 is unlimited
        public WinLoseConditions VictoryConditions;

        private List<linkSerializationInfo> linksSerializationInfos;
        [NonSerialized]
        private MapCashedResources mapCashedResources;

        public GameMap()
        {
            ArmyLocationPoints = new List<ArmyLocationPointBase>();
            StartLocations = new List<StartLocationsSet>();
            VictoryConditions = WinLoseConditions.CreateConditions_EluminateAllEnemyPlayers();
        }

        public void CashResources()
        {
            mapCashedResources = new MapCashedResources(this);
        }

        public MapCashedResources GetCashedResources()
        {
            return mapCashedResources;
        }

        [OnSerializing]
        private void OnSerializingStarted(StreamingContext context)
        {
            linksSerializationInfos = new List<linkSerializationInfo>();

            var links = ArmyLocationPoints.Where(x => x is Link).ToList();
            foreach (var linkPoint in links)
            {
                Link link = linkPoint as Link;
                linkSerializationInfo linkInfo = new linkSerializationInfo
                {
                    linkIndex = ArmyLocationPoints.FindIndex(x => x == link),
                    firstNodeIndex = ArmyLocationPoints.FindIndex(x => x == link.Connections[0]),
                    secondNodeIndex = ArmyLocationPoints.FindIndex(x => x == link.Connections[1])
                };

                linksSerializationInfos.Add(linkInfo);
            }
        }

        [OnDeserialized]
        private void OnDeserialized(object o)
        {
            //restore connections
            foreach (linkSerializationInfo linkInfo in linksSerializationInfos)
            {
                var link = ArmyLocationPoints[linkInfo.linkIndex] as Link;
                var firstNode = ArmyLocationPoints[linkInfo.firstNodeIndex] as Node;
                var secondNode = ArmyLocationPoints[linkInfo.secondNodeIndex] as Node;

                link.Connections = new Node[2];
                link.Connections[0] = firstNode;
                link.Connections[1] = secondNode;

                firstNode.ConnectTo(secondNode, link);
            }
        }


        public void ClaimStartLocation(int slotNumber, FactoryNode factory)
        {
            if (!ArmyLocationPoints.Contains(factory))
                return;

            if (slotNumber >= StartLocations.Count)
            {
                //add new team
                var newLocSet = new StartLocationsSet();
                newLocSet.FactoryNodes = new List<FactoryNode>();
                newLocSet.FactoryNodes.Add(factory);
                StartLocations.Add(newLocSet);
            }
            else
            {
                //add to currently existing team
                var locSet = StartLocations[slotNumber];
                locSet.FactoryNodes.Add(factory);
            }
        }

        [Serializable]
        private struct linkSerializationInfo
        {
            public int linkIndex;
            public int firstNodeIndex;
            public int secondNodeIndex;
        }
    }

    [Serializable]
    public class StartLocationsSet
    {
        public List<FactoryNode> FactoryNodes = new List<FactoryNode>();
        public AIDifficulty AIDifficulty = AIDifficulty.VeryDifficult;
        public AIBehaviourPattern AIBehaviour = AIBehaviourPattern.AttackAndDefend;
    }

    public class MapCashedResources
    {
        private List<NodeBase> nodes;
        private List<FactoryNode> factoryNodes;
        private List<Link> links;
        private List<KeyValuePair<NodeBase, List<KeyValuePair<NodeBase, int>>>> nodesAndWeightedNodesRelativelyToThem;

        public MapCashedResources(GameMap map)
        {
            nodes = new List<NodeBase>();
            factoryNodes = new List<FactoryNode>();
            links = new List<Link>();
            nodesAndWeightedNodesRelativelyToThem = new List<KeyValuePair<NodeBase, List<KeyValuePair<NodeBase, int>>>>();

            foreach (var point in map.ArmyLocationPoints)
            {
                if (point is NodeBase)
                    nodes.Add(point as NodeBase);
                if (point is FactoryNode)
                    factoryNodes.Add(point as FactoryNode);
                if (point is Link)
                    links.Add(point as Link);
            }

            var pathConstructor = new PathConstructor();
            foreach (var node in nodes)
            {
                var distanceToAllNodesFromThisFactory = pathConstructor.WeightAllNodesBasingOnHowFarTheyAreFrom(node);
                nodesAndWeightedNodesRelativelyToThem.Add(new KeyValuePair<NodeBase, List<KeyValuePair<NodeBase, int>>>(node, distanceToAllNodesFromThisFactory));
            }
        }

        public List<NodeBase> NodesAndInheritedFromNodes()
        {
            return nodes;
        }

        public List<FactoryNode> FactoryNodes()
        {
            return factoryNodes;
        }

        public List<Link> Links()
        {
            return links;
        }

        public List<KeyValuePair<NodeBase, int>> GetWeightedNodesFrom(NodeBase node)
        {
            var weightedNodes = nodesAndWeightedNodesRelativelyToThem.SingleOrDefault(x => x.Key == node).Value;
            return weightedNodes;
        }
    }
}