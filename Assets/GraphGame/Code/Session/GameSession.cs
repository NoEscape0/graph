using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading;
using ThreadPriority = System.Threading.ThreadPriority;

namespace GraphGame.Models
{
    public class GameSession : IUpdateListener
    {
        private GameMap map;
        private List<Player> players;
        private GameState state;
        private Thread armyLocPointsUpdateThread;

        public void Init(GameMap map, List<Player> players, bool cashResources = true)
        {
            this.map = map;
            state = GameState.Paused;
            this.players = players;
            if (cashResources)
                map.CashResources();

            setupPlayersNodes();
        }

        public GameState GetGameState()
        {
            return state;
        }

        public void SetGameState(GameState state)
        {
            this.state = state;
        }

        public GameMap GetMap()
        {
            return map;
        }

        private void setupPlayersNodes()
        {
            var startLocations = map.StartLocations;

            if (players.Count > startLocations.Count)
                throw new Exception("Players count is more then amount of start location groups");

            for (int i = 0; i < this.players.Count; ++i)
            {
                foreach (var factoryNode in startLocations[i].FactoryNodes)
                    factoryNode.SetOwner(this.players[i]);
            }
        }

        public void Update(float elapsedSec)
        {
            map.VictoryConditions.Update(elapsedSec);

            if (Constants.GetInstance().RunMovementLogicInSeparateThread)
            {
                if (armyLocPointsUpdateThread == null || armyLocPointsUpdateThread.ThreadState == ThreadState.Stopped)
                {
                    armyLocPointsUpdateThread = new Thread(delegate ()
                    {
                        updateMap(elapsedSec);
                    });
                    armyLocPointsUpdateThread.IsBackground = true;
                    armyLocPointsUpdateThread.Priority = ThreadPriority.Highest;
                    armyLocPointsUpdateThread.Start();
                }
                else
                {
                    InGameLogger.GetInstance().Log("Bad performance: Logic update skipped");
                }
            }
            else
            {
                updateMap(elapsedSec);
            }
        }

        private void updateMap(float elapsedSec)
        {
            if (state == GameState.Running)
            {
                float passedMs = elapsedSec * 1000;
                foreach (var armyLocPoint in map.ArmyLocationPoints)
                    armyLocPoint.Update(passedMs);

                foreach (var player in players)
                    player.Update(passedMs);
            }
        }
    }

    public enum GameState
    {
        Paused,
        Running,
        Finished
    }
}