using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace GraphGame.Models
{
    [Serializable]
    public class Link : ArmyLocationPointBase
    {
        public OnLinkCapacityChanged OnLinkCapacityChanged { get { return onLinkCapacityChanged; } set { onLinkCapacityChanged = value; } }
        [NonSerialized]
        private OnLinkCapacityChanged onLinkCapacityChanged;

        //bite map where each element can be NULL or UNIT
        private List<ArmyRange> armies = new List<ArmyRange>(); 
        private int linkPiecesCapacity;
        private int linkLenght;

        [NonSerialized]
        public NodeBase[] Connections = new NodeBase[2];

        public Link(NodeBase node1, NodeBase node2)
        {
            linkPiecesCapacity = (int)LinkPieceCapacity.Normal;
            Connections[0] = node1;
            Connections[1] = node2;
            UpdateLinkLenght();
        }

        public LinkPieceCapacity GetLinkPiecesCapcity()
        {
            return (LinkPieceCapacity)linkPiecesCapacity;
        }

        public void SetLinkPieceCapacity(LinkPieceCapacity linkPieceCapacity)
        {
            this.linkPiecesCapacity = (int)linkPieceCapacity;

            if (OnLinkCapacityChanged != null)
                OnLinkCapacityChanged(this, linkPieceCapacity);
        }

        public override void OnSessionStarted()
        {
            armies = new List<ArmyRange>();
            base.OnSessionStarted();
        }

        public List<ArmyRange> GetArmies()
        {
            return armies;
        }

        public int GetLinkLenght()
        {
            return linkLenght;
        }

        public void UpdateLinkLenght()
        {
            linkLenght = getPathLenght();
        }

        public override bool MoveArmyToThisLocation(ArmyRange army)
        {
            bool forwardDirection = isMovingForward(army);
            int enteringIndex = forwardDirection ? 0 : linkLenght - 1;

            int collisionCount = armies.Count(x => x.DoesRangeCollidesWithIndex(enteringIndex));
            
            bool collidingArmiesContainEnemyArmy = armies.Count(x => x.DoesRangeCollidesWithIndex(enteringIndex) && x.Owner != army.Owner) > 0;

            if (collidingArmiesContainEnemyArmy)
            {
                var enemyArmy = armies.First(x => x.DoesRangeCollidesWithIndex(enteringIndex) && x.Owner != army.Owner);
                enemyArmy.KillUnitAtCollidingIndex(enteringIndex);
                if (enemyArmy.Amount <= 0)
                    armies.Remove(enemyArmy);
                
                return true;
            }

            if (collisionCount < linkPiecesCapacity)
            {
                ArmyRange armyThatCanUnite = armies.FirstOrDefault(x => x.CanUniteWith(enteringIndex));
                //if can unite with
                if (armyThatCanUnite != null)
                    armyThatCanUnite.UniteWith(enteringIndex);
                else
                    armies.Add(new ArmyRange() {Amount = 1, Owner = army.Owner, Path = army.Path, StartIndex = enteringIndex});

                return true;
            }

            return false;
        }

        protected override void moveArmiesOneStep()
        {
            for (int i = armies.Count -1; i >= 0; i--)
                if (i < armies.Count)
                    moveArmyOneStepInRoute(armies[i]);
        }

        private void moveArmyOneStepInRoute(ArmyRange army)
        {
            bool forwardDirection = isMovingForward(army);

            //reached end of current link, moving to a new location
            if ((forwardDirection && army.FinishIndex == linkLenght - 1) || (!forwardDirection && army.StartIndex == 0))
            {
                var nextLocation = getNextLocation(army);
                if (nextLocation.MoveArmyToThisLocation(army))
                {
                    if (forwardDirection)
                        army.KillUnitAtStart();
                    else
                        army.KillUnitAtFinish();
                }

                if (army.Amount <= 0)
                    armies.Remove(army);
            }
            else
            {
                //moving further in the path

                int nextIndex = forwardDirection ? army.FinishIndex + 1 : army.StartIndex - 1;
                int collisions = armies.Count(x => x.DoesRangeCollidesWithIndex(nextIndex));

                var collidingEnemyArmy = armies.FirstOrDefault(x => x.DoesRangeCollidesWithIndex(nextIndex) && x.Owner != army.Owner);
                if (collidingEnemyArmy != null)
                {
                    if (forwardDirection)
                        army.KillUnitAtStart();
                    else
                        army.KillUnitAtFinish();

                    if (isMovingForward(collidingEnemyArmy))
                        collidingEnemyArmy.KillUnitAtStart();
                    else
                        collidingEnemyArmy.KillUnitAtFinish();

                    //remove empty
                    if (army.Amount <= 0)
                        armies.Remove(army);
                    if (collidingEnemyArmy.Amount <= 0)
                        armies.Remove(collidingEnemyArmy);

                } else if (collisions < linkPiecesCapacity) //todo else if
                {
                    //just move
                    if (forwardDirection)
                        army.StartIndex++;
                    else
                        army.StartIndex--;
                }
                else
                {
                    //SWAP
                    var otherArmy = armies.FirstOrDefault(x => x.DoesRangeCollidesWithIndex(nextIndex));
                    bool forwardDirectionForOtherUnit = isMovingForward(otherArmy);

                    if (forwardDirection != forwardDirectionForOtherUnit)
                    {
                        var leftArmy = (otherArmy.StartIndex > army.StartIndex) ? army : otherArmy;
                        var rightArmy = (otherArmy.StartIndex > army.StartIndex) ? otherArmy : army;
                        
                        rightArmy.StartIndex = leftArmy.StartIndex;
                        leftArmy.StartIndex = rightArmy.FinishIndex + 1;

                        if (leftArmy.StartIndex < 0 || rightArmy.StartIndex < 0 ||
                            leftArmy.FinishIndex >= GetLinkLenght() || rightArmy.FinishIndex >= GetLinkLenght())
                            Debug.LogError("ERROR");
                    }
                }
            }
       }


        private bool isMovingForward(ArmyRange army)
        {
            var linkStart = Connections[0];
            var linkFinish = Connections[1];


            for (int i = 0; i < army.Path.Count; ++i)
            {
                if (army.Path[i] != this)
                    continue;

                if (linkStart == army.Path[i - 1] && linkFinish == army.Path[i + 1])
                    return true;

                if (linkStart == army.Path[i + 1] && linkFinish == army.Path[i - 1])
                    return false;
            }

            Debug.LogError("Shouldn't be here " + army.Path.Count);
            return true;
        }

        private int getPathLenght()
       {
           var coord1 = Connections[0].GetCoordinate();
           var coord2 = Connections[1].GetCoordinate();

           var lenght = (float) Math.Sqrt(Math.Pow((coord1.x - coord2.x), 2) + Math.Pow((coord1.y - coord2.y), 2));
           lenght *= Constants.LinkPiecesInOneUnityUnit;
           return (int) lenght;
       }
   }

   public enum LinkPieceCapacity
   {
       Normal = 1,
       Double = 2,
       Tripple = 3
   }
}
 