using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace GraphGame.Models
{
    //node that contains a owerup that player can pick if he takes it over
    [Serializable]
    public class PowerUpNode : Node
    {
        private PowerUpType powerUpType;
        private bool taken = false;
        
        private static System.Random r = new System.Random();

        public PowerUpNode(Vector2 coordinate, PowerUpType powerUpType = PowerUpType.Random) : base(coordinate)
        {
            this.powerUpType = powerUpType;
        }

        public void SetPowerUpType(PowerUpType powerUpType)
        {
            this.powerUpType = powerUpType;

            if (OnPowerUpChanged != null)
                OnPowerUpChanged(this, powerUpType);
        }

        public PowerUpType GetPowerUpType()
        {
            return powerUpType;
        }

        public bool IsPowerUpTaken()
        {
            return taken;
        }

        public override void OnSessionStarted()
        {
            if (powerUpType == PowerUpType.Random)
            {
                int powerUpOptionsCount = Enum.GetValues(typeof (PowerUpType)).Cast<PowerUpType>().Select(v => v.ToString()).Count();
                int randomOptionNumber = r.Next(1, powerUpOptionsCount);
                SetPowerUpType((PowerUpType) randomOptionNumber);
            }

            base.OnSessionStarted();
        }

        protected override void onNewOwnerSet(Player oldOwner, Player newOwner)
        {
            //taken over for the first time
            if (!taken)
            {
                taken = true;

                //add powerUp to this player
                newOwner.AddPowerUp(powerUpType);

                OnPowerUpRemoved(this);
            }

            base.onNewOwnerSet(oldOwner, newOwner);
        }
    }

    public enum PowerUpType
    {
        Random,
        
        Fortify, StopProduction, Reset, LockOwner, RemovePowerUps
    }
}