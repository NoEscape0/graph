using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace GraphGame.Models
{
    [Serializable]
    public class ArmyRange
    {
        public List<ArmyLocationPointBase> Path = new List<ArmyLocationPointBase>();
        public Player Owner;

        public int Amount;
        public int StartIndex = 0;
        public int FinishIndex { get { return StartIndex + Amount;} }

        public bool DoesRangeCollidesWithIndex(int index)
        {
            if (index < StartIndex || index > FinishIndex)
                return false;
            return true;
        }

        public bool CanUniteWith(int singleUnitIndex)
        {
            if (singleUnitIndex + 1 == StartIndex || singleUnitIndex - 1 == FinishIndex)
                return true;

            return false;
        }

        public void UniteWith(int singleUnitIndex)
        {
            if (!CanUniteWith(singleUnitIndex))
                return;

            if (singleUnitIndex + 1 == StartIndex)
            {
                StartIndex--;
                Amount++;
                return;
            }

            if (singleUnitIndex - 1 == FinishIndex)
            {
                Amount++;
                return;
            }
        }

        public void KillUnitAtCollidingIndex(int index)
        {
            Amount--;

            if (index == StartIndex)
                StartIndex++;
        }

        public void KillUnitAtStart()
        {
            Amount--;
            StartIndex++;
        }

        public void KillUnitAtFinish()
        {
            Amount--;
        }

        public ArmyLocationPointBase GetCurrentLocation()
        {
            for (int i = 0; i < Path.Count; ++i)
            {
                if (Path[i] is Link)
                {
                    if ((Path[i] as Link).GetArmies().Contains(this))
                        return Path[i];
                }
                if (Path[i] is Node)
                {
                    if ((Path[i] as Node).GetArmies().Contains(this))
                        return Path[i];
                }
            }

            return null;
        }
    }
}