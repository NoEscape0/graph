using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace GraphGame.Models
{
    [Serializable]
    public class Node : NodeBase, IObservableNode
    {
        public ElsapsedMsDelegate OnUpdate { get { return onUpdate; } set { onUpdate = value; } }
        [NonSerialized]
        private ElsapsedMsDelegate onUpdate;

        public OnSomethingHappenedToNode OnFriendlyUnitDied { get { return onFriendlyUnitDied; } set { onFriendlyUnitDied = value; } }
        [NonSerialized]
        private OnSomethingHappenedToNode onFriendlyUnitDied;

        public OnSomethingHappenedToNode OnPowerUpRemoved { get { return onPowerUpRemoved; } set { onPowerUpRemoved = value; } }
        [NonSerialized]
        OnSomethingHappenedToNode onPowerUpRemoved;

        public OnPowerUpChanged OnPowerUpChanged { get { return onPowerUpChanged; } set { onPowerUpChanged = value; } }
        [NonSerialized]
        private OnPowerUpChanged onPowerUpChanged;

        public OnNodePowerUpTimeLeftChanged OnPowerUpTimeLeftUpdated { get { return onPowerUpTimeLeftUpdated; } set { onPowerUpTimeLeftUpdated = value; } }
        [NonSerialized]
        private OnNodePowerUpTimeLeftChanged onPowerUpTimeLeftUpdated;

        public OnSomethingHappenedToNode OnWaypointRemoved { get { return onWaypointRemoved; } set { onWaypointRemoved = value; } }
        [NonSerialized]
        private OnSomethingHappenedToNode onWaypointRemoved;

        public OnWaypointSettedUp OnWaypointSettedUp { get { return onWaypointSettedUp; } set { onWaypointSettedUp = value; } }
        [NonSerialized]
        private OnWaypointSettedUp onWaypointSettedUp;

        public OnNodeUnitsCountChanged OnNodeUnitsCountChanged { get { return onNodeUnitsCountChanged; } set { onNodeUnitsCountChanged = value; } }
        [NonSerialized]
        private OnNodeUnitsCountChanged onNodeUnitsCountChanged;

        public OnNodeUnitsStartSpawnRangeChanged OnNodeUnitsStartSpawnRangeChanged { get { return onNodeUnitsStartSpawnRangeChanged; } set { onNodeUnitsStartSpawnRangeChanged = value; } }
        [NonSerialized]
        private OnNodeUnitsStartSpawnRangeChanged onNodeUnitsStartSpawnRangeChanged;

        public OnOwnerChanged OnOwnerChanged { get { return onOwnerChanged; } set { onOwnerChanged = value; } }
        [NonSerialized]
        private OnOwnerChanged onOwnerChanged;

        public OnSomethingHappenedToNode OnNodeDestroyed { get { return onNodeDestroyed; } set { onNodeDestroyed = value; } }
        [NonSerialized]
        private OnSomethingHappenedToNode onNodeDestroyed;
        
        [NonSerialized]
        private Node waypoint;

        private bool affectedByPowerUp = false;
        private PowerUpType powerUp;

        private bool powerUpTimeLimited;
        private float powerUpTimeLeftSec;
        private int powerUpTimeLimitSec;
        

        //===Constructors
        public Node() : base ()
        {
        }
        public Node(Vector2 coordinate) : base(coordinate)
        {
        }
        public Node(Vector2 coordinate, Player owner) : base(coordinate, owner)
        {
        }

        //===Power Ups
        public void ApplyPowerUp(PowerUpType powerUpType, int timeLimitSec = 0)
        {
            if (OnPowerUpChanged != null)
                OnPowerUpChanged(this, powerUpType);

            affectedByPowerUp = true;
            powerUp = powerUpType;

            powerUpTimeLimited = timeLimitSec != 0;
            powerUpTimeLeftSec = timeLimitSec;
            powerUpTimeLimitSec = timeLimitSec;

            if (!tryApplyPowerUp(powerUpType))
                Debug.LogError("Cant apply power up for this type! " + powerUpType);
        }

        protected virtual bool tryApplyPowerUp(PowerUpType powerUpType)
        {
            switch (powerUpType)
            {
                case PowerUpType.Fortify:
                    new FortifyObserver(this);
                    return true;
                case PowerUpType.Reset:
                    new ResetObserver(this);
                    return true;
                case PowerUpType.RemovePowerUps:
                    RemovePowerUp();
                    return true;
            }

            return false;
        }

        public bool IsPowerUpApplied()
        {
            return affectedByPowerUp;
        }

        public PowerUpType GetAppliedPowerUpType()
        {
            if (!IsPowerUpApplied())
                throw new Exception("No PowerUp applied but you are trying to get applied power up!");
            return powerUp;
        }

        public void RemovePowerUp()
        {
            affectedByPowerUp = false;
            if (OnPowerUpRemoved != null)
                OnPowerUpRemoved(this);
        }

        public override void Update(float elapsedMs)
        {
            if (waypoint != null)
            {
                var notBusyArmy = GetArmyAmountWithoutOrders();
                if (notBusyArmy > Constants.SendingArmyByWaypointTreshHold)
                    OrderFreeArmyToMove(waypoint, 100);
            }

            //power ups time left
            if (powerUpTimeLimited && IsPowerUpApplied())
            {
                powerUpTimeLeftSec -= elapsedMs / 1000.0f;

                if (OnPowerUpTimeLeftUpdated != null)
                    OnPowerUpTimeLeftUpdated(this, powerUpTimeLeftSec, powerUpTimeLimitSec);

                if (powerUpTimeLeftSec <= 0)
                    RemovePowerUp();
            }

            if (OnUpdate != null)
                OnUpdate(elapsedMs);

            base.Update(elapsedMs);
        }


        //===Waypoints
        public void RemoveWaypoint()
        {
            waypoint = null;

            if (OnWaypointRemoved != null)
                OnWaypointRemoved(this);
        }

        public void SetWaypoint(Node destNode)
        {
            if (destNode == null || destNode == this)
                throw new Exception("Waypoint destNode cant be null or same as this one!");

            waypoint = destNode;

            OnWaypointSettedUp(this, destNode);
        }

        public Node GetWaypoint()
        {
            return waypoint;
        }

        //=== Observable
        public override void SetStartUnitsGenerationRange(int min, int max)
        {
            base.SetStartUnitsGenerationRange(min, max);
            if (OnNodeUnitsStartSpawnRangeChanged != null)
                OnNodeUnitsStartSpawnRangeChanged(this, GetStartUnitsGenerationRange());
        }

        protected override void onArmyAmountChanged(int newAmount)
        {
            if (OnNodeUnitsCountChanged != null)
                OnNodeUnitsCountChanged(this, newAmount);
            base.onArmyAmountChanged(newAmount);
        }
        
        public override void SetOwner(Player newOwner)
        {
            base.SetOwner(newOwner);

            if (IsPowerUpApplied())
                RemovePowerUp();
        }

        protected override void onNewOwnerSet(Player oldOwner, Player newOwner)
        {
            base.onNewOwnerSet(oldOwner, newOwner);

            if (waypoint != null)
                RemoveWaypoint();

            if (OnOwnerChanged != null && oldOwner != newOwner)
                OnOwnerChanged(this, oldOwner, newOwner);
        }


        protected override void onFriendlyUnitDiedDefendingNode()
        {
            base.onFriendlyUnitDiedDefendingNode();
            if (OnFriendlyUnitDied != null)
                OnFriendlyUnitDied(this);
        }

        protected override void OnDestroy()
        {
            OnNodeDestroyed(this);
        }
    }
}