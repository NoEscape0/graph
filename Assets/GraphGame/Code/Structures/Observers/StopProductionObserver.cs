using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame.Controllers;

namespace GraphGame.Models
{
    public class StopProductionObserver : PowerUpObserverBase
    {
        private FactoryNode observedNode;

        public StopProductionObserver(FactoryNode observedNode) : base(observedNode)
        {
            this.observedNode = observedNode;
            observedNode.OnUnitSpawned += onUnitSpawned;
        }
        

        private void onUnitSpawned(Node node, int totalAmount)
        {
            node.RemoveSingleUnit();
        }

        protected override void onAllPowerUpsRemoved(Node node)
        {
            observedNode.OnUnitSpawned -= onUnitSpawned;
            base.onAllPowerUpsRemoved(node);
        }
    }
}