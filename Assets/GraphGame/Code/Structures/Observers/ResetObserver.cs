using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame.Controllers;

namespace GraphGame.Models
{
    public class ResetObserver : PowerUpObserverBase
    {
        public ResetObserver(Node observedNode) : base(observedNode)
        {
            observedNode.SetArmyAmount(0);
            observedNode.RemovePowerUp();
        }
    }
}