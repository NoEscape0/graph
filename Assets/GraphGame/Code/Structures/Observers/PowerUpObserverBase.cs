using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GraphGame.Controllers;
using GraphGame.Views;

namespace GraphGame.Models
{
    public abstract class PowerUpObserverBase
    {
        public PowerUpObserverBase(IObservableNode observedObject)
        {
            var observedNode = (Node)observedObject;
            observedNode.OnUpdate += onUpdate;
            observedNode.OnFriendlyUnitDied += onFriendlyUnitDied;
            observedNode.OnPowerUpRemoved += onAllPowerUpsRemoved;
            observedNode.OnPowerUpChanged += onPowerUpChanged;
            observedNode.OnWaypointRemoved += onWaypointRemoved;
            observedNode.OnWaypointSettedUp += onWaypointSettedUp;
            observedNode.OnNodeUnitsCountChanged += onUnitsCountChanged;
            observedNode.OnOwnerChanged += onOwnerChanged;
            observedNode.OnNodeDestroyed += onNodeDestroyed;
            observedNode.OnPowerUpTimeLeftUpdated += onPowerUpTimeLeftUpdated;
        }

        protected virtual void onUpdate(float elapsedMs)
        {
        }

        protected virtual void onFriendlyUnitDied(Node node)
        {
        }

        protected virtual void onPowerUpChanged(Node node, PowerUpType powerUpType)
        {
        }

        protected virtual void onPowerUpTimeLeftUpdated(Node node, float timeLeft, int maxTime)
        {
        }

        protected virtual void onWaypointRemoved(Node node)
        {
        }

        protected virtual void onWaypointSettedUp(Node node, Node destinationNode)
        {
        }

        protected virtual void onUnitsCountChanged(Node node, int newCount)
        {
        }

        protected virtual void onOwnerChanged(Node node, Player oldOwner, Player newOwner)
        {
        }

        protected virtual void onNodeDestroyed(Node node)
        {
        }

        protected virtual void onAllPowerUpsRemoved(Node node)
        {
            node.OnUpdate -= onUpdate;
            node.OnFriendlyUnitDied -= onFriendlyUnitDied;
            node.OnPowerUpRemoved -= onAllPowerUpsRemoved;
            node.OnPowerUpChanged -= onPowerUpChanged;
            node.OnWaypointRemoved -= onWaypointRemoved;
            node.OnWaypointSettedUp -= onWaypointSettedUp;
            node.OnNodeUnitsCountChanged -= onUnitsCountChanged;
            node.OnOwnerChanged -= onOwnerChanged;
            node.OnNodeDestroyed -= onNodeDestroyed;
            node.OnPowerUpTimeLeftUpdated -= onPowerUpTimeLeftUpdated;
        }
    }
}