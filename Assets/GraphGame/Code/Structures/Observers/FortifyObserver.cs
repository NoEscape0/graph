using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame.Controllers;

namespace GraphGame.Models
{
    //while power up is active node is 50% tougher to take over
    public class FortifyObserver : PowerUpObserverBase
    {
        private bool spawnUnitRightAfterDeathOfAnother = false;

        public FortifyObserver(IObservableNode observedNode) : base (observedNode)
        {
        }

        protected override void onFriendlyUnitDied(Node node)
        {
            if (spawnUnitRightAfterDeathOfAnother)
                node.AddArmyAmount(1);

            spawnUnitRightAfterDeathOfAnother = !spawnUnitRightAfterDeathOfAnother;
        }
    }
}