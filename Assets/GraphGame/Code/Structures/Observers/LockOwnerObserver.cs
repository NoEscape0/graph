using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame.Controllers;

namespace GraphGame.Models
{
    public class LockOwnerObserver : PowerUpObserverBase
    {
        private Node observedNode;

        public LockOwnerObserver(Node observedNode) : base(observedNode)
        {
            this.observedNode = observedNode;
            observedNode.SetLockOwner(true);
        }

        protected override void onAllPowerUpsRemoved(Node node)
        {
            observedNode.SetLockOwner(false);
            base.onAllPowerUpsRemoved(node);
        }
    }
}