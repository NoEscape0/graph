using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;

namespace GraphGame.Models
{
    [Serializable]
    public abstract class ArmyLocationPointBase
    {
        [NonSerialized]
        private float elapsedTotalms = 0;

        public ArmyLocationPointBase()
        {
        }

        public virtual void Update(float elapsedMs)
        {
            elapsedTotalms += elapsedMs;

            float timeToMoveUnitOneStapForwardms = (1.0f/(float) Constants.UnitSpeedPerSecond)*1000.0f;

            //add 1 step to all units. 
            while (elapsedTotalms >= timeToMoveUnitOneStapForwardms)
            {
                elapsedTotalms -= timeToMoveUnitOneStapForwardms;
                
                moveArmiesOneStep();
            }
        }

        public virtual void OnSessionStarted()
        {
        }
        
        public abstract bool MoveArmyToThisLocation(ArmyRange army);

        protected ArmyLocationPointBase getNextLocation(ArmyRange army)
        {
            for (int i = 0; i < army.Path.Count; ++i)
            {
                if (army.Path[i] == this)
                {
                    if (i + 1 < army.Path.Count)
                        return army.Path[i + 1];
                }
            }
            return null;
        }

        protected abstract void moveArmiesOneStep();

        protected int absValueToPercentage(int value, int percentageFromValue)
        {
            if (value == 0 || percentageFromValue == 0)
                return 0;
            return (int) ((float) value/((float) percentageFromValue/100.0f));
        }

        protected int percentageToAbsValue(int percentage, int percentageFromValue)
        {
            if (percentageFromValue == 0)
                return 0;
            return (int) (percentage*(percentageFromValue/100.0f));
        }

        protected virtual void OnDestroy()
        {
        }
    }
}