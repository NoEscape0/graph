using System;
using UnityEngine;
using System.Collections;

namespace GraphGame.Models
{
    //node that spawns units once its taken over
    [Serializable]
    public class FactoryNode : Node
    {
        public OnNodeUnitsCountChanged OnUnitSpawned { get { return onUnitSpawned; } set { onUnitSpawned = value; } }
        [NonSerialized]
        private OnNodeUnitsCountChanged onUnitSpawned;

        private int stopSpawningWhenReached;
        private float totalElapsedMs = 0;

        public FactoryNode(Vector2 coordinate,
            int stopSpawningWhenReached = Constants.FactoryNodeStopSpawningAfterReachingNumber)
            : base(coordinate, null)
        {
            this.stopSpawningWhenReached = stopSpawningWhenReached;
            SetStartUnitsGenerationRange(Constants.StartFactoryArmyAmount, Constants.StartFactoryArmyAmount);
        }

        public override void Update(float elapsedMs)
        {
            base.Update(elapsedMs);

            if (!HasOwner() || GetTotalArmyAmount() >= stopSpawningWhenReached)
                return;

            totalElapsedMs += elapsedMs;

            float timeToSpawnOneUnitMs = Constants.FactoryNodeTimeToSpawnOneUnitInSeconds*1000;
            if (totalElapsedMs > timeToSpawnOneUnitMs)
            {
                addArmy(1);
                if (OnUnitSpawned != null)
                    OnUnitSpawned(this, GetTotalArmyAmount());

                if (OnNodeUnitsCountChanged != null)
                    OnNodeUnitsCountChanged(this, GetTotalArmyAmount());
                totalElapsedMs -= timeToSpawnOneUnitMs;
            }
        }

        protected override bool tryApplyPowerUp(PowerUpType powerUpType)
        {
            switch (powerUpType)
            {
                case PowerUpType.StopProduction:
                    new StopProductionObserver(this);
                    return true;
                case PowerUpType.LockOwner:
                    new LockOwnerObserver(this);
                    return true;
            }

            return base.tryApplyPowerUp(powerUpType);
        }
    }
}