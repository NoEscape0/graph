using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using GraphGame.Controllers;

namespace GraphGame.Models
{
    [Serializable]
    public abstract class NodeBase : ArmyLocationPointBase
    {
        protected List<ArmyRange> armies = new List<ArmyRange>();
        private Player owner = null;
        private SerializebleVector2 coordinate;
        private bool lockOwner = false;
        private IntRange startUnitsGenRange;

        private List<Link> links
        {
            get
            {
                if (linksUnsafe == null)
                    linksUnsafe = new List<Link>();
                return linksUnsafe;
            }
            set
            {
                linksUnsafe = value;
            }
        }
        [NonSerialized]
        private List<Link> linksUnsafe;

        private static System.Random r = new System.Random();

        public NodeBase()
        {
            sharedConstructor(new Vector2(0,0));
        }
        public NodeBase(Vector2 coordinate)
        {
            sharedConstructor(coordinate);
        }
        public NodeBase(Vector2 coordinate, Player owner)
        {
            sharedConstructor(coordinate);
        }

        private void sharedConstructor(Vector2 coordinate)
        {
            this.coordinate = new SerializebleVector2(coordinate);
            SetStartUnitsGenerationRange(Constants.NeutoralNodeSpawnArmyMinAmount, Constants.NeutoralNodeSpawnArmyMaxAmount);
        }

        public override void OnSessionStarted()
        {
            addArmy(r.Next(startUnitsGenRange.Min, startUnitsGenRange.Max));
        }


        public void SetLockOwner(bool LockOwner)
        {
            lockOwner = LockOwner;
        }

        //===Army unitsAmount and units
        public virtual void SetStartUnitsGenerationRange(int min, int max)
        {
            startUnitsGenRange = new IntRange() {Max = max, Min = min};

            if (startUnitsGenRange.Min < 0)
                startUnitsGenRange.Min = 0;
            if (startUnitsGenRange.Max < startUnitsGenRange.Min)
                startUnitsGenRange.Max = startUnitsGenRange.Min;
        }

        public IntRange GetStartUnitsGenerationRange()
        {
            return startUnitsGenRange;
        }

        public int GetTotalArmyAmount()
        {
            int amount = 0;
            foreach (var army in armies)
                amount += army.Amount;
            return amount;
        }
        public int GetArmyAmountWithoutOrders()
        {
            int amount = 0;
            foreach (var army in armies)
                if (army.Path.Count == 0)
                    amount += army.Amount;
            return amount;
        }

        public virtual void SetArmyAmount(int amount)
        {
            armies.Clear();
            addArmy(amount);
            onArmyAmountChanged(GetTotalArmyAmount());
        }

        public virtual void AddArmyAmount(int addAmount)
        {
            addArmy(addAmount);
            onArmyAmountChanged(GetTotalArmyAmount());
        }

        public virtual void RemoveSingleUnit()
        {
            var army = armies.First();

            army.Amount--;
            if (army.Amount <= 0)
                armies.Remove(army);

            onArmyAmountChanged(GetTotalArmyAmount());
        }

        public List<ArmyRange> GetArmies()
        {
            return armies;
        }

        protected virtual void onArmyAmountChanged(int newAmount)
        {
        }

        protected virtual void onFriendlyUnitDiedDefendingNode()
        {
            onArmyAmountChanged(GetTotalArmyAmount());
        }

        //===Army movement
        public void OrderFreeArmyToMove(NodeBase destinationNode, int unitsPercentage)
        {
            var pathConstructor = new PathConstructor();
            var path = pathConstructor.GenerateRoute(this, destinationNode);

            int movingAmount = percentageToAbsValue(unitsPercentage, GetArmyAmountWithoutOrders());

            ArmyRange freeArmy = null;
            for (int i = 0; i < armies.Count; ++i)
                if (armies[i].Path.Count == 0)
                    freeArmy = armies[i];
            
            if (freeArmy == null)
                return;

            if (freeArmy.Amount <= movingAmount)
                freeArmy.Path = path;
            else
            {
                //separate armies
                freeArmy.Amount -= movingAmount;
                if (freeArmy.Amount == 0)
                    Debug.Log("WTF"); //TODO
                armies.Add(new ArmyRange() {Amount = movingAmount, Owner = owner, Path = path});
            }
        }

        public void OrderArmyToMove(NodeBase destinationNode, int unitsPercentage)
        {
            var pathConstructor = new PathConstructor();
            var path = pathConstructor.GenerateRoute(this, destinationNode);

            cancelPreviousOrder();
            int movingAmount = percentageToAbsValue(unitsPercentage, GetTotalArmyAmount());
            
            if (armies.Count == 0)
                return;

            var freeArmy = armies[0];

            if (freeArmy.Amount <= movingAmount)
                freeArmy.Path = path;
            else
            {
                //separate armies
                freeArmy.Amount -= movingAmount;
                armies.Add(new ArmyRange() { Amount = movingAmount, Owner = owner, Path = path });
            }
        }

        //===Owner
        public bool HasOwner()
        {
            return owner != null;
        }

        public Player GetOwner()
        {
            if (owner == null)
                throw new NoOwnerException();
            return owner;
        }

        public virtual void SetOwner(Player newOwner)
        {
            if (newOwner == null)
                throw new NoOwnerException();

            var oldOwner = owner;
            owner = newOwner;

            if (oldOwner != newOwner)
                MainThreadController.GetInstance().DoFromMainThread(delegate { onNewOwnerSet(oldOwner, newOwner); });

            foreach (var army in armies)
                army.Owner = newOwner;
        }

        protected virtual void onNewOwnerSet(Player oldOwner, Player newOwner)
        {
        }


        //===Connectivity
        public bool IsConnectedTo(NodeBase otherNode)
        {
            return links.AsEnumerable().Count(x => x.Connections.AsEnumerable().Contains(otherNode)) > 0;
        }

        public Link ConnectTo(NodeBase otherNode)
        {
            if (IsConnectedTo(otherNode))
                return GetLinkWith(otherNode);

            Link link = new Link(this, otherNode);
            otherNode.links.Add(link);
            links.Add(link);
            return link;
        }

        public void ConnectTo(NodeBase otherNode, Link alreadyCreatedLink)
        {
            if (IsConnectedTo(otherNode))
                return;

            otherNode.links.Add(alreadyCreatedLink);
            links.Add(alreadyCreatedLink);
        }

        public void DisconnectFrom(NodeBase otherNode)
        {
            if (!IsConnectedTo(otherNode))
                return;

            var link = links.Single(x => x.Connections.AsEnumerable().Contains(otherNode));

            otherNode.links.Remove(link);
            links.Remove(link);
        }

        public List<NodeBase> GetConnectedNodes()
        {
            List<NodeBase> connections = new List<NodeBase>();

            foreach (var link in links)
                connections.AddRange(link.Connections.AsEnumerable().Where(x => x != this));

            return connections;
        }

        public List<Link> GetLinks()
        {
            return links;
        }

        public Link GetLinkWith(NodeBase node)
        {
            return links.FirstOrDefault(x => x.Connections.AsEnumerable().Contains(node) &&
                                             x.Connections.AsEnumerable().Contains(this));
        }

        //===Coordinate
        public Vector2 GetCoordinate()
        {
            return coordinate.Vector;
        }
        public void SetCoordinate(Vector2 coordinate)
        {
            this.coordinate.Vector = coordinate;
        }

        //===Movement calculation
        public override void Update(float elapsedMs)
        {
            var savedArmyCount = GetTotalArmyAmount();

            base.Update(elapsedMs);

            if (savedArmyCount != GetTotalArmyAmount())
                onArmyAmountChanged(GetTotalArmyAmount());
        }

        public override bool MoveArmyToThisLocation(ArmyRange army)
        {
            if (owner != army.Owner)
            {
                Player cashedOwner = owner;
                if (GetTotalArmyAmount() > 0)
                {
                    //fight
                    armies[0].Amount--;
                    if (armies[0].Amount <= 0)
                        armies.Remove(armies[0]);
                    onFriendlyUnitDiedDefendingNode();
                    return true;
                }
                else
                {
                    if (lockOwner)
                    {
                        sendArmyToPreviewsFactoryNodeInPath(army);
                        return false;
                    }
                    else
                    {
                        SetOwner(army.Owner);
                        return MoveArmyToThisLocation(army);
                    }
                }
            }
            else
            {
                //friendly unit came
                var nextDest = getNextLocation(army);

                if (nextDest == null)
                {
                    AddArmyAmount(1);
                    onArmyAmountChanged(GetTotalArmyAmount());
                    return true;
                }
                else
                {
                    //add to existing or create new one
                    var armyWithSamePathAndOwner = armies.FirstOrDefault(x => x.Path == army.Path && x.Owner == army.Owner);

                    if (armyWithSamePathAndOwner != null)
                        armies.First(x => x.Path == army.Path).Amount += 1;
                    else
                        armies.Add(new ArmyRange() { Amount = 1, Owner = owner, Path = army.Path });

                    onArmyAmountChanged(GetTotalArmyAmount());

                    return true;
                }
            }
        }
        
        private void sendArmyToPreviewsFactoryNodeInPath(ArmyRange army)
        {
            //send to previews node in path
            bool nextNodeIsReverseDest = false;
            for (int i = army.Path.Count - 1; i >= 0; --i)
            {
                var currentPathPoint = army.Path[i];
                if (currentPathPoint == this)
                {
                    nextNodeIsReverseDest = true;
                    continue;
                }

                if (nextNodeIsReverseDest && currentPathPoint is FactoryNode)
                {
                    //set dist
                    var pathConstructor = new PathConstructor();
                    var path = pathConstructor.GenerateRoute(this, currentPathPoint as Node);
                    army.Path = path;
                    break;
                }
            }
        }

        protected override void moveArmiesOneStep()
        {
            for (int i = armies.Count - 1; i >= 0; --i)
            {
                var currentArmy = armies[i];
                if (currentArmy.Path.Count == 0)
                    continue;

                ArmyLocationPointBase nextDest = getNextLocation(currentArmy);

                if (nextDest == null)
                {
                    Debug.LogError("Shouldnt be here!!!!_______");
                    currentArmy.Path = new List<ArmyLocationPointBase>();
                    continue;
                }

                bool moved = true;

                while (currentArmy.Amount > 0 && moved)
                {
                    moved = nextDest.MoveArmyToThisLocation(currentArmy);
                    if (moved)
                        currentArmy.Amount--;

                    if (currentArmy.Amount == 0)
                        armies.Remove(currentArmy);
                }
            }
        }

        protected void cancelPreviousOrder()
        {
            foreach (var army in armies)
                army.Path = new List<ArmyLocationPointBase>();
        }

        protected void addArmy(int unitsAmount)
        {
            bool armyWithNoOrdersExist = armies.Exists(x => x.Path.Count == 0);

            if (armyWithNoOrdersExist)
                armies.First(x => x.Path.Count == 0).Amount += unitsAmount;
            else
                armies.Add(new ArmyRange() {Amount = unitsAmount, Owner = owner, Path = new List<ArmyLocationPointBase>()});
        }
    }

    [Serializable]
    public class IntRange
    {
        public int Min, Max;
    }


    public class NoOwnerException : Exception
    {
        public override string Message
        {
            get
            {
                return "No owner exception";
            }
        }
    }
}