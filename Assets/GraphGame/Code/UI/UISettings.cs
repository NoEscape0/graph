using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using GraphGame.Models;

namespace GraphGame.Views
{
    public class UISettings : MonoBehaviour
    {
        //GAME
        //structure prefabs
        public NodeView NodeViewPrefab;
        public FactoryNodeView FactoryNodeViewPrefab;
        public PowerUpNodeView PowerUpNodeView;

        public EditorNodeView EditorNodeViewPrefab;
        public EditorFactoryNodeView EditorFactoryNodeViewPrefab;
        public EditorPowerUpNodeView EditorPowerUpNodeView;

        //sprites
        public Sprite NormalLinkSprite, DoubleLinkSprite, TripleLinkSprite;
        public Sprite WaypointActionSprite, RemoveWaypointSprite;
        public Sprite PowerUp_Random, PowerUp_Fortify, PowerUp_StopProduction, PowerUp_Reset, PowerUp_LockOwner, PowerUp_RemovePowerUps;

        public Color NoOwnerNodeColor = new Color(0.7f, 0.7f, 0.7f, 0.5f);
        public Color EmptyLinkColor = new Color(0.6f, 0.6f, 0.6f, 1.0f);
        public Color DisabledHudActionColor = new Color(1, 1, 1, 0.4f);
        public Material OrderArmyMoveLineMaterial;
        public Material WaypointLineineMaterial;

        public Color LocalPlayerColor = new Color(0, 1, 0);
        public Color SendArmyLineColor = Color.red;
        public Color WaypointLineColor = new Color(0.2f, 0.2f, 0.8f, 0.5f);

        public float ChangeColorTimeSec = 0.3f;

        //where link starts margin
        public int NodeMarginInUnits = 2;
        public int FactoryNodeMarginInUnits = 3;
        public int LinkPosZ = 2;

        //layers (closer to -100 means closer to cam)
        public int MoveCommandLineZPos = -2;
        
        //MAP EDITOR 
        public Material SelectionBoxMaterial;
        public Color GridColor = new Color(0.6f, 0.6f, 0.6f, 0.5f);

        public UITexts UiTexts;

        private static UISettings instance;

        public static UISettings GetInstance()
        {
            return instance;
        }

        private void Awake()
        {
            instance = this;
        }

        public Sprite GetPowerUpSprite(PowerUpType powerUpType)
        {
            switch (powerUpType)
            {
                case PowerUpType.Random:
                    return PowerUp_Random;
                case PowerUpType.Fortify:
                    return PowerUp_Fortify;
                case PowerUpType.StopProduction:
                    return PowerUp_StopProduction;
                case PowerUpType.Reset:
                    return PowerUp_Reset;
                case PowerUpType.LockOwner:
                    return PowerUp_LockOwner;
                case PowerUpType.RemovePowerUps:
                    return PowerUp_RemovePowerUps;
            }

            Debug.Log("No sprite for this power up type!");
            return null;
        }

        public Sprite GetLinkPieceSprite(LinkPieceCapacity capacity)
        {
            switch (capacity)
            {
                case LinkPieceCapacity.Normal:
                    return NormalLinkSprite;
                case LinkPieceCapacity.Double:
                    return DoubleLinkSprite;
                case LinkPieceCapacity.Tripple:
                    return TripleLinkSprite;
            }

            Debug.Log("No sprite for this capacity is found!");
            return null;
        }
    }

    [Serializable]
    public class UITexts
    {
        public string WaypointActionPopupMessage = "Click on the node you want to place Waypoint to. Units will automatically move there untill you remove the Waypoint";
        public string CancelWaypointCreationText = "Cancel waypoint creation";
    }
}