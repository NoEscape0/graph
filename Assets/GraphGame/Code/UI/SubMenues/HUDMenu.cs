using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GraphGame.Controllers;
using GraphGame.Models;
using UnityEngine.UI;

namespace GraphGame.Views
{
    [Serializable]
    public class HUDMenu
    {
        public RectTransform HUD_Holder;
        public Button HUDTopButton, HUDBottomButton, HUDLeftButton, HUDRightButton;

        public Image TopButtonSprite, BottomButtonSprite, LeftButtonSprite, RightButtonSprite;

        private Node hudShowForNode;

        public void ShowHUDFor(Node node, params NodeHudOption[] options)
        {
            hudShowForNode = node;
            List<Button> hudButtons = new List<Button>() { HUDTopButton, HUDBottomButton, HUDLeftButton, HUDRightButton };

            foreach (var button in hudButtons)
            {
                button.onClick.RemoveAllListeners();
                button.GetComponent<Image>().color = UISettings.GetInstance().DisabledHudActionColor;
                button.onClick.AddListener(delegate { HideHud(); });
            }

            TopButtonSprite.color = BottomButtonSprite.color = LeftButtonSprite.color = RightButtonSprite.color = new Color(0,0,0,0);

            foreach (var option in options)
            {
                Button button = null;

                switch (option.ButtonPlace)
                {
                    case HudButtonPlace.Top:
                        button = HUDTopButton;
                        break;
                    case HudButtonPlace.Bottom:
                        button = HUDBottomButton;
                        break;
                    case HudButtonPlace.Left:
                        button = HUDLeftButton;
                        break;
                    case HudButtonPlace.Right:
                        button = HUDRightButton;
                        break;
                }
                configureHUDButton(button, option);
            }

            HUD_Holder.gameObject.SetActive(true);
            var screenPos = Camera.main.WorldToScreenPoint(hudShowForNode.GetCoordinate());
            HUD_Holder.position = new Vector3(screenPos.x, screenPos.y, HUD_Holder.position.z);
        }

        public bool IsHUDShownFor(Node node)
        {
            return HUD_Holder.gameObject.activeSelf && node == hudShowForNode;
        }

        public void HideHud()
        {
            HUD_Holder.gameObject.SetActive(false);
        }

        private void configureHUDButton(Button button, NodeHudOption option)
        {
            Color color = option.Enabled ? Color.white : UISettings.GetInstance().DisabledHudActionColor;
            
            Image iconImage = null;
            switch (option.ButtonPlace)
            {
                case HudButtonPlace.Top:
                    iconImage = TopButtonSprite;
                    break;
                case HudButtonPlace.Bottom:
                    iconImage = BottomButtonSprite;
                    break;
                case HudButtonPlace.Left:
                    iconImage = LeftButtonSprite;
                    break;
                case HudButtonPlace.Right:
                    iconImage = RightButtonSprite;
                    break;
            }

            iconImage.color = color;

            switch (option.ActionType)
            {
                case NodeGameAction.SetWaypoint:
                    iconImage.sprite = UISettings.GetInstance().WaypointActionSprite;
                    break;
                case NodeGameAction.RemoveWaypoint:
                    iconImage.sprite = UISettings.GetInstance().RemoveWaypointSprite;
                    break;
                case NodeGameAction.UseFortifyPowerUp:
                    iconImage.sprite = UISettings.GetInstance().PowerUp_Fortify;
                    break;
                case NodeGameAction.UseLockOwnerPowerUp:
                    iconImage.sprite = UISettings.GetInstance().PowerUp_LockOwner;
                    break;
                case NodeGameAction.UseResetPowerUp:
                    iconImage.sprite = UISettings.GetInstance().PowerUp_Reset;
                    break;
                case NodeGameAction.UseStopProductionPowerUp:
                    iconImage.sprite = UISettings.GetInstance().PowerUp_StopProduction;
                    break;
                case NodeGameAction.UseRemovePowerUpsPowerUp:
                    iconImage.sprite = UISettings.GetInstance().PowerUp_RemovePowerUps;
                    break;
            }

            if (iconImage.sprite == null)
                Debug.LogError("No sprite for this power up!");
            
            button.GetComponent<Image>().color = color;

            if (option.OnActionClicked != null)
                button.onClick.AddListener(
                    delegate {
                    if (option.Enabled)
                        option.OnActionClicked();
                    });
        }
        
        public class NodeHudOption
        {
            public NodeHudOption(bool Enabled, NodeGameAction ActionType, HudButtonPlace ButtonPlace,
                NoVariablesDelegate OnActionClicked = null)
            {
                this.Enabled = Enabled;
                this.ActionType = ActionType;
                this.OnActionClicked = OnActionClicked;
                this.ButtonPlace = ButtonPlace;
            }

            public bool Enabled;
            public NodeGameAction ActionType;
            public NoVariablesDelegate OnActionClicked;
            public HudButtonPlace ButtonPlace;
        }

        public enum HudButtonPlace
        {
            Left,
            Right,
            Top,
            Bottom
        }
    }
}