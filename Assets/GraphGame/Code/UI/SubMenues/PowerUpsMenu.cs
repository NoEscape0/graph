using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GraphGame.Controllers;
using GraphGame.Models;
using UnityEngine.UI;

namespace GraphGame.Views
{
    [Serializable]
    public class PowerUpsMenu : IPlayerListener
    {
        public List<PowerUpMenuItem> PowerUpsMenuList;

        public void OnPlayerPowerUpsChanged(Dictionary<PowerUpType, int> powerUps)
        {
            updateMenu(powerUps);
        }

        private void updateMenu(Dictionary<PowerUpType, int> powerUps)
        {
            var orderedPowerUps = powerUps.ToList();

            for (int i = 0; i < PowerUpsMenuList.Count(); ++i)
            {
                PowerUpMenuItem item = PowerUpsMenuList[i];

                if (i >= orderedPowerUps.Count())
                {
                    //disable power up item
                    item.Holder.gameObject.SetActive(false);
                }
                else
                {
                    item.Holder.gameObject.SetActive(true);
                    var currentPowerUpInfo = orderedPowerUps[i];

                    item.PowerUpImage.sprite = UISettings.GetInstance().GetPowerUpSprite(currentPowerUpInfo.Key);
                    item.PowerUpText.text = currentPowerUpInfo.Value.ToString();
                }
            }
        }
    }

    [Serializable]
    public class PowerUpMenuItem
    {
        public RectTransform Holder;
        public Image PowerUpImage;
        public Text PowerUpText;
    }
}