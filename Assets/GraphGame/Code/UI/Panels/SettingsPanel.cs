using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame.Controllers;
using UnityEngine.UI;

namespace GraphGame.Views
{
    [Serializable]
    public class SettingsPanel : Panel
    {
        public Button BackButton;
        public Button ResetCampaignButton;

        public override void Init()
        {
            BackButton.onClick.AddListener(() =>
            {
                UIController.GetInstance().MainMenu.Show();
            });

            ResetCampaignButton.onClick.AddListener(() =>
            {
                CampaignController.GetInstance().CurrentMissionIndex = 0;
            });
        }
    }
}