using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GraphGame.Controllers;
using GraphGame.Models;
using UnityEngine.UI;

namespace GraphGame.Views
{
    [Serializable]
    public class MapEditorPanel : Panel
    {
        //top panel ====
        public Button MainMenuButton;
        public Button SaveMapButton;
        public Button SaveMapAsButton;
        public Button LoadButton;
        public Text LastLoadedMapText;

        //tools panel ====
        //Nodes
        public Button AddNodeButton;
        public Button AddFactoryNodeButton;
        public Button AddPowerUpNodeButton;

        //Links
        public Button ConnectSelectedNodesButton;
        public Button RemoveOneConnectionSelectedNodesButton;

        //start locations
        public Dropdown GroupsDropDown;
        public Button CreateNewGroup;
        public Button RemoveSelectedGroup;
        public Button AddSelectedFactoryToGroup;
        public Button RemoveSelectedFactoryFromGroup;
        public Dropdown AIDifficultyDropDown;
        public Dropdown AIBehaviourDropDown;

        //power ups
        public Dropdown PowerUpsDropDown;
        public InputField PowerUpsDurationInputField;

        //start units count
        public InputField StartUnitsCountMINInputField;
        public InputField StartUnitsCountMAXInputField;
        public Button SetStartUnitsRange;

        //Snap
        public Toggle SnapToggle;

        //selection and removal
        public Button DeselectAllButton;
        public Button DeleteSelectedButton;

        public override void Show(bool hideOtherPanels = true)
        {
            EditorSessionController.GetInstance().CreateSession();
            setLastLoadedMapText(string.Empty);
            refreshEditorPanel();

            SnapToggle.isOn = EditorSessionController.GetInstance().IsSnapToGridEnabled();

            base.Show(hideOtherPanels);
        }

        public override void Init()
        {
            //subscribe to selection change
            EditorSessionController.GetInstance().OnSelectedNodesModified += selectedNodesModified;
            EditorSessionController.GetInstance().OnNewSessionCreated += newSessionCreated;

            //top panel
            MainMenuButton.onClick.AddListener(() =>
            {
                EditorSessionController.GetInstance().RemoveSession();
                UIController.GetInstance().MainMenu.Show();
            });

            SaveMapButton.onClick.AddListener(() =>
            {
                saveMap();
            });

            SaveMapAsButton.onClick.AddListener(() =>
            {
                saveMapAs();
            });

            LoadButton.onClick.AddListener(() =>
            {
                loadMap();
            });

            //tools panel ===
            //nodes
            AddNodeButton.onClick.AddListener(() =>
            {
                addNode();
            });

            AddFactoryNodeButton.onClick.AddListener(() =>
            {
                addFactoryNode();
            });

            AddPowerUpNodeButton.onClick.AddListener(() =>
            {
                addPowerUpNode();
            });

            //linnks
            ConnectSelectedNodesButton.onClick.AddListener(() =>
            {
                EditorSessionController.GetInstance().ConnectSelectedNodes();
            });

            RemoveOneConnectionSelectedNodesButton.onClick.AddListener(() =>
            {
                EditorSessionController.GetInstance().DisconnectSelectedNodes();
            });

            //start locations
            GroupsDropDown.onValueChanged.AddListener(newValue =>
            {
                setDifficultyAndDefendOptionsAfterGroupChanged();
                startLocationGroupSelectedItemChanged();
            });

            CreateNewGroup.onClick.AddListener(() =>
            {
                EditorSessionController.GetInstance().CreateNewStartLocationGroup();
                refreshLocationGroupsDropDownsOptions();
                GroupsDropDown.value = GroupsDropDown.options.Count - 1;
                startLocationGroupSelectedItemChanged();
            });

            RemoveSelectedGroup.onClick.AddListener(() =>
            {
                EditorSessionController.GetInstance().RemoveSelectedLocationGroup(GroupsDropDown.value);
                refreshLocationGroupsDropDownsOptions();
                startLocationGroupSelectedItemChanged();
            });

            AddSelectedFactoryToGroup.onClick.AddListener(() =>
            {
                EditorSessionController.GetInstance().AddSelectedFactoryNodeToLocationGroup(GroupsDropDown.value);
            });

            RemoveSelectedFactoryFromGroup.onClick.AddListener(() =>
            {
                EditorSessionController.GetInstance().RemoveSelectedFactoryNodeFromLocationGroup();
            });
            
            AIDifficultyDropDown.onValueChanged.AddListener(newValue =>
            {
                AIDifficulty newDiff = (AIDifficulty)newValue;
                EditorSessionController.GetInstance().SetSelectedLocationGroupAIDifficulty(GroupsDropDown.value, newDiff);
            });

            AIBehaviourDropDown.onValueChanged.AddListener(newValue =>
            {
                AIBehaviourPattern newBehaviour = (AIBehaviourPattern)newValue;
                EditorSessionController.GetInstance().SetSelectedLocationGroupAIBehaviour(GroupsDropDown.value, newBehaviour);
            });

            //Power ups
            PowerUpsDropDown.ClearOptions();
            List<string> options = Enum.GetValues(typeof (PowerUpType)).Cast<PowerUpType>().Select(v => v.ToString()).ToList();
            PowerUpsDropDown.AddOptions(options);
            PowerUpsDropDown.onValueChanged.AddListener(newValue =>
            {
                EditorSessionController.GetInstance().SetPowerUpOfSelectedPowerUpNodes((PowerUpType)newValue);
            });

            PowerUpsDurationInputField.onValueChanged.AddListener(delegate(string newVal)
            {
                int duration;
                if (int.TryParse(newVal, out duration) && duration >= 0)
                    EditorSessionController.GetInstance().SetPowerUpsDuration(duration);
                else
                    PowerUpsDurationInputField.text = "0";
            });

            //start units count
            
            SetStartUnitsRange.onClick.AddListener(() =>
            {
                int min = int.Parse(StartUnitsCountMINInputField.text);
                int max = int.Parse(StartUnitsCountMAXInputField.text);
                EditorSessionController.GetInstance().SetStartGenerateRangeOfSelectedNodes(min, max);
            });
            

            //Snap
            SnapToggle.onValueChanged.AddListener(delegate (bool enableSnap)
            {
                EditorSessionController.GetInstance().SetSnapToGrid(enableSnap);
            });


            //selection and removal
            DeleteSelectedButton.onClick.AddListener(() =>
            {
                EditorSessionController.GetInstance().RemoveSelectedNodes();
            });

            DeselectAllButton.onClick.AddListener(() =>
            {
                EditorSessionController.GetInstance().DeselectAll();
            });

            base.Init();
        }

        public override void HotKeyPressed()
        {
            if (Input.GetKeyDown(Constants.EditorHotKeys.DeselectAllKey))
            {
                EditorSessionController.GetInstance().DeselectAll();
            }
            if (Input.GetKeyDown(Constants.EditorHotKeys.DeleteSelectedKey))
            {
                EditorSessionController.GetInstance().RemoveSelectedNodes();
            }

            base.HotKeyPressed();
        }

        private void saveMap()
        {
            if (EditorSessionController.GetInstance().GetLastOpenedMapName() == String.Empty)
                saveMapAs();
            else
                EditorSessionController.GetInstance().SaveMap(EditorSessionController.GetInstance().GetLastOpenedMapName());
        }

        private void saveMapAs()
        {
            var options = MapSaveLoadHelper.GetSavedMapsNamesList();
            UIController.GetInstance().SaveLoadPanel.UpdateOptions(true, options, delegate (string mapName)
            {
                setLastLoadedMapText(mapName);
                EditorSessionController.GetInstance().SaveMap(mapName);
            });
            UIController.GetInstance().SaveLoadPanel.Show(false);
        }

        private void loadMap()
        {
            var options = MapSaveLoadHelper.GetSavedMapsNamesList();
            UIController.GetInstance().SaveLoadPanel.UpdateOptions(false, options, delegate (string mapName)
            {
                setLastLoadedMapText(mapName);
                EditorSessionController.GetInstance().CreateSession(mapName);
                refreshEditorPanel();
            });

            UIController.GetInstance().SaveLoadPanel.Show(false);
        }

        private void addNode()
        {
            var pos = new Vector2(Camera.main.transform.position.x, Camera.main.transform.position.y);
            Node node = new Node(pos);
            EditorSessionController.GetInstance().AddArmyLocationPoint(node);
        }
        private void addFactoryNode()
        {
            var pos = new Vector2(Camera.main.transform.position.x, Camera.main.transform.position.y);
            FactoryNode node = new FactoryNode(pos);
            EditorSessionController.GetInstance().AddArmyLocationPoint(node);
        }
        private void addPowerUpNode()
        {
            var pos = new Vector2(Camera.main.transform.position.x, Camera.main.transform.position.y);
            PowerUpNode node = new PowerUpNode(pos, PowerUpType.Random);
            EditorSessionController.GetInstance().AddArmyLocationPoint(node);
        }

        private void setLastLoadedMapText(string text)
        {
            LastLoadedMapText.text = text;
        }

        private void refreshEditorPanel()
        {
            refreshLocationGroupsDropDownsOptions();
        }

        private void refreshLocationGroupsDropDownsOptions()
        {
            //group
            GroupsDropDown.ClearOptions();
            List<string> options = new List<string>();
            for (int i = 0; i < EditorSessionController.GetInstance().GetStartLocationsCount(); i++)
            {
                string name = (i == 0) ? i.ToString() + " player" : i.ToString() + " AI";
                options.Add(name);
            }
            GroupsDropDown.AddOptions(options);
            GroupsDropDown.value = 0;

            AIDifficultyDropDown.ClearOptions();
            AIBehaviourDropDown.ClearOptions();

            if (options.Count != 0)
            {
                //difficulty
                List<string> difficultyOptions = Enum.GetNames(typeof(AIDifficulty)).ToList();
                AIDifficultyDropDown.AddOptions(difficultyOptions);

                //behaviour
                List<string> behaviourOptions = Enum.GetNames(typeof(AIBehaviourPattern)).ToList();
                AIBehaviourDropDown.AddOptions(behaviourOptions);

                setDifficultyAndDefendOptionsAfterGroupChanged();
            }
        }

        private void setDifficultyAndDefendOptionsAfterGroupChanged()
        {
            AIDifficultyDropDown.value = (int)EditorSessionController.GetInstance().GetSelectedLocationGroupAIDifficulty(GroupsDropDown.value);
            AIBehaviourDropDown.value = (int)EditorSessionController.GetInstance().GetSelectedLocationGroupAIBehaviour(GroupsDropDown.value);
        }

        private void selectedNodesModified(List<Node> selectedNodes)
        {
            //===Deselect and remove
            DeselectAllButton.interactable = selectedNodes.Count != 0;
            DeleteSelectedButton.interactable = selectedNodes.Count != 0;
             
            AddSelectedFactoryToGroup.interactable = false;
            RemoveSelectedFactoryFromGroup.interactable = false;
            
            ConnectSelectedNodesButton.interactable = selectedNodes.Count >=2;
            RemoveOneConnectionSelectedNodesButton.interactable = selectedNodes.Count >= 2;

            bool aiDropDownsActive = GroupsDropDown.value != 0;
            AIDifficultyDropDown.interactable = aiDropDownsActive;
            AIBehaviourDropDown.interactable = aiDropDownsActive;
            PowerUpsDropDown.interactable = false;
            PowerUpsDropDown.captionText.text = "---";

            StartUnitsCountMINInputField.interactable = selectedNodes.Count != 0;
            StartUnitsCountMAXInputField.interactable = selectedNodes.Count != 0;
            SetStartUnitsRange.interactable = selectedNodes.Count != 0;

            if (selectedNodes.Count == 0)
                return;

            //units generation range
            bool sameMin = true;
            bool sameMax = true;
            var range = selectedNodes.First().GetStartUnitsGenerationRange();

            foreach (var node in selectedNodes)
            {
                var currentRange = node.GetStartUnitsGenerationRange();
                if (currentRange.Min != range.Min)
                    sameMin = false;
                if (currentRange.Max != range.Max)
                    sameMax = false;
            }

            StartUnitsCountMINInputField.text = sameMin ? range.Min.ToString() : "---";
            StartUnitsCountMAXInputField.text = sameMax ? range.Max.ToString() : "---";

            //only factory nodes
            if (selectedNodes.Count(x => !(x is FactoryNode)) == 0)
            {
                AddSelectedFactoryToGroup.interactable = true;
                RemoveSelectedFactoryFromGroup.interactable = true;
            }

            //only power up nodes
            bool onlyPowerUpsSelected = selectedNodes.Count(x => !(x is PowerUpNode)) == 0;
            PowerUpsDropDown.interactable = onlyPowerUpsSelected;
            if (onlyPowerUpsSelected)
            {
                var firstNodePowerUp = (selectedNodes[0] as PowerUpNode).GetPowerUpType();
                bool allNodesHaveSamePowerUp = selectedNodes.Count(x => (x as PowerUpNode).GetPowerUpType() != firstNodePowerUp) == 0;

                //set dropdown value
                if (allNodesHaveSamePowerUp)
                {
                    PowerUpsDropDown.value = (int) firstNodePowerUp;
                    PowerUpsDropDown.captionText.text = firstNodePowerUp.ToString();
                }
            }
        }

        private void newSessionCreated()
        {
            PowerUpsDurationInputField.text = EditorSessionController.GetInstance().GetPowerUpsDuration().ToString();
        }

        private void startLocationGroupSelectedItemChanged()
        {
            //AI
            AIDifficultyDropDown.interactable = GroupsDropDown.value != 0;
            AIBehaviourDropDown.interactable = GroupsDropDown.value != 0;
        }
    }
}