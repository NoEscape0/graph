using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace GraphGame.Views
{
    [Serializable]
    public class SaveLoadPanel : Panel
    {
        public UGUIListConstructor ListConstructor;

        public Text TitleText;
        public Text OkButtonText;
        public InputField FileNameInput;
        public Button OkButton;
        public Button CancelButton;
        public Button RemoveButton;

        private OnItemChosen onItemChosen;
        private List<string> options;
        private bool save;

        public void UpdateOptions(bool Save, List<string> Options, OnItemChosen OnItemChosen)
        {
            onItemChosen = OnItemChosen;
            save = Save;
            options = Options;

            OkButtonText.text = save ? "Save" : "Load";
            TitleText.text = save ? "Save as.." : "Load..";
            FileNameInput.text = string.Empty;
        }

        public override void Show(bool hideOtherPanels = true)
        {
            FileNameInput.text = string.Empty;

            ListConstructor.ConstructList(options.Count, true, delegate(List<GameObject> items)
            {
                for (int i = 0; i < items.Count; ++i)
                {
                    SaveLoadMenuItem menuItem = items[i].GetComponent<SaveLoadMenuItem>();
                    menuItem.Text.text = options[i];
                    menuItem.Button.onClick.AddListener(delegate
                    {
                        optionChosen(menuItem.Text.text);
                    });
                }
            });


            base.Show(hideOtherPanels);
        }

        public override void Init()
        {
            CancelButton.onClick.AddListener(delegate {
                                                          Hide();
            });

            OkButton.onClick.AddListener(delegate
            {
                if (FileNameInput.text != string.Empty)
                {
                    if (((!save && options.Contains(FileNameInput.text)) || save) && onItemChosen != null)
                    {
                        onItemChosen(FileNameInput.text);
                        Hide();
                    }
                }
            });

            RemoveButton.onClick.AddListener(delegate
            {
                if (FileNameInput.text != string.Empty && options.Contains(FileNameInput.text))
                {
                    MapSaveLoadHelper.DeleteMap(FileNameInput.text);
                    options.Remove(FileNameInput.text);
                    FileNameInput.text = string.Empty;
                    Show(false);
                }
            });

            base.Init();
        }

        private void optionChosen(string option)
        {
            FileNameInput.text = option;
        }
    }

    public delegate void OnItemChosen(string item);
}