using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame.Controllers;
using UnityEngine.UI;

namespace GraphGame.Views
{
    //doesnt stop the game. one button that performs action
    [Serializable]
    public class SmallMessagePanel : Panel
    {
        public Text MessageText;
        public Button ActionButton;
        public Text ActionButtonText;

        private NoVariablesDelegate onActionClicked;

        public void SetupAndShow(string text, string buttonText, NoVariablesDelegate OnActionClicked)
        {
            MessageText.text = text;
            ActionButtonText.text = buttonText;

            onActionClicked = OnActionClicked;

            Show(false);
        }

        public override void Init()
        {
            ActionButton.onClick.AddListener(() =>
            {
                if (onActionClicked != null)
                    onActionClicked();
            });

            base.Init();
        }
    }
}