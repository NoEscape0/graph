using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame.Controllers;
using UnityEngine.UI;

namespace GraphGame.Views
{
    //can pause the game, has title and shown in the middle of the screen
    [Serializable]
    public class MessagePanel : Panel
    {
        public Text PopUpTitle;
        public Text PopUpText;
        public Button CloseButton;

        public Color ErrorColor, NormalColor, InfoColor;

        private bool pauseGame = false;
        private bool unpauseGame = false;

        public void SetupAndShow(string title, string text, PopupTextType type, bool pauseGame = true)
        {
            PopUpTitle.text = title;
            PopUpText.text = text;
            this.pauseGame = pauseGame;

            Color color = new Color();

            switch (type)
            {
                case PopupTextType.Error:
                    color = ErrorColor;
                    break;
                case PopupTextType.Normal:
                    color = NormalColor;
                    break;
                case PopupTextType.Info:
                    color = InfoColor;
                    break;
            }

            PopUpTitle.color = PopUpText.color = color;

            Debug.Log(title + "  :  " + text);

            Show(false);
        }

        public override void Show(bool hideOtherPanels = true)
        {
            if (pauseGame)
            {
                GameSessionController controller = GameSessionController.GetInstance();

                if (controller != null && controller.IsSessionCreated())
                {
                    controller.Pause();
                    unpauseGame = true;
                }
            }

            base.Show(hideOtherPanels);
        }

        public override void Hide()
        {
            if (unpauseGame)
            {
                GameSessionController controller = GameSessionController.GetInstance();

                if (controller != null && controller.IsSessionCreated())
                    controller.UnPause();

                unpauseGame = false;
            }

            base.Hide();
        }

        public override void Init()
        {
            PopUpText.fontSize = PopUpTitle.fontSize; //TODO

            CloseButton.onClick.AddListener(() =>
            {
                this.Hide();
            });

            base.Init();
        }
    }

    public enum PopupTextType
    {
        Error,
        Normal,
        Info
    }
}