using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame.Controllers;
using UnityEngine.UI;

namespace GraphGame.Views
{
    [Serializable]
    public class PauseGameMenu : Panel
    {
        public Button ContinueButton;

        public Button RestartButton;
        public Button ToMainMenu;
        

        public override void Init()
        {
            ToMainMenu.onClick.AddListener(() =>
            {
                GameSessionController.GetInstance().RemoveSession();
                UIController.GetInstance().MainMenu.Show();
            });
            ContinueButton.onClick.AddListener(() =>
            {
                GameSessionController.GetInstance().UnPause();
                Hide();
            });
            RestartButton.onClick.AddListener(() =>
            {
                UIController.GetInstance().PerformActionWithLoadingScreen(delegate
                    { GameSessionController.GetInstance().RestartSession(); });
            });

            base.Init();
        }
    }
}