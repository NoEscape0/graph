using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GraphGame.Controllers;
using GraphGame.Models;
using UnityEngine.UI;

namespace GraphGame.Views
{
    [Serializable]
    public class CampaignConsolePanel : Panel
    {
        public Text ConsoleText;
        public Button SkipButton;

        private List<CampaignDialogLine> dialog;
        private NoVariablesDelegate onDialogFinished;
        private Coroutine coroutine;

        public void SetUpAndShow(List<CampaignDialogLine> Dialog, NoVariablesDelegate OnDialogFinished)
        {
            dialog = Dialog;
            onDialogFinished = OnDialogFinished;

            Show();

            if (Dialog.Count > 0)
                coroutine = UIController.GetInstance().StartCoroutine(runDialog());
            else
                OnDialogFinished();
        }

        public override void Init()
        {
            SkipButton.onClick.AddListener(delegate { skipButtonClicked(); });
            base.Init();
        }

        private IEnumerator runDialog()
        {
            ConsoleText.text = "";
            foreach (var dialogLine in dialog)
            {
                if (dialogLine.WaitSeconds > 0)
                    yield return new WaitForSeconds(dialogLine.WaitSeconds);

                if (!dialogLine.AppendToPreviewsText)
                {
                    if (dialogLine != dialog.First())
                        ConsoleText.text += "\n";
                    
                    ConsoleText.text += string.Format("<color={0}><{1}></color> ", toHex(dialogLine.Color), dialogLine.AuthorName);
                }

                foreach (var simbol in dialogLine.Text)
                {
                    ConsoleText.text += simbol;
                    solveOverflow();
                    yield return new WaitForSeconds(Constants.CampaignTypingDelay);
                }
            }

            onDialogFinished();
        }

        private void solveOverflow()
        {
            if (checkForTextHeightOverflow())
            {
                int indexOfNewLine = ConsoleText.text.IndexOf("\n") + 1;
                ConsoleText.text = ConsoleText.text.Substring(indexOfNewLine, ConsoleText.text.Length - indexOfNewLine);
            }
        }

        protected bool checkForTextHeightOverflow()
        {
            float textHeight = LayoutUtility.GetPreferredHeight(ConsoleText.rectTransform);
            float parentHeight = PanelGameObject.GetComponent<RectTransform>().rect.height;
            return textHeight > parentHeight;
        }

        private string toHex(Color c)
        {
            return string.Format("#{0:X2}{1:X2}{2:X2}", ToByte(c.r), ToByte(c.g), ToByte(c.b));
        }

        private byte ToByte(float f)
        {
            f = Mathf.Clamp01(f);
            return (byte)(f * 255);
        }

        private void skipButtonClicked()
        {
            if (coroutine != null)
                UIController.GetInstance().StopCoroutine(coroutine);
            onDialogFinished();
        }
    }
}
