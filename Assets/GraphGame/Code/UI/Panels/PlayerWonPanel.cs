using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame.Controllers;
using UnityEngine.UI;

namespace GraphGame.Views
{
    [Serializable]
    public class PlayerWonPanel : Panel
    {
        public Button MainMenuButton;
        public Button NextButton;

        public void SetUpAndShow(bool nextCompainMissionButtonEnabled)
        {
            NextButton.gameObject.SetActive(nextCompainMissionButtonEnabled);
            Show();
        }

        public override void Init()
        {
            MainMenuButton.onClick.AddListener(() =>
            {
                UIController.GetInstance().MainMenu.Show();
            });
            NextButton.onClick.AddListener(() =>
            {
                CampaignController.GetInstance().ContinueCompain();
            });
        }
    }
}