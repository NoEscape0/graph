using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame.Controllers;
using GraphGame.Models;
using UnityEngine.UI;

namespace GraphGame.Views
{
    [Serializable]
    public class GamePanel : Panel
    {
        public Button PauseGameButton;

        public Button FirstArmyOrderPercentageButton;
        public Button SecondArmyOrderPercentageButton;
        public Button ThirdArmyOrderPercentageButton;

        public Text FirstArmyOrderPercentageText;
        public Text SecondArmyOrderPercentageText;
        public Text ThirdArmyOrderPercentageText;

        public HUDMenu HUDMenu;
        public PowerUpsMenu PowerUpsMenu;

        private List<Button> armyPercentageButtons;

        public override void Show(bool hideOtherPanels = true)
        {
            FirstArmyOrderPercentageText.text = Constants.FirstArmyMovePercentage + "%";
            SecondArmyOrderPercentageText.text = Constants.SecondArmyMovePercentage + "%";
            ThirdArmyOrderPercentageText.text = Constants.ThirdArmyMovePercentage + "%";

            GameSessionController.GetInstance().CurrentArmyMoveOrderPercentage = Constants.FirstArmyMovePercentage;
            setActiveRadioButton(FirstArmyOrderPercentageButton);
            HUDMenu.HideHud();
            PowerUpsMenu.OnPlayerPowerUpsChanged(new Dictionary<PowerUpType, int>());

            base.Show(hideOtherPanels);
        }

        public override void Init()
        {
            armyPercentageButtons = new List<Button>()
            {
                FirstArmyOrderPercentageButton,
                SecondArmyOrderPercentageButton,
                ThirdArmyOrderPercentageButton
            };

            //top panel
            PauseGameButton.onClick.AddListener(() =>
            {
                GameSessionController.GetInstance().Pause();
                UIController.GetInstance().PauseGameMenu.Show(false);
            });

            //bottom
            FirstArmyOrderPercentageButton.onClick.AddListener(() =>
            {
                GameSessionController.GetInstance().CurrentArmyMoveOrderPercentage = Constants.FirstArmyMovePercentage;
                setActiveRadioButton(FirstArmyOrderPercentageButton);
            });
            SecondArmyOrderPercentageButton.onClick.AddListener(() =>
            {
                GameSessionController.GetInstance().CurrentArmyMoveOrderPercentage = Constants.SecondArmyMovePercentage;
                setActiveRadioButton(SecondArmyOrderPercentageButton);
            });
            ThirdArmyOrderPercentageButton.onClick.AddListener(() =>
            {
                GameSessionController.GetInstance().CurrentArmyMoveOrderPercentage = Constants.ThirdArmyMovePercentage;
                setActiveRadioButton(ThirdArmyOrderPercentageButton);
            });

            base.Init();
        }
        
        private void setActiveRadioButton(Button activeButton)
        {
            foreach (var radioButton in armyPercentageButtons)
            {
                var image = radioButton.GetComponent<Image>();
                image.color = (activeButton == radioButton) ? Color.green : Color.white;
            }
        }
    }
}