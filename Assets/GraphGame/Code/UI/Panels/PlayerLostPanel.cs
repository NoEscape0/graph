using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame.Controllers;
using UnityEngine.UI;

namespace GraphGame.Views
{
    [Serializable]
    public class PlayerLostPanel : Panel
    {
        public Button MainMenuButton;
        public Button ReplayButton;

        public override void Init()
        {
            MainMenuButton.onClick.AddListener(() =>
            {
                UIController.GetInstance().MainMenu.Show();
            });

            ReplayButton.onClick.AddListener(() =>
            {
                UIController.GetInstance().PerformActionWithLoadingScreen(delegate
                    { GameSessionController.GetInstance().RestartSession(); });
            });
        }
    }
}