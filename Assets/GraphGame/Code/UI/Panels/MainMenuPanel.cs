using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame.Controllers;
using UnityEngine.UI;

namespace GraphGame.Views
{
    [Serializable]
    public class MainMenuPanel : Panel
    {
        public Button PlayCampaignButton;
        public Button SettingsButton;
        public Button QuitButton;

        public Button ToMapEditorButton;
        public Button PlayMapButton;

        public override void Init()
        {
            ToMapEditorButton.onClick.AddListener(delegate {UIController.GetInstance().MapEditor.Show();});

            PlayCampaignButton.onClick.AddListener(delegate { playCampaign(); });

            PlayMapButton.onClick.AddListener(delegate { loadMap();});

            SettingsButton.onClick.AddListener(delegate {UIController.GetInstance().Settings.Show();});

            QuitButton.onClick.AddListener(delegate {Application.Quit();});

            base.Init();
        }

        private void playCampaign()
        {
            if (CampaignController.GetInstance().IsCampaignCompleted())
            {
                UIController.GetInstance().MessagePanel.SetupAndShow("Campaign complted!", "You have already completed the campaign!", PopupTextType.Info, false);
                return;
            }

            CampaignController.GetInstance().ContinueCompain();
        }

        private void loadMap()
        {
            var options = MapSaveLoadHelper.GetSavedMapsNamesList();
            UIController.GetInstance().SaveLoadPanel.UpdateOptions(false, options, delegate(string mapName)
            {
                UIController.GetInstance().PerformActionWithLoadingScreen(delegate
                    { GameSessionController.GetInstance().CreateSession(mapName); });
            });

            UIController.GetInstance().SaveLoadPanel.Show(false);
        }
    }
}