using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace GraphGame.Views
{
    public class SaveLoadMenuItem : MonoBehaviour
    {
        public Button Button;
        public Text Text;
        public Image BackgroundImage;
    }
}