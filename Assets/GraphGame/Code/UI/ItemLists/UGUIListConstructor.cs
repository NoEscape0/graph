using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using  UnityEngine.UI;

namespace GraphGame.Views
{
    public class UGUIListConstructor : MonoBehaviour
    {
        /*
     * content должен быть top Stretach
     * */
        public GameObject ListItemPrefab, ScrollView;
        public RectTransform ViewportContent;

        public delegate void GOList(List<GameObject> constructedItems);

        public float spaceBetweenItems = 0.03f;

        private Vector2 cashedSizeDelta;
        private Vector3 cashedLocalPosition;
        private List<GameObject> previewslyConstructed = null;
        private bool initialized = false;

        public void InitWithZeroItems()
        {
            foreach (RectTransform item in ViewportContent)
            {
                item.gameObject.SetActive(false);
            }
            initialized = true;
        }

        public void ConstructList(int count, bool forceRecreate = true, GOList onListConstructedCallback = null)
        {
            List<GameObject> constructed = new List<GameObject>();

            if (forceRecreate || (previewslyConstructed == null || previewslyConstructed.Count != count))
            {
                if (!initialized)
                    InitWithZeroItems();


                ViewportContent.sizeDelta = new Vector2(0, 0);

                //cashing values of the first item (screen resolution specific)
                foreach (RectTransform item in ViewportContent)
                {
                    cashedSizeDelta = item.gameObject.GetComponent<RectTransform>().sizeDelta;
                    cashedLocalPosition = new Vector3(item.gameObject.GetComponent<RectTransform>().localPosition.x,
                        -cashedSizeDelta.y/2);
                    break;
                }

                foreach (RectTransform item in ViewportContent)
                {
                    Destroy(item.gameObject);
                }

                float posY = -(cashedSizeDelta.y*spaceBetweenItems)/2;
                for (int i = 0; i < count; ++i)
                {
                    var instantiated =
                        (GameObject) Instantiate(ListItemPrefab, new Vector3(0, 0, 0), Quaternion.identity);
                    constructed.Add(instantiated);
                    instantiated.gameObject.transform.SetParent(ViewportContent);
                    RectTransform rectTransform = instantiated.GetComponent<RectTransform>();

                    rectTransform.sizeDelta = cashedSizeDelta;
                    rectTransform.localPosition = cashedLocalPosition + new Vector3(0, posY);
                    posY -= cashedSizeDelta.y + cashedSizeDelta.y*spaceBetweenItems;
                }
                ViewportContent.sizeDelta = new Vector2(0, count*cashedSizeDelta.y*(1 + spaceBetweenItems));
            }
            else
            {
                if (previewslyConstructed != null)
                    constructed = previewslyConstructed;
            }

            previewslyConstructed = new List<GameObject>();
            previewslyConstructed.AddRange(constructed);



            if (onListConstructedCallback != null)
                onListConstructedCallback(constructed);
        }
    }
}