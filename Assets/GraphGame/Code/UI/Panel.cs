using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame.Controllers;

namespace GraphGame.Views
{
    [Serializable]
    public class Panel : System.Object
    {
        public GameObject PanelGameObject;

        public virtual void Init()
        {
        }

        public bool IsShown()
        {
            return PanelGameObject.activeSelf;
        }

        public virtual void Show(bool hideOtherPanels = true)
        {
            if (hideOtherPanels)
                UIController.GetInstance().HideAllPanels();
            PanelGameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            PanelGameObject.SetActive(false);
        }

        public virtual void HotKeyPressed()
        {

        }
    }
}