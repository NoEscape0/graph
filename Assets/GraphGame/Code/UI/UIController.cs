using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;

namespace GraphGame.Views
{
    public class UIController : MonoBehaviour
    {
        //each panel should be also added in Awake()
        public MainMenuPanel MainMenu;
        public MapEditorPanel MapEditor;
        public GamePanel GamePanel;
        public SettingsPanel Settings;
        public Panel LoadingScreen;
        public PlayerWonPanel PlayerWonPanel;
        public PlayerLostPanel PlayerLostPanel;
        public CampaignConsolePanel CampaignConsolePanel;

        public SaveLoadPanel SaveLoadPanel;
        public PauseGameMenu PauseGameMenu;
        public MessagePanel MessagePanel;
        public SmallMessagePanel SmallMessageWindow;

        public List<RectTransform> ObjectsBlockingInteractionWhenMouseOver;

        private static UIController instance;

        private List<Panel> panels;
        private List<GameObject> currentlyBlockingUIObjects;

        public void HideAllPanels()
        {
            foreach (var panel in panels)
                panel.Hide();
        }

        public bool IsMouseOverUI()
        {
            for (int i = currentlyBlockingUIObjects.Count - 1; i >= 0; --i)
            {
                var blockingElement = currentlyBlockingUIObjects[i];

                var currentElemInHier = blockingElement.transform;
                for (;;)
                {
                    if (!currentElemInHier.gameObject.activeSelf)
                    {
                        removeFromCurrentlyBlockingObject(blockingElement);
                        break;
                    }

                    if (currentElemInHier.parent != null)
                        currentElemInHier = currentElemInHier.parent;
                    else
                        return true;
                }
            }

            return false;
        }

        public void PerformActionWithLoadingScreen(NoVariablesDelegate action)
        {
            StartCoroutine(actionResolver(action));
        }

        private IEnumerator actionResolver(NoVariablesDelegate action)
        {
            LoadingScreen.Show();
            yield return new WaitForFixedUpdate();
            action();
            LoadingScreen.Hide();
        }

        public static UIController GetInstance()
        {
            return instance;
        }

        private void Awake()
        {
            instance = this;
            panels = new List<Panel>()
            {
                MainMenu, Settings, MapEditor, GamePanel, PlayerWonPanel, PlayerLostPanel, CampaignConsolePanel,

                SaveLoadPanel, PauseGameMenu, MessagePanel, SmallMessageWindow, 

                LoadingScreen
            };

            foreach (var panel in panels)
                panel.Init();

            currentlyBlockingUIObjects = new List<GameObject>();
            foreach (var objectToBlockInteraction in ObjectsBlockingInteractionWhenMouseOver)
            {
                var trigger = objectToBlockInteraction.gameObject.AddComponent<EventTrigger>();

                EventTrigger.Entry pointerEnterEntry = new EventTrigger.Entry();
                pointerEnterEntry.eventID = EventTriggerType.PointerEnter;
                pointerEnterEntry.callback.AddListener((eventData) =>
                {
                    addToCurrentlyBlockingObjects(trigger.gameObject);
                });
                trigger.triggers.Add(pointerEnterEntry);

                EventTrigger.Entry pointerExitEntry = new EventTrigger.Entry();
                pointerExitEntry.eventID = EventTriggerType.PointerExit;
                pointerExitEntry.callback.AddListener((eventData) =>
                {
                    removeFromCurrentlyBlockingObject(trigger.gameObject);
                });
                trigger.triggers.Add(pointerExitEntry);
            }

            HideAllPanels();
            MainMenu.Show();
        }

        private void addToCurrentlyBlockingObjects(GameObject blockingObject)
        {
            if (!currentlyBlockingUIObjects.Contains(blockingObject))
                currentlyBlockingUIObjects.Add(blockingObject);
        }

        private void removeFromCurrentlyBlockingObject(GameObject blockingObject)
        {
            if (currentlyBlockingUIObjects.Contains(blockingObject))
                currentlyBlockingUIObjects.Remove(blockingObject);
        }

        private void Update()
        {
            if (Input.anyKeyDown)
                foreach (var panel in panels)
                    if (panel.IsShown())
                        panel.HotKeyPressed();
        }
    }
}