//#define AIDEBUG
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using ThreadState = System.Threading.ThreadState;

namespace GraphGame.Models
{
    [Serializable]
    public class AIPlayer : Player
    {
        //how many times factory node should be more far from enemy then closest to enemy f node to be considered safe. higher the value- lower safe nodes count
        private const double safeNodeDistanceRate = 1.5;

        //TODO make all of them non serializeble. and find a way to provide map after deserialization if gonna add save level ai
        private GameMap map;
        private float elapsedTotalms = 0;
        private AISettings.AIDifficultyConstants difficultyConstants;
        private AIBehaviourPattern behaviourPattern;
        private Thread aiPlayerThread;

        public AIPlayer(string name, Color color, GameMap map, AIBehaviourPattern behaviour, AIDifficulty difficulty) : base (name, color)
        {
            this.map = map;

            switch (difficulty)
            {
                case AIDifficulty.VeryDifficult:
                    difficultyConstants = Constants.GetInstance().AiSettings.Difficulty_VeryDifficult;
                    break;
                case AIDifficulty.Difficult:
                    difficultyConstants = Constants.GetInstance().AiSettings.Difficulty_Difficult;
                    break;
                case AIDifficulty.Average:
                    difficultyConstants = Constants.GetInstance().AiSettings.Difficulty_Average;
                    break;
                case AIDifficulty.Easy:
                    difficultyConstants = Constants.GetInstance().AiSettings.Difficulty_Easy;
                    break;
                case AIDifficulty.VeryEasy:
                    difficultyConstants = Constants.GetInstance().AiSettings.Difficulty_VeryEasy;
                    break;
            }

            elapsedTotalms = difficultyConstants.UpdateLogicEverySec * 1000;
            behaviourPattern = behaviour;
        }

        public override void Update(float elapsedms)
        {
            elapsedTotalms += elapsedms;
        
            if (elapsedTotalms >= difficultyConstants.UpdateLogicEverySec * 1000)
            {
                elapsedTotalms = 0;

                if (Constants.GetInstance().RunAILogicInAdditionalThread)
                {
                    if (aiPlayerThread == null || aiPlayerThread.ThreadState == ThreadState.Stopped)
                    {
                        aiPlayerThread = new Thread(calculateAIActions);
                        aiPlayerThread.IsBackground = true;
                        aiPlayerThread.Start();
                    }
                }
                else
                    calculateAIActions();
            }
        }

        private void calculateAIActions()
        {
            #if AIDEBUG
            Stopwatch watch = new Stopwatch();
            watch.Start();
            #endif
            var factories = getMyFactoryNodes();
        
            List<FactoryNode> factoryNodesThatAreBusyDefending = new List<FactoryNode>();
            //defend
            if (behaviourPattern != AIBehaviourPattern.AttackOnly)
            {
                List<KeyValuePair<FactoryNode, int>> nodesUnderAttackAndAttackingAmount = getFactoryNodesUnderAttack();
                foreach (var nodeAttackingAmountPair in nodesUnderAttackAndAttackingAmount)
                {
                    //defend only if have small amount of units
                    if (nodeAttackingAmountPair.Key.GetTotalArmyAmount() < difficultyConstants.RequiredArmyAmountForDefense * 2)
                    {
                        factoryNodesThatAreBusyDefending.Add(nodeAttackingAmountPair.Key);

                        performDefenseAction(nodeAttackingAmountPair.Key, nodeAttackingAmountPair.Value,
                            factoryNodesThatAreBusyDefending);
                    }
                }
            }

            //attack
            var safeFactories = getMySafeFactoryNodes();
            List<NodeBase> attackingNodes = new List<NodeBase>();
            if (behaviourPattern != AIBehaviourPattern.DefendOnly)
            {
                foreach (var factory in factories)
                {
                    if (factory.GetArmyAmountWithoutOrders() < difficultyConstants.MinimulArmyAmountToAttack ||
                        factoryNodesThatAreBusyDefending.Contains(factory))
                        continue;

                    var attackingNode = performAttackAction(factory, safeFactories.Contains(factory));
                    if (attackingNode != null)
                        attackingNodes.Add(attackingNode);
                }
            }

            //find in nodes and power up nodes lost units and send them to closest factory
            var nodes = getMyNodes();
            foreach (var node in nodes)
            {
                if (node is FactoryNode)
                    continue;

                if (node.HasOwner() && node.GetOwner() == this && node.GetTotalArmyAmount() > 0)
                {
                    List<KeyValuePair<NodeBase, int>> distanceToAllNodesFromThis = map.GetCashedResources().GetWeightedNodesFrom(node);
                    foreach (var pair in distanceToAllNodesFromThis)
                    {
                        if (pair.Key is FactoryNode && pair.Key.HasOwner() && pair.Key.GetOwner() == this)
                        {
                            node.OrderFreeArmyToMove(pair.Key, 100);
                            break;
                        }
                    }
                }
            }


            for (int i = powerUps.Count - 1; i >= 0; i--)
            {
                var powerUp = powerUps.ToList()[i].Key;

                switch (powerUp)
                {
                    case PowerUpType.Fortify:
                    case PowerUpType.LockOwner:
                        foreach (var factory in factories)
                        {
                            if (safeFactories.Contains(factory) || !factoryNodesThatAreBusyDefending.Contains(factory) || factory.IsPowerUpApplied())
                                continue;

                            if (!powerUps.ContainsKey(PowerUpType.LockOwner) || factory.GetTotalArmyAmount() > 10)
                            {
                                if (powerUp == PowerUpType.Fortify)
                                    UsePowerUp(PowerUpType.Fortify, factory, map.PowerUpsDurationSec);
                            }
                            else if (powerUp == PowerUpType.LockOwner)
                                UsePowerUp(PowerUpType.LockOwner, factory, map.PowerUpsDurationSec);
                        }
                        break;

                    case PowerUpType.RemovePowerUps:
                        foreach (var factory in factories)
                        {
                            if (factory.IsPowerUpApplied() && factory.GetAppliedPowerUpType() == PowerUpType.StopProduction)
                            {
                                UsePowerUp(PowerUpType.RemovePowerUps, factory, map.PowerUpsDurationSec);
                                break;
                            }
                        }
                        break;

                    case PowerUpType.Reset:
                        foreach (var attackingNode in attackingNodes)
                        {
                            if (attackingNode.GetTotalArmyAmount() >= difficultyConstants.MinimalArmyAmountToUseReset)
                            {
                                UsePowerUp(PowerUpType.Reset, attackingNode as Node, map.PowerUpsDurationSec);
                                break;
                            }
                        }
                        break;

                    case PowerUpType.StopProduction:
                        var target = map.GetCashedResources().FactoryNodes().FirstOrDefault(x => x.HasOwner() && x.GetOwner() != this);
                        if (target != null && !target.IsPowerUpApplied())
                            UsePowerUp(PowerUpType.StopProduction, target, map.PowerUpsDurationSec);
                        break;
                }
            }

            #if AIDEBUG
            watch.Stop();
            float passedTime = (float) watch.ElapsedMilliseconds/1000.0f;
            float maxCalcTime = (float) difficultyConstants.UpdateLogicEverySec/Constants.GameSpeed;
            UnityEngine.Debug.Log(string.Format("Passed real time: {0}, out of {1}", passedTime, maxCalcTime));
            #endif
        }

        private void performDefenseAction(NodeBase factory, int approxAttackingUnits, List<FactoryNode> nodesThatCantBeUsedForReinforcement)
        {
            int unitsGoingThere = 0;

            foreach (var link in map.GetCashedResources().Links())
            {
                var armiesGointThere = link.GetArmies().Where(x => x.Path.Last() == factory && x.Owner == this);
                foreach (var army in armiesGointThere)
                    unitsGoingThere += army.Amount;
            }

            foreach (var node in map.GetCashedResources().NodesAndInheritedFromNodes())
            {
                if (node.GetArmies().Count(x => x.Path.Count > 0 && x.Path.Last() == factory && x.Owner == this) == 0)
                    continue;
                var armiesGoingThere = node.GetArmies().Where(x => x.Path.Count > 0 && x.Path.Last() == factory && x.Owner == this);
                foreach (var army in armiesGoingThere)
                    unitsGoingThere += army.Amount;
            }
        
            int unitsAmountRequired = difficultyConstants.RequiredArmyAmountForDefense - factory.GetArmyAmountWithoutOrders() -
                                      unitsGoingThere + approxAttackingUnits;

            if (unitsAmountRequired < 1)
                return;

            List<KeyValuePair<NodeBase, int>> weightedNodes = map.GetCashedResources().GetWeightedNodesFrom(factory as FactoryNode);

            foreach (var weightedNode in weightedNodes)
            {
                if (nodesThatCantBeUsedForReinforcement.Count(x => x == weightedNode.Key) > 0 || 
                    !weightedNode.Key.HasOwner() ||
                    weightedNode.Key.GetOwner() != this ||
                    weightedNode.Key.GetArmyAmountWithoutOrders() == 0)
                    continue;

                int percentageToSend = 0;
                if (unitsAmountRequired > weightedNode.Key.GetArmyAmountWithoutOrders())
                {
                    unitsAmountRequired -= weightedNode.Key.GetArmyAmountWithoutOrders();
                    percentageToSend = 100;
                }
                else
                {
                    percentageToSend = (int)((float)unitsAmountRequired / ((float)weightedNode.Key.GetArmyAmountWithoutOrders() / 100.0f));
                    unitsAmountRequired = 0;
                }
            
                weightedNode.Key.OrderFreeArmyToMove(factory, percentageToSend);

                if (unitsAmountRequired <= 0)
                    break;
            }
        }

        private NodeBase performAttackAction(FactoryNode factory, bool isSafe)
        {
            List<KeyValuePair<NodeBase, int>> distanceToAllNodesFromThisFactory = map.GetCashedResources().GetWeightedNodesFrom(factory);

            float lowestDifficulty = float.MaxValue;
            NodeBase leastDiffNode = null;
            List<ArmyLocationPointBase> pathToLeastDifficultNode = null;

            PathConstructor constructor = new PathConstructor();
            foreach (var nodeWeightPair in distanceToAllNodesFromThisFactory)
            {
                NodeBase node = nodeWeightPair.Key;
                if ((node.HasOwner() && node.GetOwner() == this))
                    continue;

                if (!(node is FactoryNode) && !(node is PowerUpNode && !(node as PowerUpNode).IsPowerUpTaken()))
                    continue;

                var path = constructor.GenerateRoute(factory, nodeWeightPair.Key, distanceToAllNodesFromThisFactory);
                float difficulty = 0;
                if (node is FactoryNode)
                {
                    difficulty = calculateTakingOverDifficulty(nodeWeightPair.Key as Node, path);
                }
                else
                {
                    difficulty = calculatePowerUpNodeTakingOverDifficulty(nodeWeightPair.Key as PowerUpNode, path);
                }

                if (difficulty < lowestDifficulty)
                {
                    lowestDifficulty =  difficulty;
                    leastDiffNode = nodeWeightPair.Key;
                    pathToLeastDifficultNode = path;
                }
            }

            int attackingPercentage = isSafe
                ? difficultyConstants.SafeNodeSendToAttackPercentage
                : difficultyConstants.UnSafeNodeSendToAttackPercentage;

            if (leastDiffNode != null)
            {
                //if sending units to attack and path contains factory node of AI- send there
                FactoryNode friendlyFactoryOnTheWay = null;
                for (int i = pathToLeastDifficultNode.Count - 2; i >= 1; --i)
                {
                    if (pathToLeastDifficultNode[i] is FactoryNode)
                    {
                        FactoryNode factoryOnTheWay = pathToLeastDifficultNode[i] as FactoryNode;
                        if (factoryOnTheWay.HasOwner() && factoryOnTheWay.GetOwner() == this)
                        {
                            friendlyFactoryOnTheWay = factoryOnTheWay;
                            break;
                        }
                    }
                }

                if (friendlyFactoryOnTheWay != null)
                    factory.OrderFreeArmyToMove(friendlyFactoryOnTheWay, attackingPercentage);
                else
                {
                    factory.OrderFreeArmyToMove(leastDiffNode, attackingPercentage);
                    return leastDiffNode;
                }
            }

            return null;
        }

        private List<NodeBase> getMyNodes()
        {
            List<NodeBase> nodes = map.GetCashedResources().NodesAndInheritedFromNodes().Where(x => x.HasOwner() && x.GetOwner() == this).ToList();
            return nodes;
        }

        private List<FactoryNode> getMyFactoryNodes()
        {
            var factories = map.GetCashedResources().FactoryNodes().Where(x => x.HasOwner() && x.GetOwner() == this).ToList();
            return factories;
        }

        //return f nodes that are at least 1.5 times fore far than closest to other player f node
        private List<Node> getMySafeFactoryNodes()
        {
            var factories = getMyFactoryNodes();

            int cloesetDistToEnemyBetweenAllFactories = int.MaxValue;
            List<KeyValuePair<Node, int>> factoriesWeightedByHowCloseTheyAreToEnemy = new List<KeyValuePair<Node, int>>();

            foreach (var factory in factories)
            {
                List<KeyValuePair<NodeBase, int>> weightedNodes = map.GetCashedResources().GetWeightedNodesFrom(factory);

                int distToClosestEnemy = int.MaxValue;

                foreach (var weightedNode in weightedNodes)
                {
                    if (weightedNode.Key.HasOwner() &&
                        weightedNode.Key.GetOwner() != this &&
                        distToClosestEnemy > weightedNode.Value)
                        distToClosestEnemy = weightedNode.Value;
                }

                if (cloesetDistToEnemyBetweenAllFactories > distToClosestEnemy)
                    cloesetDistToEnemyBetweenAllFactories = distToClosestEnemy;

                factoriesWeightedByHowCloseTheyAreToEnemy.Add(new KeyValuePair<Node, int>(factory, distToClosestEnemy));
            }

            List<Node> safeNodes = new List<Node>();

            foreach (var factoryWeightPair in factoriesWeightedByHowCloseTheyAreToEnemy)
            {
                if (factoryWeightPair.Value >= cloesetDistToEnemyBetweenAllFactories * safeNodeDistanceRate)
                    safeNodes.Add(factoryWeightPair.Key);
            }

            return safeNodes;
        }

        private List<KeyValuePair<FactoryNode, int>> getFactoryNodesUnderAttack()
        {
            List<FactoryNode> factories = getMyFactoryNodes();
            List<KeyValuePair<FactoryNode, int>> factoriesUnderAttackAndAttackingCount = new List<KeyValuePair<FactoryNode, int>>();

            foreach (var factory in factories)
            {
                int attackingArmyAmount = 0;
                
                var enemyArmies = new List<ArmyRange>();
                foreach (var link in factory.GetLinks())
                    enemyArmies.AddRange(link.GetArmies().Where(x => x.Owner != this));
                foreach (var army in enemyArmies)
                    attackingArmyAmount += army.Amount;


                if (attackingArmyAmount > 0)
                    factoriesUnderAttackAndAttackingCount.Add(new KeyValuePair<FactoryNode, int>(factory, attackingArmyAmount)); //attackingArmyAmount
            
            }

            return factoriesUnderAttackAndAttackingCount;
        }

        private float calculateTakingOverDifficulty(Node defender, List<ArmyLocationPointBase> pathToDefender)
        {
            if (defender.HasOwner() && defender.GetOwner() == this)
                return 0;
            if (isAnyOfPathNodesAreLockedFromTakingOver(pathToDefender))
                return float.MaxValue;

            int distanceToDefender = 0;
            int enemyArmyToDefender = 0;
            foreach (var pathPiece in pathToDefender)
            {
                if (pathPiece is Link)
                {
                    var link = pathPiece as Link;
                    distanceToDefender += link.GetLinkLenght();

                    var enemyArmies = new List<ArmyRange>();
                    enemyArmies.AddRange(link.GetArmies().Where(x => x.Owner != this));
                    foreach (var army in enemyArmies)
                        enemyArmyToDefender += army.Amount;

                }
                else if (pathPiece is Node)
                {
                    Node node = pathPiece as Node;
                    if (!node.HasOwner() || node.GetOwner() != this)
                        enemyArmyToDefender += node.GetTotalArmyAmount();
                }
            }

            float timeToNodeSec = distanceToDefender / (float)Constants.UnitSpeedPerSecond;

            var spawnedUnitsByTheTimeOfComing = (defender is FactoryNode && defender.HasOwner() ? timeToNodeSec / Constants.FactoryNodeTimeToSpawnOneUnitInSeconds : 0);
            if (defender.IsPowerUpApplied() && defender.GetAppliedPowerUpType() == PowerUpType.StopProduction)
                spawnedUnitsByTheTimeOfComing = 0;

            float approxNeedUnitsToTakeOver = enemyArmyToDefender + spawnedUnitsByTheTimeOfComing;

            if (defender.IsPowerUpApplied() && defender.GetAppliedPowerUpType() == PowerUpType.Fortify)
                //include defender army for the second time
                approxNeedUnitsToTakeOver += defender.GetTotalArmyAmount();

            int ownerMultiplyer = defender.HasOwner() ? 3 : 1;
            float difficulty = timeToNodeSec * ownerMultiplyer + approxNeedUnitsToTakeOver;
            
            return difficulty;
        }

        private float calculatePowerUpNodeTakingOverDifficulty(PowerUpNode defender, List<ArmyLocationPointBase> pathToDefender)
        {
            float baseDifficulty = calculateTakingOverDifficulty(defender, pathToDefender);

            if (defender.IsPowerUpTaken())
                return float.MaxValue;

            float difficulty = baseDifficulty * 3;
            return difficulty;
        }

        private bool isAnyOfPathNodesAreLockedFromTakingOver(List<ArmyLocationPointBase> path)
        {
            foreach (var pathPiece in path)
            {
                if (pathPiece is Node)
                {
                    Node node = pathPiece as Node;
                    if (node.IsPowerUpApplied() && node.GetAppliedPowerUpType() == PowerUpType.LockOwner && node.HasOwner() && node.GetOwner() != this)
                        return true;
                }
            }
            return false;
        }
    }

    public enum AIDifficulty
    {
        VeryEasy, Easy, Average, Difficult, VeryDifficult
    }

    public enum AIBehaviourPattern
    {
        AttackOnly, DefendOnly, AttackAndDefend
    }
}