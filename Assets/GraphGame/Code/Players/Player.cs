using System;
using System.Collections.Generic;
using UnityEngine;

namespace GraphGame.Models
{
    [Serializable]
    public class Player
    {
        public string Name;
        public SerializebleColor SerializebleColor = new SerializebleColor();

        protected Dictionary<PowerUpType, int> powerUps;

        [NonSerialized]
        private List<IPlayerListener> listeners;

        public Player(string name, Color color)
        {
            Name = name;
            SerializebleColor.SetColor(color);

            powerUps = new Dictionary<PowerUpType, int>();
        }

        public virtual void Update(float elapsedms)
        {
        }

        public Dictionary<PowerUpType, int> GetPowerUps()
        {
            return powerUps;
        }

        public virtual void AddPowerUp(PowerUpType powerUp, int count = 1)
        {
            if (powerUps.ContainsKey(powerUp))
                powerUps[powerUp] += 1;
            else
                powerUps.Add(powerUp, count);

            if (listeners != null)
                foreach (var listener in listeners)
                    listener.OnPlayerPowerUpsChanged(powerUps);
        }

        public virtual void UsePowerUp(PowerUpType powerUp, Node targetNode, int powerUpDuration = 0)
        {
            if (!powerUps.ContainsKey(powerUp))
                Debug.LogError("You cant use this power up!");

            targetNode.ApplyPowerUp(powerUp, powerUpDuration);

            powerUps[powerUp]--;
            if (powerUps[powerUp] <= 0)
                powerUps.Remove(powerUp);
             
            if (listeners != null)
                foreach (var listener in listeners)
                    listener.OnPlayerPowerUpsChanged(powerUps);
        }

        public void AddListener(IPlayerListener listener)
        {
            if (listeners == null)
                listeners = new List<IPlayerListener>();
            if (!listeners.Contains(listener))
                listeners.Add(listener);
        }
    }
}