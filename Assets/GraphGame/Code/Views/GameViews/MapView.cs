using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GraphGame.Controllers;
using GraphGame.Models;
using Vectrosity;

namespace GraphGame.Views
{
    public class MapView
    {
        protected CameraScript CameraScriptInstance;
        protected GameMap map;
        protected GameObject mapHolder;
        protected List<LocationPointViewBase> instantiatedViews;

        public MapView(GameMap map)
        {
            this.map = map;
            CameraScriptInstance = CameraScript.GetInstance();
            mapHolder = new GameObject("MapView");
            instantiatedViews = new List<LocationPointViewBase>();
            buildMapView();

            if (CameraScriptInstance != null)
            {
                if (map.StartLocations.Count > 0 && map.StartLocations[0].FactoryNodes.Count > 0)
                    CameraScriptInstance.Focus(map.StartLocations[0].FactoryNodes[0].GetCoordinate());
                else
                {
                    var anyNode = (Node) map.ArmyLocationPoints.FirstOrDefault(x => x is Node);
                    if (anyNode != null)
                        CameraScriptInstance.Focus(anyNode.GetCoordinate());
                }

                CameraScriptInstance.ResetZoom();
            }

            determineCameraBordersForMovement();
        }

        public CameraScript GetCameraController()
        {
            return CameraScriptInstance;
        }

        public virtual void RebuildView()
        {
            //check if new points added.
            foreach (ArmyLocationPointBase mapPoint in map.ArmyLocationPoints)
            {
                if (instantiatedViews.AsEnumerable().Count(x => x.GetSource() == mapPoint) == 0)
                {
                    var view = createSingleView(mapPoint);
                    instantiatedViews.Add(view);
                }
            }

            //check if some Points are deleted
            for (int i = instantiatedViews.Count - 1; i >= 0; --i)
            {
                var currentView = instantiatedViews[i];
                if (map.ArmyLocationPoints.AsEnumerable().Count(x => x == currentView.GetSource()) == 0)
                {
                    //location point for this view dont exist, remove view
                    currentView.OnViewRemoval();
                    GameObject.Destroy(currentView.gameObject);
                    instantiatedViews.RemoveAt(i);
                }
            }
        }

        public virtual void DestroyMap()
        {
            for (int i = instantiatedViews.Count - 1; i >= 0; --i)
                instantiatedViews[i].OnViewRemoval();
            GameObject.Destroy(mapHolder);
            instantiatedViews.Clear();

            if (CameraScriptInstance != null)
                CameraScriptInstance.RemoveBorders();
        }

        private void buildMapView()
        {
            foreach (var point in map.ArmyLocationPoints)
            {
                var instantiatedView = createSingleView(point);
                instantiatedViews.Add(instantiatedView);
            }
        }

        protected LocationPointViewBase createSingleView(ArmyLocationPointBase point)
        {
            LocationPointViewBase instantiatedLocationPointView = null;

            bool pointIsNode = false;

            if (point.GetType() == typeof (Node))
            {
                var node = point as Node;
                instantiatedLocationPointView = createSpecifiedView(node);
                pointIsNode = true;
            }
            else if (point.GetType() == typeof (FactoryNode))
            {
                var factoryNode = point as FactoryNode;
                instantiatedLocationPointView = createSpecifiedView(factoryNode);
                pointIsNode = true;
            }
            else if (point.GetType() == typeof(PowerUpNode))
            {
                var powerUpNode = point as PowerUpNode;
                instantiatedLocationPointView = createSpecifiedView(powerUpNode);
                pointIsNode = true;
            }
            else if (point.GetType() == typeof (Link))
            {
                instantiatedLocationPointView = createSpecifiedView(point as Link);
            }
            else
            {
                Debug.LogError("Such armyLocationPoint type is not allowed");
            }
            

            instantiatedLocationPointView.transform.parent = mapHolder.transform;

            if (pointIsNode)
            {
                var node = instantiatedLocationPointView.GetSource() as Node;

                instantiatedLocationPointView.transform.position = new Vector3(node.GetCoordinate().x, node.GetCoordinate().y,
                    instantiatedLocationPointView.transform.position.z);
            }

            return instantiatedLocationPointView;
        }

        protected virtual NodeView createSpecifiedView(Node node)
        {
            var nodeView = GameObject.Instantiate(UISettings.GetInstance().NodeViewPrefab);
            nodeView.Init(node);
            return nodeView;
        }

        protected virtual FactoryNodeView createSpecifiedView(FactoryNode factoryNode)
        {
            var factoryNodeView = GameObject.Instantiate(UISettings.GetInstance().FactoryNodeViewPrefab);
            factoryNodeView.Init(factoryNode);
            return factoryNodeView;
        }

        protected virtual PowerUpNodeView createSpecifiedView(PowerUpNode powerUpNode)
        {
            var powerUpNodeView = GameObject.Instantiate(UISettings.GetInstance().PowerUpNodeView);
            powerUpNodeView.Init(powerUpNode);
            return powerUpNodeView;
        }

        protected virtual LocationPointViewBase createSpecifiedView(Link link)
        {
            var viewHolder = new GameObject();
            var linkView = viewHolder.AddComponent<LinkView>();
            linkView.Init(link);
            viewHolder.name = "Link";
            return linkView;
        }

        protected virtual void determineCameraBordersForMovement()
        {
            if (CameraScriptInstance == null)
                return;

            var nodes = map.ArmyLocationPoints.Where(x => x is Node);
            float mostLeftPoint, mostRightPoint, mostTopPoint, mostDownPoint;
            mostLeftPoint = mostRightPoint = mostTopPoint = mostDownPoint = 0;

            bool init = true;
            foreach (var node in nodes)
            {
                var coordinate = (node as Node).GetCoordinate();
                if (init)
                {
                    mostLeftPoint = mostRightPoint = coordinate.x;
                    mostTopPoint = mostDownPoint = coordinate.y;
                    init = false;
                    continue;
                }

                if (mostLeftPoint > coordinate.x)
                    mostLeftPoint = coordinate.x;
                if (mostRightPoint < coordinate.x)
                    mostRightPoint = coordinate.x;

                if (mostDownPoint > coordinate.y)
                    mostDownPoint = coordinate.y;
                if (mostTopPoint < coordinate.y)
                    mostTopPoint = coordinate.y;
            }

            CameraScriptInstance.SetBorders(new Vector2(mostLeftPoint, mostDownPoint),
                new Vector2(mostRightPoint, mostTopPoint));
        }
    }
}