using UnityEngine;
using System.Collections;
using GraphGame.Models;

namespace GraphGame.Views
{
    public class PowerUpNodeView : NodeView
    {
        public SpriteRenderer PickablePowerUpIcon;

        protected PowerUpNode powerUpNode;

        public override void Init(Node node)
        {
            base.Init(node);
            powerUpNode = node as PowerUpNode;
            onActivePowerUpChanged(node, powerUpNode.GetPowerUpType());
        }

        protected override void onPowerUpRemoved(Node node)
        {
            PickablePowerUpIcon.sprite = null;
            base.onPowerUpRemoved(node);
        }
    }
}