using UnityEngine;
using System.Collections;
using GraphGame.Models;

namespace GraphGame.Views
{
    public class FactoryNodeView : NodeView
    {
        protected FactoryNode factory;

        public override void Init(Node node)
        {
            factory = node as FactoryNode;
            base.Init(node);
        }
    }
}