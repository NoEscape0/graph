using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame;
using GraphGame.Controllers;
using GraphGame.Models;
using Vectrosity;

namespace GraphGame.Views
{
    public class NodeView : LocationPointViewBase
    {
        public SpriteRenderer CurrentPowerUpSprite;
        public TextMesh TextMesh;
        public GameObject PowerUpTimeLeftHolder;

        protected Node sourceNode;

        private LineWithMovingTexture orderArmyMoveLine;
        private LineWithMovingTexture waypointLine;

        private bool HUDforThisNodeIsShown = false;
        private bool moveCommandStarted = false;
        
        public virtual void Init(Node node)
        {
            sourceNode = node;

            CurrentPowerUpSprite.sprite = null;
            PowerUpTimeLeftHolder.SetActive(false);

            //color
            var newColor = node.HasOwner() ? node.GetOwner().SerializebleColor.GetColor() : UISettings.GetInstance().NoOwnerNodeColor;
            adjustColor(node, newColor, false);
            
            onUnitsAmountChanged(node, node.GetTotalArmyAmount());
            initLocationPointView(node);

            node.OnPowerUpChanged += delegate(Node n, PowerUpType t)
            { MainThreadController.GetInstance().DoFromMainThread(delegate { onActivePowerUpChanged(n,t); }); }; 

            node.OnPowerUpRemoved += delegate(Node n)
            { MainThreadController.GetInstance().DoFromMainThread(delegate { onPowerUpRemoved(n); }); };

            node.OnNodeUnitsCountChanged += delegate(Node n, int c)
            { MainThreadController.GetInstance().DoFromMainThread(delegate { onUnitsAmountChanged(n, c); }); };

            node.OnWaypointSettedUp += delegate(Node n1, Node n2)
            { MainThreadController.GetInstance().DoFromMainThread(delegate { onWaypointSettedUp(n1, n2); }); };

            node.OnWaypointRemoved += delegate(Node n)
            { MainThreadController.GetInstance().DoFromMainThread(delegate { onWaypointRemoved(n); }); };

            node.OnOwnerChanged += delegate(Node n, Player p1, Player p2)
            { MainThreadController.GetInstance().DoFromMainThread(delegate { onOwnerChanged(n, p1, p2); }); };

            node.OnPowerUpTimeLeftUpdated += delegate(Node n, float left, int time)
            { MainThreadController.GetInstance().DoFromMainThread(delegate { onPowerUpTimeLeftUpdated(n, left, time); }); };
        }

        protected void onActivePowerUpChanged(Node node, PowerUpType newPowerUpType)
        {
            CurrentPowerUpSprite.sprite = UISettings.GetInstance().GetPowerUpSprite(newPowerUpType);
        }

        protected virtual void onPowerUpRemoved(Node node)
        {
            PowerUpTimeLeftHolder.SetActive(false);
            CurrentPowerUpSprite.sprite = null;
        }

        private void onPowerUpTimeLeftUpdated(Node node, float timeLeft, int maxTime)
        {
            PowerUpTimeLeftHolder.SetActive(timeLeft > 0);
            if (timeLeft > 0)
                PowerUpTimeLeftHolder.transform.localScale = new Vector3(timeLeft / (float)maxTime, 1,1);
        }

        protected virtual void onUnitsAmountChanged(Node node, int newAmount)
        {
            if (TextMesh != null)
                TextMesh.text = node.GetTotalArmyAmount().ToString();
        }

        private void adjustColor(Node node, Color color, bool slowly = true)
        {
            var newColor = color;

            if (slowly)
            {
                foreach (var spriteRenderer in ColorChangingSpriteRenderers)
                    changeColorSlowly(spriteRenderer, newColor);
            }

            else
            {
                foreach (var spriteRenderer in ColorChangingSpriteRenderers)
                    spriteRenderer.color = newColor;
            }
        }

        private void onWaypointSettedUp(Node node, Node otherNode)
        {
            if (waypointLine == null)
            {
                var waypointLineGameObject = new GameObject("WaypointLine");
                waypointLine = waypointLineGameObject.AddComponent<LineWithMovingTexture>();
                waypointLine.Init(UISettings.GetInstance().WaypointLineColor, UISettings.GetInstance().WaypointLineineMaterial, 5f, 13);
            }

            waypointLine.SetStartPoint(node.GetCoordinate());
            waypointLine.SetEndPoint(otherNode.GetCoordinate());
        }
        private void onWaypointRemoved(Node node)
        {
            if (waypointLine == null)
                return;
            waypointLine.SetStartPoint(Vector2.zero);
            waypointLine.SetEndPoint(Vector2.zero);
        }

        private void onOwnerChanged(Node node, Player oldOwner, Player newOwner)
        {
            adjustColor(node, newOwner.SerializebleColor.GetColor());
        }

        public List<Vector3> GetSpriteBounds()
        {
            Vector3 min = ColorChangingSpriteRenderers[0].bounds.min;
            Vector3 max = ColorChangingSpriteRenderers[0].bounds.max;

            foreach (var spriteRenderer in ColorChangingSpriteRenderers)
            {
                if (min.x > spriteRenderer.bounds.min.x && min.y > spriteRenderer.bounds.min.y)
                    min = spriteRenderer.bounds.min;

                if (max.x < spriteRenderer.bounds.max.x && max.y < spriteRenderer.bounds.max.y)
                    max = spriteRenderer.bounds.max;
            }

            List<Vector3> bounds = new List<Vector3>() {min, max};
            return bounds;
        }

        protected override void Update()
        {
            if (UIController.GetInstance().IsMouseOverUI())
            {
                moveCommandStarted = false;
                destroyArmyMovingLine();
                return;
            }

            //hide hud on random click on map
            if (Input.GetMouseButtonDown(0) && HUDforThisNodeIsShown)
            {
                GameSessionController.GetInstance().HideHUDForNode();
                HUDforThisNodeIsShown = false;
            }

            //updating sendingarmy orderArmyMoveLine and finishing sending army
            if (GameSessionController.GetInstance().IsMoveCommandCreationInProgress() && moveCommandStarted)
            {
                var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                if (Input.GetMouseButton(0))
                    //update orderArmyMoveLine
                    orderArmyMoveLine.SetEndPoint(mousePos.x, mousePos.y);
                else
                {
                    Node nodeUnderCursor = null;
                    if (nodeIsUnderCursor(out nodeUnderCursor))
                        GameSessionController.GetInstance().FinishCreatingMoveCommandTo(nodeUnderCursor);
                    else
                        GameSessionController.GetInstance().CancelMoveCommandCreation();

                    //remove orderArmyMoveLine
                    destroyArmyMovingLine();
                }
            }

            base.Update();
        }

        protected void OnDestroy()
        {
            destroyArmyMovingLine();
            if (waypointLine != null)
            {
                Destroy(waypointLine);
                waypointLine.Destroy();
            }
            base.OnViewRemoval();
        }
        
        protected override void OnMouseDown()
        {
            CameraScript.GetInstance().EnableCameraMoveAndZoom = false;

            //if not awaiting to set auto sending units destination - start normal move command
            if (!GameSessionController.GetInstance().IsAutoSendingUnitsStartSettedUp() &&
                GameSessionController.GetInstance().TryStartCreatingMoveCommandFrom(sourceNode))
            {
                //create orderArmyMoveLine
                var lineObj = new GameObject("SendArmyLine");
                orderArmyMoveLine = lineObj.AddComponent<LineWithMovingTexture>();
                orderArmyMoveLine.Init(UISettings.GetInstance().SendArmyLineColor, UISettings.GetInstance().OrderArmyMoveLineMaterial);
                orderArmyMoveLine.SetStartPoint(sourceNode.GetCoordinate().x, sourceNode.GetCoordinate().y);
                moveCommandStarted = true;
            }

            base.OnMouseDown();
        }

        protected override void OnMouseUp()
        {
            CameraScript.GetInstance().EnableCameraMoveAndZoom = true;

            //awaiting to set auto sending units destination
            if (GameSessionController.GetInstance().IsAutoSendingUnitsStartSettedUp())
            {
                GameSessionController.GetInstance().SetAutoSendingUnitsFinish(GetSource() as Node);
                GameSessionController.GetInstance().ApplyAutoSendingUnitsCommand();
            }
            else
            {
                //Show HUD
                Node nodeUnderCursor = null;
                if (!HUDforThisNodeIsShown && nodeIsUnderCursor(out nodeUnderCursor) && GetSource() == nodeUnderCursor && 
                    !UIController.GetInstance().IsMouseOverUI() && GameSessionController.GetInstance().IsSessionCreated())
                {
                    HUDforThisNodeIsShown = true;
                    GameSessionController.GetInstance().ShowHUDForNode(sourceNode);
                }
            }

            base.OnMouseUp();
        }

        protected virtual bool nodeIsUnderCursor(out Node hittedNode)
        {
            hittedNode = null;

            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var hits = Physics2D.RaycastAll(mousePos, Vector2.zero);

            foreach (var hit in hits)
            {
                var view = hit.collider.gameObject.GetComponent<NodeView>();
                if (view != null && view.GetSource() is Node)
                {
                    hittedNode = view.GetSource() as Node;
                    break;
                }
            }

            if (hittedNode != null)
                return true;

            return false;
        }

        private void destroyArmyMovingLine()
        {
            moveCommandStarted = false;
            if (orderArmyMoveLine != null)
            {
                Destroy(orderArmyMoveLine.gameObject);
                orderArmyMoveLine.Destroy();
            }
        }
    }
}