using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GraphGame.Models;

namespace GraphGame.Views
{
    public class LinkView : LocationPointViewBase
    {
        protected Link link;

        public void Init(Link link)
        {
            this.link = link;
            link.OnLinkCapacityChanged += onLinkCapacityChanged;

            if (ColorChangingSpriteRenderers == null)
                ColorChangingSpriteRenderers = new List<SpriteRenderer>();

            var start = this.link.Connections.AsEnumerable().First();
            var startPoint = start.GetCoordinate();

            var finish = this.link.Connections.AsEnumerable().Last();
            var finishPoint = finish.GetCoordinate();

            var startMargin = (start is FactoryNode)
                ? UISettings.GetInstance().FactoryNodeMarginInUnits
                : UISettings.GetInstance().NodeMarginInUnits;

            var finishMargin = (finish is FactoryNode)
                ? UISettings.GetInstance().FactoryNodeMarginInUnits
                : UISettings.GetInstance().NodeMarginInUnits;

            //http://ru.stackoverflow.com/questions/499716/%D0%9A%D0%B0%D0%BA-%D0%BD%D0%B0%D0%B9%D1%82%D0%B8-%D1%82%D0%BE%D1%87%D0%BA%D1%83-%D0%BD%D0%B0-%D0%BE%D1%82%D1%80%D0%B5%D0%B7%D0%BA%D0%B5
            var fullLenght = Vector2.Distance(startPoint, finishPoint);
            var startWithMargin = startPoint + (finishPoint - startPoint)*(startMargin / fullLenght);
            var finishWithMargin = finishPoint + (startPoint - finishPoint)*(finishMargin / fullLenght);
            var linkPiecesAmount = this.link.GetLinkLenght();

            for (int i = 1; i <= linkPiecesAmount; ++i)
            {
                float lambda = (float) ((float) linkPiecesAmount - (float) i)/((float) i - 0.5f);
                var mPoint = getPoint(startWithMargin, finishWithMargin, lambda);

                var linkPiece = createLinkPiece();

                //pos
                linkPiece.transform.position = new Vector3(mPoint.x, mPoint.y, UISettings.GetInstance().LinkPosZ);

                //rotation
                var deltaY = startWithMargin.y - finishWithMargin.y;
                var deltaX = startWithMargin.x - finishWithMargin.x;
                var angleInDegrees = Mathf.Atan(deltaY/deltaX)*180/Mathf.PI;
                linkPiece.transform.eulerAngles = new Vector3(0,0, angleInDegrees);

                //parent
                linkPiece.transform.parent = transform;
                ColorChangingSpriteRenderers.Insert(0, linkPiece.GetComponent<SpriteRenderer>());
            }
            
            initLocationPointView(link);
        }

        protected virtual void onLinkCapacityChanged(Link thisLink, LinkPieceCapacity capacity)
        {
            //foreach all link pieces
            foreach (var spriteRenderer in ColorChangingSpriteRenderers)
                spriteRenderer.sprite = UISettings.GetInstance().GetLinkPieceSprite(link.GetLinkPiecesCapcity());
        }

        protected virtual GameObject createLinkPiece()
        {
            GameObject linkPiece = new GameObject("linkPiece");

            //image
            var spriteRenderer = linkPiece.AddComponent<SpriteRenderer>();
            spriteRenderer.sprite = UISettings.GetInstance().GetLinkPieceSprite(link.GetLinkPiecesCapcity());
            spriteRenderer.color = UISettings.GetInstance().EmptyLinkColor;
            return linkPiece;
        }

        private Vector2 getPoint(Vector2 start, Vector2 end, float ratio)
        {
            //http://mathprofi.ru/delenie_otrezka_v_dannom_otnoshenii.html
            float Xm = (start.x + ratio*end.x)/(1 + ratio);
            float Ym = (start.y + ratio*end.y)/(1 + ratio);
            return new Vector2(Xm, Ym);
        }

        protected override void Update()
        {
            var armies = link.GetArmies();
            Color newColor = new Color();

            for (int i = 0; i < link.GetLinkLenght(); ++i)
            {
                if (!armies.Exists(x => x.DoesRangeCollidesWithIndex(i)) && link.GetLinkLenght() > 3)
                {
                    if (i == 0 && armies.Exists(x => x.DoesRangeCollidesWithIndex(1)))
                        newColor = armies.First(x => x.DoesRangeCollidesWithIndex(1)).Owner.SerializebleColor.GetColor();
                    else if (i == link.GetLinkLenght() - 1 && armies.Exists(x => x.DoesRangeCollidesWithIndex(link.GetLinkLenght() - 2)))
                        newColor = armies.First(x => x.DoesRangeCollidesWithIndex(link.GetLinkLenght() - 2)).Owner.SerializebleColor.GetColor();
                    else if (i < ColorChangingSpriteRenderers.Count)
                        newColor = UISettings.GetInstance().EmptyLinkColor;
                }
                else
                {
                    var army = armies.First(x => x.DoesRangeCollidesWithIndex(i));
                    newColor = army.Owner.SerializebleColor.GetColor();
                }

                changeColorSlowly(ColorChangingSpriteRenderers[i], newColor);
            }

            base.Update();
        }
    }
}