using UnityEngine;
using System.Collections;
using GraphGame.Controllers;
using GraphGame.Models;

namespace GraphGame.Views
{
    public class EditorPowerUpNodeView : PowerUpNodeView
    {
        public override void Init(Node node)
        {
            node.OnNodeUnitsStartSpawnRangeChanged += onNodeUnitsStartSpawnRangeChanged;

            onNodeUnitsStartSpawnRangeChanged(node, node.GetStartUnitsGenerationRange());
            base.Init(node);
        }

        protected override void onUnitsAmountChanged(Node node, int newAmount)
        {
        }

        private void onNodeUnitsStartSpawnRangeChanged(Node node, IntRange range)
        {
            TextMesh.text = (range.Min != range.Max) ? string.Format("{0}-{1}", range.Min, range.Max) : range.Min.ToString();
        }
    }
}