using UnityEngine;
using System.Collections;
using GraphGame.Models;

namespace GraphGame.Views
{
    public class EditorLinkView : LinkView
    {
        private static Pool<GameObject> pool;
        private static GameObject poolObjectsHolder;

        public void DeleteLinkPiecesToPool()
        {
            if (pool == null)
                createPool();

            foreach (var spriteRenderer in ColorChangingSpriteRenderers)
                pool.AddToPool(spriteRenderer.gameObject);

            Destroy(this.gameObject);
        }

        protected override GameObject createLinkPiece()
        {
            if (pool != null && pool.PoolObjectsCount() > 0)
            {
                var fromPool = pool.GetFromPool();
                fromPool.GetComponent<SpriteRenderer>().sprite = UISettings.GetInstance().GetLinkPieceSprite(link.GetLinkPiecesCapcity());
                return fromPool;
            }

            return base.createLinkPiece();
        }

        private void createPool()
        {
            poolObjectsHolder = new GameObject("LinkPiecePool");

            pool = new Pool<GameObject>(delegate(GameObject poolObject)
            {
                //deactivate
                poolObject.SetActive(false);
                poolObject.transform.parent = poolObjectsHolder.transform;
            },
                delegate(GameObject poolObject)
                {
                    //activate
                    poolObject.SetActive(true);
                });
        }
    }
}