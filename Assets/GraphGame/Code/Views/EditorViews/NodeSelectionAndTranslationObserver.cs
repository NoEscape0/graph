using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GraphGame.Controllers;
using GraphGame.Models;

namespace GraphGame.Views
{
    public class NodeSelectionAndTranslationObserver
    {
        private bool dragging = false;
        private Vector3 mouseDownPos;
        private Vector3 draggingStartPos;
        private bool isMouseDownThisNode = false;
        private bool isMouseUpThisNode = false;
        private bool mouseWasReleasedSinceSelection = false;
        private Node node;

        public NodeSelectionAndTranslationObserver(NodeView observedObject)
        {
            node = (Node)observedObject.GetSource();

            observedObject.OnMouseButtonDownThisObject += onMouseButtonDownThisObject;
            observedObject.OnMouseButtonUpThisObject += onMouseButtonUpThisObject;
            observedObject.OnUpdate += onUpdate;

            /*
            observedObject.OnDestroyed += delegate
            {
                observedObject.OnMouseButtonDownThisObject -= onMouseButtonDownThisObject;
                observedObject.OnMouseButtonUpThisObject -= onMouseButtonUpThisObject;
                observedObject.OnUpdate -= onUpdate;
            };
            */
        }

        
        private void onMouseButtonDownThisObject()
        {
            CameraScript.GetInstance().EnableCameraMoveAndZoom = false;

            isMouseDownThisNode = true;
            isMouseUpThisNode = false;
            mouseDownPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        private void onMouseButtonUpThisObject()
        {
            isMouseUpThisNode = true;
            isMouseDownThisNode = false;
            CameraScript.GetInstance().EnableCameraMoveAndZoom = true;
        }

        private void onUpdate(float elapsedMs)
        {
            if (UIController.GetInstance().IsMouseOverUI())
            {
                dragging = false;
                return;
            }

            if (isMouseDownThisNode)
            {
                if (!EditorSessionController.GetInstance().IsNodeSelected(node))
                {
                    if (!Input.GetKey(Constants.EditorHotKeys.SelectMultipleItemsKey))
                        EditorSessionController.GetInstance().DeselectAll();

                    EditorSessionController.GetInstance().SelectNode(node);
                    mouseWasReleasedSinceSelection = false;
                }
            }

            //dragging
            if (dragging)
            {
                var mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                var delta = mouseWorldPos - draggingStartPos;

                EditorSessionController.GetInstance().TranslateSelectedNodes(delta);
                draggingStartPos = mouseWorldPos;
            }
            //check if start dragging
            else
            {
                var mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                bool nodeSelected = EditorSessionController.GetInstance().IsNodeSelected(node);
                if (mouseDownPos != mouseWorldPos && nodeSelected && isMouseDownThisNode)
                {
                    dragging = true;
                    draggingStartPos = mouseWorldPos;
                }
            }

            if (Input.GetMouseButtonDown(0))
                isMouseUpThisNode = false;

            //stop dragging OR diselect
            if (Input.GetMouseButtonUp(0))
            {
                if (dragging)
                {
                    dragging = false;
                    if (EditorSessionController.GetInstance().IsSnapToGridEnabled())
                        EditorSessionController.GetInstance().SnapSelectedNodesToGrid();
                }
                else if (isMouseUpThisNode && mouseWasReleasedSinceSelection)
                    EditorSessionController.GetInstance().DeselectNode(node);

                mouseWasReleasedSinceSelection = true;
            }
        }
    }
}