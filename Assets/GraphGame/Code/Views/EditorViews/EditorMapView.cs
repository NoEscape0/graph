using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GraphGame.Models;
using Vectrosity;

namespace GraphGame.Views
{
    public class EditorMapView : MapView
    {
        private List<VectorLine> activeSelections;
        private Pool<VectorLine> selectionLinesPool;

        private const float selectionBoxTextureScale = 4.0f;
        private const float selectionBoxTextureWidth = 4.0f;

        public EditorMapView(GameMap map) : base(map)
        {
            activeSelections = new List<VectorLine>();

            if (CameraScript.GetInstance() != null)
            {
                CameraScript.GetInstance().EnableCameraMoveAndZoom = true;
                CameraScript.GetInstance().EnableGrid = true;
            }

            selectionLinesPool = new Pool<VectorLine>(delegate(VectorLine line)
            {
                //hide vector line
                line.color = new Color(0, 0, 0, 0);
            },
            delegate(VectorLine line)
            {
                //show vector line
                line.color = Color.white;
            });
        }

        public override void RebuildView()
        {
            //check if some Points are moved
            for (int i = 0; i < instantiatedViews.Count; ++i)
            {
                var view = instantiatedViews[i];
                var source = view.GetSource();
                if (!(source is Node))
                    continue;

                var node = (Node) source;
                if (node.GetCoordinate() != new Vector2(view.transform.position.x, view.transform.position.y))
                {
                    view.transform.position = new Vector3(node.GetCoordinate().x, node.GetCoordinate().y, view.transform.position.z);

                    //recreate all links that node have
                    var links = node.GetLinks();
                    foreach (var link in links)
                    {
                        LocationPointViewBase linkLocationPointView = instantiatedViews.FirstOrDefault(x => x.GetSource() == link);
                        if (linkLocationPointView == null)
                            continue;
                        (linkLocationPointView as EditorLinkView).DeleteLinkPiecesToPool();
                        instantiatedViews.Remove(linkLocationPointView);
                        GameObject.Destroy(linkLocationPointView);

                        var newLinkView = createSingleView(link);
                        instantiatedViews.Add(newLinkView);
                    }
                }
            }

            //check if some factory nodes changed their startLocationGroups
            for (int i = 0; i < instantiatedViews.Count; ++i)
            {
                var view = instantiatedViews[i];
                if (!(view.GetSource() is FactoryNode))
                    continue;
                EditorFactoryNodeView factoryNodeView = (EditorFactoryNodeView) view ;
                factoryNodeView.OnLocationGroupChanged();
            }

            base.RebuildView();
        }

        public override void DestroyMap()
        {
            while (activeSelections.Count > 0)
            {
                var line = activeSelections[0];
                VectorLine.Destroy(ref line);
                activeSelections.RemoveAt(0);
            }

            while (selectionLinesPool.PoolObjectsCount() > 0)
            {
                var line = selectionLinesPool.GetFromPool();
                VectorLine.Destroy(ref line);
            }

            CameraScriptInstance.EnableGrid = false;

            base.DestroyMap();
        }

        public void DrawSelection(List<Node> selectedNodes)
        {
            RemoveSelectionBoxes();

            for (int i = 0; i < selectedNodes.Count; ++i)
            {
                VectorLine selection = (selectionLinesPool.PoolObjectsCount() > 0)
                    ? selectionLinesPool.GetFromPool()
                    : createLine();

                //draw selection
                var node = selectedNodes[i];
                NodeView nodeView = (NodeView) instantiatedViews.Single(x => x.GetSource() == node);
                var bounds = nodeView.GetSpriteBounds();
                selection.MakeRect(bounds[0], bounds[1]);
                selection.Draw3DAuto();
                activeSelections.Add(selection);
            }
        }

        public void RemoveSelectionBoxes()
        {
            while (activeSelections.Count > 0)
            {
                VectorLine line = activeSelections[0];
                activeSelections.RemoveAt(0);
                selectionLinesPool.AddToPool(line);
            }
        }

        private VectorLine createLine()
        {
            var selectionLine = new VectorLine("Selection", new Vector3[5],
                UISettings.GetInstance().SelectionBoxMaterial, selectionBoxTextureWidth, LineType.Continuous, Joins.Fill);
            selectionLine.textureScale = selectionBoxTextureScale;
            return selectionLine;
        }

        protected override LocationPointViewBase createSpecifiedView(Link link)
        {
            var viewHolder = new GameObject();
            var linkView = viewHolder.AddComponent<EditorLinkView>();
            linkView.Init(link);
            viewHolder.name = "EditorLink";
            return linkView;
        }

        protected override NodeView createSpecifiedView(Node node)
        {
            var nodeView = GameObject.Instantiate(UISettings.GetInstance().EditorNodeViewPrefab);
            nodeView.Init(node);
            new NodeSelectionAndTranslationObserver(nodeView);
            return nodeView;
        }

        protected override FactoryNodeView createSpecifiedView(FactoryNode factoryNode)
        {
            var factoryNodeView = GameObject.Instantiate(UISettings.GetInstance().EditorFactoryNodeViewPrefab);
            factoryNodeView.Init(factoryNode);
            new NodeSelectionAndTranslationObserver(factoryNodeView);
            return factoryNodeView;
        }

        protected override PowerUpNodeView createSpecifiedView(PowerUpNode powerUpNode)
        {
            var powerUpNodeView = GameObject.Instantiate(UISettings.GetInstance().EditorPowerUpNodeView);
            powerUpNodeView.Init(powerUpNode);
            new NodeSelectionAndTranslationObserver(powerUpNodeView);
            return powerUpNodeView;
        }

        protected override void determineCameraBordersForMovement()
        {
            if (CameraScriptInstance != null)
                CameraScriptInstance.RemoveBorders();
        }
    }
}