using UnityEngine;
using System.Collections;
using GraphGame.Controllers;
using GraphGame.Models;

namespace GraphGame.Views
{
    public class EditorFactoryNodeView : FactoryNodeView
    {
        public TextMesh OwnerTextMesh;

        public override void Init(Node node)
        {
            base.Init(node);
            node.OnNodeUnitsStartSpawnRangeChanged += onNodeUnitsStartSpawnRangeChanged;
            onNodeUnitsStartSpawnRangeChanged(node, node.GetStartUnitsGenerationRange());
        }

        public void OnLocationGroupChanged()
        {
            int locNum;
            if (EditorSessionController.GetInstance().IsFactoryNodeInAStartLocationGroup(factory, out locNum))
                OwnerTextMesh.text = "Group #" + locNum;
            else
                OwnerTextMesh.text = "";
        }

        private void onNodeUnitsStartSpawnRangeChanged(Node node, IntRange range)
        {
            OnLocationGroupChanged();
            TextMesh.text = (range.Min != range.Max) ? string.Format("{0}-{1}", range.Min, range.Max) : range.Min.ToString();
        }

        protected override void onUnitsAmountChanged(Node node, int newAmount)
        {
        }
    }
}