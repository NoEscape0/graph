using UnityEngine;
using System.Collections;
using GraphGame.Controllers;
using Vectrosity;

namespace GraphGame.Views
{
    public class LineWithMovingTexture : MonoBehaviour
    {
        private VectorLine line;
        private Vector3 originalPos;
        private float depth;

        public void Init(Color color, Material material, float lineScale = 1.0f, float lineWidth = 15.0f)
        {
            depth = UISettings.GetInstance().MoveCommandLineZPos;
            line = new VectorLine("LineWithMovingTexture", new Vector3[2], material, lineScale, LineType.Discrete, Joins.Weld);
            line.color = color;

            line.textureScale = lineScale;
            line.lineWidth = lineWidth;
            line.Draw3DAuto();
        }

        public void SetStartPoint(Vector2 point)
        {
            line.points3[0] = new Vector3(point.x, point.y, depth);
        }

        public void SetStartPoint(float x, float y)
        {
            line.points3[0] = new Vector3(x, y, depth);
        }

        public void SetEndPoint(float x, float y)
        {
            line.points3[1] = new Vector3(x, y, depth);
        }

        public void SetEndPoint(Vector2 point)
        {
            line.points3[1] = new Vector3(point.x, point.y, depth);
        }

        public void Hide()
        {
            line.points3[0] = line.points3[1] = new Vector3();
        }

        public void Destroy()
        {
            VectorLine.Destroy(ref line);
        }

        public VectorLine GetLine()
        {
            return line;
        }

        private void Update()
        {
            if (GameSessionController.GetInstance().IsSessionCreated() && GameSessionController.GetInstance().IsPaused())
                return;

            line.textureOffset = -Time.time*2.0f%1.0f;
        }
    }
}