using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GraphGame.Models;

namespace GraphGame.Views
{
    public abstract class LocationPointViewBase : MonoBehaviour, IObservableView
    {
        public List<SpriteRenderer> ColorChangingSpriteRenderers;
        public NoVariablesDelegate OnMouseButtonDownThisObject { get; set; }
        public NoVariablesDelegate OnMouseButtonUpThisObject { get; set; }
        public ElsapsedMsDelegate OnUpdate { get; set; }
        public NoVariablesDelegate OnDestroyed { get; set; }

        protected ArmyLocationPointBase source;
        private List<ColorChangeNote> colorChangeNotes = new List<ColorChangeNote>();

        protected void initLocationPointView(ArmyLocationPointBase sourceObject)
        {
            source = sourceObject;
        }

        public ArmyLocationPointBase GetSource()
        {
            if (source == null)
                Debug.LogError("Source for the view is NULL!");
            return source;
        }

        public virtual void OnViewRemoval()
        {
        }

        protected virtual void Update()
        {
            //change color
            for (int i = colorChangeNotes.Count - 1; i >= 0; --i)
            {
                Color lerpedColor = new Color();
                if (UISettings.GetInstance().ChangeColorTimeSec != 0)
                {
                    lerpedColor = Color.Lerp(colorChangeNotes[i].OldColor, colorChangeNotes[i].NewColor,
                        colorChangeNotes[i].TimeFromChangingColorStart/UISettings.GetInstance().ChangeColorTimeSec);
                }
                else
                    lerpedColor = colorChangeNotes[i].NewColor;

                colorChangeNotes[i].SpriteRenderer.color = lerpedColor;
                colorChangeNotes[i].TimeFromChangingColorStart += Time.deltaTime;

                if (lerpedColor == colorChangeNotes[i].NewColor)
                    colorChangeNotes.RemoveAt(i);
            }

            if (OnUpdate != null)
                OnUpdate(Time.deltaTime);
        }

        protected virtual void OnMouseDown()
        {
            if (OnMouseButtonDownThisObject != null)
                OnMouseButtonDownThisObject();
        }

        protected virtual void OnMouseUp()
        {
            if (OnMouseButtonUpThisObject != null)
                OnMouseButtonUpThisObject();
        }

        protected void changeColorSlowly(SpriteRenderer renderer, Color newColor)
        {
            if (renderer.color == newColor ||
                colorChangeNotes.Count(x => x.SpriteRenderer == renderer && x.NewColor == newColor) > 0)
                return;

            //same renderer but different color
            if (colorChangeNotes.Count(x => x.SpriteRenderer == renderer) > 0)
                colorChangeNotes.Remove(colorChangeNotes.Single(x => x.SpriteRenderer == renderer));

            colorChangeNotes.Add(new ColorChangeNote()
            {
                OldColor = renderer.color,
                NewColor = newColor,
                SpriteRenderer = renderer
            });
        }

        private void OnDestroy()
        {
            if (OnDestroyed != null)
                OnDestroyed();
        }

        private class ColorChangeNote
        {
            public SpriteRenderer SpriteRenderer;
            public Color OldColor;
            public Color NewColor;
            public float TimeFromChangingColorStart = 0;
        }
    }
}