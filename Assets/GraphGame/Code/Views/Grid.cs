using UnityEngine;
using System.Collections;
using Vectrosity;

namespace GraphGame.Views
{
    public class Grid
    {
        private VectorLine gridLine;
        private Camera cam;

        public Grid(Camera cam)
        {
            this.cam = cam;
        }

        public void UpdateGrid()
        {
            var vertExtent = cam.orthographicSize;
            var horzExtent = vertExtent*Screen.width/Screen.height;

            int height = (int) vertExtent*3;
            int width = (int) horzExtent*3;

            var offset =
                new Vector3(((int) (cam.transform.position.x - horzExtent*1.5f)/Constants.GridStep)*Constants.GridStep,
                    ((int) (cam.transform.position.y - vertExtent*1.5f)/Constants.GridStep)*Constants.GridStep);

            var numberOfGridPoints = ((width/Constants.GridStep + 1) + (height/Constants.GridStep + 1))*2;
            gridLine.Resize(numberOfGridPoints);

            var index = 0;
            for (var x = 0; x < width; x += Constants.GridStep)
            {
                gridLine.points3[index++] = new Vector3(x, 0);
                gridLine.points3[index++] = new Vector3(x, height - 1);
            }

            for (var y = 0; y < height; y += Constants.GridStep)
            {
                gridLine.points3[index++] = new Vector3(0, y);
                gridLine.points3[index++] = new Vector3(width - 1, y);
            }
            gridLine.rectTransform.position = offset;
            gridLine.Draw();
        }

        public bool IsShown()
        {
            return gridLine != null;
        }

        public void HideGrid()
        {
            if (IsShown())
            {
                VectorLine.Destroy(ref gridLine);
                gridLine = null;
            }
        }

        public void ShowGrid()
        {
            if (!IsShown())
                initGrid();
        }

        private void initGrid()
        {
            gridLine = new VectorLine("Grid", new Vector3[0], null, 1.0f);
            gridLine.Draw3DAuto();
            gridLine.rectTransform.anchoredPosition = new Vector3(.5f, .5f);
            gridLine.color = UISettings.GetInstance().GridColor;
            gridLine.lineWidth = 1;
            UpdateGrid();
        }
    }
}