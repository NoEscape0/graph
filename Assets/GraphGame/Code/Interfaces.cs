using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GraphGame.Models;

namespace GraphGame
{
    public delegate void ElsapsedMsDelegate(float elapsedMs);
    public delegate void NoVariablesDelegate();
    public delegate void OnSelectedNodesModifiedDelegate(List<Node> newSelection);

    public delegate void OnSomethingHappenedToNode(Node node);
    public delegate void OnPowerUpChanged(Node node, PowerUpType powerUpType);
    public delegate void OnWaypointSettedUp(Node node, Node destinationNode);
    public delegate void OnNodeUnitsCountChanged(Node node, int newVal);
    public delegate void OnNodeUnitsStartSpawnRangeChanged(Node node, IntRange newRange);
    public delegate void OnNodePowerUpTimeLeftChanged(Node node, float timeLeft, int maxTime);
    public delegate void OnOwnerChanged(Node node, Player oldOwner, Player newOwner);
    public delegate void OnLinkCapacityChanged(Link link, LinkPieceCapacity newLinkPieceCapacity);


    public interface IUpdateListener
    {
        void Update(float elapsedSec);
    }

    public interface IPlayerListener
    {
        void OnPlayerPowerUpsChanged(Dictionary<PowerUpType, int> powerUps);
    }

    public interface IObservable
    {
        ElsapsedMsDelegate OnUpdate { get; set; }
    }

    public interface IObservableNode : IObservable
    {
        OnSomethingHappenedToNode OnFriendlyUnitDied { get; set; }
        OnSomethingHappenedToNode OnPowerUpRemoved { get; set; }
        OnPowerUpChanged OnPowerUpChanged { get; set; }
        OnSomethingHappenedToNode OnWaypointRemoved { get; set; }
        OnWaypointSettedUp OnWaypointSettedUp { get; set; }
        OnNodeUnitsCountChanged OnNodeUnitsCountChanged { get; set; }
        OnSomethingHappenedToNode OnNodeDestroyed { get; set; }
    }

    public interface IObservableView : IObservable
    {
        NoVariablesDelegate OnMouseButtonDownThisObject { get; set; }
        NoVariablesDelegate OnMouseButtonUpThisObject { get; set; }
        NoVariablesDelegate OnDestroyed { get; set; }
    }
}