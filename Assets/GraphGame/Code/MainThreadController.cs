using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Object = UnityEngine.Object;

namespace GraphGame.Controllers
{
    public class MainThreadController : MonoBehaviour
    {
        private EditorSessionController editorSessionController;
        private GameSessionController gameSessionController;

        private List<NoVariablesDelegate> doFromMainThreadActions = new List<NoVariablesDelegate>();
        private object locker = new Object();

        private static MainThreadController instance;

        public static MainThreadController GetInstance()
        {
            return instance;
        }

        public void DoFromMainThread(NoVariablesDelegate action)
        {
            if (Constants.GetInstance().RunMovementLogicInSeparateThread)
            {
                lock (locker)
                {
                    doFromMainThreadActions.Add(action);
                }
            }
            else
            {
                action();
            }
        }
        public MainThreadController()
        {
            instance = this;
        }

        private void Awake()
        {
            instance = this;

            new Constants();

            editorSessionController = new EditorSessionController();
            gameSessionController = new GameSessionController();
        }

        private void Update()
        {
            try
            {
                editorSessionController.Update(Time.deltaTime * Constants.GameSpeed);
                gameSessionController.Update(Time.deltaTime * Constants.GameSpeed);
            }
            catch (Exception e)
            {
                InGameLogger.GetInstance().Log("Error: " + e.Message);
                throw e;
            }
            

            lock (locker)
            {
                if (doFromMainThreadActions.Count > 0)
                {
                    var actionsList = doFromMainThreadActions.ToList();
                    foreach (var actionFromMainThread in actionsList)
                        actionFromMainThread();

                    doFromMainThreadActions.RemoveAll(x => actionsList.Contains(x));
                }
            }
        }
    }
}
