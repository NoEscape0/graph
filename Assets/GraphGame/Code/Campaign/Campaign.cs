using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using GraphGame.Views;

namespace GraphGame.Models
{
    [Serializable]
    public class Campaign : MonoBehaviour
    {
        public List<CampaignActionFigure> ActionFigures = new List<CampaignActionFigure>();
        public List<CampaignMission> Misisons = new List<CampaignMission>();
    }

    [Serializable]
    public class CampaignMission
    {
        public TextAsset Map;
        public List<CampaignDialogLine> StartMapDialog = new List<CampaignDialogLine>();
        public List<CampaignDialogLine> FinishMapDialog = new List<CampaignDialogLine>();
    }

    [Serializable]
    public class CampaignActionFigure
    {
        public string Name;
        public Color Color;
    }

    [Serializable]
    public class CampaignDialogLine
    {
        public CampaignDialogLine()
        {
        }

        public CampaignDialogLine(string authorName, string text, bool append = false, float waitSeconds = 0)
        {
            AuthorName = authorName;
            WaitSeconds = waitSeconds;
            AppendToPreviewsText = append;
            Text = text;
        }

        public string AuthorName;
        public Color Color;
        public float WaitSeconds;
        public bool AppendToPreviewsText;
        public string Text;
    }
}