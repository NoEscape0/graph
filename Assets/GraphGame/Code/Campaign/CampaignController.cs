using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using GraphGame.Models;
using GraphGame.Views;

namespace GraphGame.Controllers
{
    [System.Serializable]
    public class CampaignController : MonoBehaviour
    {
        public Campaign CampaignPrefab;
        private Campaign instantiatedCampaign;

        public int CurrentMissionIndex {
            get { return PlayerPrefs.GetInt(ReflectionHelper.GetPropertyName(() => CurrentMissionIndex)); }
            set { PlayerPrefs.SetInt(ReflectionHelper.GetPropertyName(() => CurrentMissionIndex), value); }
        }

        private static CampaignController instance;
        
        public bool IsCampaignCompleted()
        {
            return CurrentMissionIndex == instantiatedCampaign.Misisons.Count;
        }

        public void RunMissionWithThisMap(TextAsset mapAsset)
        {
            runMission(instantiatedCampaign.Misisons.Single(x => x.Map == mapAsset));
        }

        public void ContinueCompain()
        {
            runMission(instantiatedCampaign.Misisons[CurrentMissionIndex]);
        }

        public void runMission(CampaignMission mission)
        {
            UIController.GetInstance().CampaignConsolePanel.SetUpAndShow(mission.StartMapDialog, delegate
            {
                //on dialog shown
                UIController.GetInstance().PerformActionWithLoadingScreen(delegate
                {
                    var mapAsset = mission.Map;
                    GameSessionController.GetInstance().CreateCampaignSession(mapAsset);
                    UIController.GetInstance().GamePanel.Show();
                });
            });
        }

        public void CompleteMap(TextAsset mapAsset)
        {
            if (IsCampaignCompleted())
                return;

            var currentMission = instantiatedCampaign.Misisons[CurrentMissionIndex];
            if (currentMission.Map == mapAsset)
                CurrentMissionIndex++;

            UIController.GetInstance().CampaignConsolePanel.SetUpAndShow(currentMission.FinishMapDialog, delegate
            {
                UIController.GetInstance().PlayerWonPanel.SetUpAndShow(!IsCampaignCompleted());
            });
        }
        
        public static CampaignController GetInstance()
        {
            return instance;
        }

        private void Awake()
        {
            instance = this;
            instantiatedCampaign = Instantiate(CampaignPrefab);
        }
    }
}
