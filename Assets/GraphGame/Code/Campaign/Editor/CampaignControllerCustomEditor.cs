using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GraphGame.Controllers;
using GraphGame.Models;
using UnityEditor;

namespace GraphGame
{
    [System.Serializable]
    [CustomEditor(typeof(CampaignController))]
    public class CampaignControllerCustomEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            CampaignController campaignController = (CampaignController)target;
            EditorGUILayout.Space();
            if (campaignController.CampaignPrefab != null)
                editCurretMissionIndex(campaignController);
        }

        private void editCurretMissionIndex(CampaignController campaignController)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(string.Format("Current mission index: {0} / {1}  (Count: {2})",
                campaignController.CurrentMissionIndex, campaignController.CampaignPrefab.Misisons.Count - 1, campaignController.CampaignPrefab.Misisons.Count));

            if (GUILayout.Button("-"))
            {
                var index = campaignController.CurrentMissionIndex;

                index--;
                if (index < 0)
                    index = 0;

                campaignController.CurrentMissionIndex = index;
            }
            if (GUILayout.Button("+"))
            {
                var index = campaignController.CurrentMissionIndex;

                index++;
                if (index >= campaignController.CampaignPrefab.Misisons.Count)
                    index = campaignController.CampaignPrefab.Misisons.Count - 1;

                campaignController.CurrentMissionIndex = index;
            }

            EditorGUILayout.EndHorizontal();
        }
    }
}