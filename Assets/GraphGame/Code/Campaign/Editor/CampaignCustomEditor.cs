using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GraphGame.Controllers;
using GraphGame.Models;
using UnityEditor;

namespace GraphGame
{
    [CustomEditor(typeof (Campaign))]
    public class CampaignCustomEditor : Editor
    {
        SerializedProperty lookAtPoint;

        void OnEnable()
        {
            lookAtPoint = serializedObject.FindProperty("lookAtPoint");
        }

        public override void OnInspectorGUI()
        {
            Campaign campaign = (Campaign)target;
            EditorStyles.textField.wordWrap = true;

            EditorGUILayout.Space();
            editActionFigures(campaign);
            EditorGUILayout.Space();
            editMissions(campaign);

            EditorUtility.SetDirty(campaign);
        }
        
        private void editActionFigures(Campaign campaign)
        {
            EditorGUILayout.LabelField("====== Action Figures ======");
            foreach (var actionFigure in campaign.ActionFigures)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Name", GUILayout.MaxWidth(60));
                actionFigure.Name = EditorGUILayout.TextField("", actionFigure.Name, GUILayout.MinWidth(60)); //TODO при переименовке все референсы менять тоже (name & color)
                actionFigure.Color = EditorGUILayout.ColorField(actionFigure.Color); //TODO
                if (GUILayout.Button("-", GUILayout.MinWidth(30)))
                {
                    campaign.ActionFigures.Remove(actionFigure);
                    return;
                }
                EditorGUILayout.EndHorizontal();
            }
            
            if (GUILayout.Button("Add new"))
            {
                campaign.ActionFigures.Add(new CampaignActionFigure() {Color = Color.gray, Name = "No Name"});
            }
        }

        private void editMissions(Campaign campaign)
        {
            EditorGUILayout.LabelField("====== Missions ======");

            for (int i = 0; i < campaign.Misisons.Count; ++i)
            {
                var mission = campaign.Misisons[i];

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("#" + i.ToString(), GUILayout.MaxWidth(20));
                mission.Map = EditorGUILayout.ObjectField(mission.Map, typeof(TextAsset)) as TextAsset;
                if (GUILayout.Button("-"))
                {
                    campaign.Misisons.Remove(mission);
                    return;
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.LabelField("Start Dialog");
                ViewDialog(mission.StartMapDialog, campaign);

                EditorGUILayout.Space();

                EditorGUILayout.LabelField("Finish Dialog");
                ViewDialog(mission.FinishMapDialog, campaign);

                DrawSeparator();
            }

            if (GUILayout.Button("Add new mission"))
            {
                campaign.Misisons.Add(new CampaignMission());
            }
        }

        private void ViewDialog(List<CampaignDialogLine> dialog, Campaign campaign)
        {
            for (int i = 0; i < dialog.Count; ++i)
            {
                var dialogLine = dialog[i];

                //Author
                int selected = -1;
                if (campaign.ActionFigures.SingleOrDefault(x => x.Name == dialogLine.AuthorName) != null)
                    selected = campaign.ActionFigures.FindIndex(x => x.Name == dialogLine.AuthorName);

                List<string> options = new List<string>();
                foreach (var actionFigure in campaign.ActionFigures)
                    options.Add(actionFigure.Name);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(i + " Author", GUILayout.MaxWidth(70));
                var newSelection = EditorGUILayout.Popup("", selected, options.ToArray(), GUILayout.MaxWidth(100));
                if (newSelection != selected)
                {
                    dialogLine.AuthorName = campaign.ActionFigures[newSelection].Name;
                    dialogLine.Color = campaign.ActionFigures[newSelection].Color;
                }

                EditorGUILayout.LabelField("Append ", GUILayout.MaxWidth(50));
                dialogLine.AppendToPreviewsText = EditorGUILayout.Toggle(dialogLine.AppendToPreviewsText, GUILayout.MaxWidth(20));

                EditorGUILayout.LabelField("Await seconds", GUILayout.MaxWidth(90));
                dialogLine.WaitSeconds = EditorGUILayout.FloatField(dialogLine.WaitSeconds, GUILayout.MaxWidth(30));
                if (dialogLine.WaitSeconds < 0)
                    dialogLine.WaitSeconds = 0;

                
                if (GUILayout.Button("- Remove -"))
                {
                    dialog.Remove(dialogLine);
                    return;
                }

                if (GUILayout.Button("+ Insert +"))
                {
                    dialog.Insert(i + 1, new CampaignDialogLine(null, ""));
                    return;
                }
                EditorGUILayout.EndHorizontal();
                
                dialogLine.Text = EditorGUILayout.TextArea(dialogLine.Text, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            }

            if (GUILayout.Button("++Add Line++", GUILayout.MaxWidth(100)))
            {
                dialog.Add(new CampaignDialogLine(null, "NoText"));
            }
        }

        private void DrawSeparator()
        {
            GUIStyle styleHR = new GUIStyle(GUI.skin.box);
            styleHR.stretchWidth = true;
            styleHR.fixedHeight = 2;
            GUILayout.Box("", styleHR);
        }
    }
}