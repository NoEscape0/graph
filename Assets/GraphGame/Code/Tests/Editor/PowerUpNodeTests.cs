using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using GraphGame.Controllers;
using GraphGame.Models;
using NUnit.Framework;
using UnityEngine;

namespace GraphGame.Tests
{
    [TestFixture]
    [Category("PowerUpNodeTests")]
    public class PowerUpNodeTests
    {
        Player bluePlayer = new Player("BluePlayer", Color.blue);
        Player redPlayer = new Player("RedPlayer", Color.red);

        [Test]
        public void Fortify()
        {
            Node A, B;
            applyPowerUpToBNodeAndLetAAttackB(PowerUpType.Fortify, 50, 50, out A, out B);

            if (B.GetTotalArmyAmount() != 25 || A.GetTotalArmyAmount() != 0 || A.GetOwner() == B.GetOwner())
                Assert.Fail();
        }

        [Test]
        public void StopProduction()
        {
            //A--B
            var factory  = new FactoryNode(new Vector2(0, 0));
            factory.SetOwner(bluePlayer);
            
            var stepMs = 40;

            factory.ApplyPowerUp(PowerUpType.StopProduction);
            var startArmy = factory.GetTotalArmyAmount();

            float maxTestMovingTimeMs = 120 * 1000.0f;
            var movingTimeMs = 0;
            while (movingTimeMs < maxTestMovingTimeMs)
            {
                factory.Update(stepMs);
                movingTimeMs += stepMs;
            }

            if (factory.GetTotalArmyAmount() != startArmy)
                Assert.Fail();
        }

        [Test]
        public void Reset()
        {
            Node A, B;
            applyPowerUpToBNodeAndLetAAttackB(PowerUpType.Reset, 50, 50, out A, out B);

            if (B.GetTotalArmyAmount() != 50 || A.GetTotalArmyAmount() != 0 || A.GetOwner() != B.GetOwner())
                Assert.Fail();
        }

        [Test]
        public void LockOwner()
        {
            new MainThreadController();
            //A--B
            FactoryNode A = new FactoryNode(new Vector2(0, 0));
            FactoryNode B = new FactoryNode(new Vector2(0, 10));
            A.ConnectTo(B);
            List<ArmyLocationPointBase> locationPoints = new List<ArmyLocationPointBase>() { A, B, A.GetLinkWith(B) };

            var stepMs = 40;

            A.SetOwner(redPlayer);
            B.SetOwner(bluePlayer);

            A.SetArmyAmount(100);
            B.SetArmyAmount(50);
            B.ApplyPowerUp(PowerUpType.LockOwner);

            A.OrderArmyToMove(B, 100);

            float maxTestMovingTimeMs = 120 * 1000.0f;
            var movingTimeMs = 0;
            while (movingTimeMs < maxTestMovingTimeMs)
            {
                foreach (var point in locationPoints)
                    point.Update(stepMs);
                movingTimeMs += stepMs;
            }

            if (A.GetOwner() == B.GetOwner())
                Assert.Fail();
        }

        [Test]
        public void RemovePowerUpsTest()
        {
            new MainThreadController();
            //A--B
            var A = new Node(new Vector2(0, 0));
            var B = new Node(new Vector2(0, 10));
            A.ConnectTo(B);
            List<ArmyLocationPointBase> locationPoints = new List<ArmyLocationPointBase>() { A, B, A.GetLinkWith(B) };

            var stepMs = 40;

            A.SetOwner(redPlayer);
            B.SetOwner(bluePlayer);

            A.SetArmyAmount(50);
            B.SetArmyAmount(50);
            B.ApplyPowerUp(PowerUpType.Fortify);
            B.ApplyPowerUp(PowerUpType.RemovePowerUps);

            A.OrderArmyToMove(B, 100);

            float maxTestMovingTimeMs = 120 * 1000.0f;
            var movingTimeMs = 0;
            while (movingTimeMs < maxTestMovingTimeMs)
            {
                foreach (var point in locationPoints)
                    point.Update(stepMs);
                movingTimeMs += stepMs;
            }

            if (B.GetTotalArmyAmount() != 0 || A.GetTotalArmyAmount() != 0 || A.GetOwner() == B.GetOwner())
                Assert.Fail();
        }

        private void applyPowerUpToBNodeAndLetAAttackB(PowerUpType powerUp, int AnodeArmy, int BnodeArmy, out Node A, out Node B)
        {
            new MainThreadController();
            //A--B
            A = new Node(new Vector2(0, 0));
            B = new Node(new Vector2(0, 10));
            A.ConnectTo(B);
            List<ArmyLocationPointBase> locationPoints = new List<ArmyLocationPointBase>() { A, B, A.GetLinkWith(B) };

            var stepMs = 40;

            A.SetOwner(redPlayer);
            B.SetOwner(bluePlayer);

            A.SetArmyAmount(AnodeArmy);
            B.SetArmyAmount(BnodeArmy);
            B.ApplyPowerUp(powerUp);

            A.OrderArmyToMove(B, 100);

            float maxTestMovingTimeMs = 120 * 1000.0f;
            var movingTimeMs = 0;
            while (movingTimeMs < maxTestMovingTimeMs)
            {
                foreach (var point in locationPoints)
                    point.Update(stepMs);
                movingTimeMs += stepMs;
            }
        }
    }
}
