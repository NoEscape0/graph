using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using GraphGame.Controllers;
using GraphGame.Models;
using NUnit.Framework;
using UnityEngine;

namespace GraphGame.Tests
{
    [TestFixture]
    [Category("Node")]
    public class NodeTests : Node
    {
        public NodeTests()
        {
        }

        public NodeTests(Vector2 coordinate) : base(coordinate)
        {
        }

        [Test]
        public void OwnerShouldBeNullOnCreation()
        {
            Node node = new Node();

            try
            {
                if (node.HasOwner() || node.GetOwner() != null)
                    Assert.Fail("Owner is not null");
            }
            catch (NoOwnerException)
            {
            }
            catch (Exception)
            {
                Assert.Fail();
            }

            Assert.Pass();
        }

        [Test]
        public void ArmyAmountOnCreation()
        {
            Node node = new Node();

            for (int i = 0; i < 1000; ++i)
            {
                node = new Node();
                node.OnSessionStarted();

                if (node.GetTotalArmyAmount() < Constants.NeutoralNodeSpawnArmyMinAmount ||
                    node.GetTotalArmyAmount() > Constants.NeutoralNodeSpawnArmyMaxAmount)
                {
                    Assert.Fail("Army amount on creation is not in the bounds of neutoral node. " +
                                node.GetTotalArmyAmount());
                    break;
                }
            }

            Assert.Pass();
        }

        /*
        
        [Test]
        public void ArmyMoveOrderUnitsSplitting()
        {
            NodeTests node = new NodeTests();

            NodeTests attackedNode = new NodeTests();
            node.ConnectTo(attackedNode);

            node.OrderArmyToMove(attackedNode, 50);

            //var freeArmyExist = node.GetArmies().Exists(x => x.Path.Count == 0);

            if (node.units.Count(x => x.Path.Count > 0) !=  percentageToAbsValue(50, node.units.Count))
                Assert.Fail("more or less units are ordered to move");

            node.cancelPreviousOrder();

            if (node.units.Count(x => x.Path.Count > 0) !=  0)
                Assert.Fail("not all units are staying after cancelling the order");

            Assert.Pass();
        }
        */ //TODO

        [Test]
        public void ConnectivityOfNodes()
        {
            NodeTests node = new NodeTests();
            NodeTests node2 = new NodeTests();

            if (node.GetConnectedNodes().Count != 0)
                Assert.Fail("On creation more then 0 connections");

            node.ConnectTo(node2);
            if (node.GetConnectedNodes().Count != 1 || !node.IsConnectedTo(node2))
                Assert.Fail("After connection to other node should be 1 connection");

            node.DisconnectFrom(node2);
            if (node.GetConnectedNodes().Count != 0 || node.IsConnectedTo(node2))
                Assert.Fail("After disconnection should be 0 connections");
        }

        [Test]
        public void PathFinding()
        {
            new MainThreadController();
            //A--B--C
            //xxx|xxx
            //D--E--F
            NodeTests A = new NodeTests(new Vector2(0, 0));
            NodeTests B = new NodeTests(new Vector2(0, 1));
            NodeTests C = new NodeTests(new Vector2(0, 2));
            NodeTests D = new NodeTests(new Vector2(2, 0));
            NodeTests E = new NodeTests(new Vector2(2, 1));
            NodeTests F = new NodeTests(new Vector2(2, 2));

            A.ConnectTo(B);
            B.ConnectTo(C);
            B.ConnectTo(E);
            E.ConnectTo(D);
            E.ConnectTo(F);

            PathConstructor constructor = new PathConstructor();

            if (constructor.GenerateRoute(A, D).Count != 7)
                Assert.Fail("Wrong map 1");

            if (constructor.GenerateRoute(A, C).Count != 5)
                Assert.Fail("Wrong map 2");

            if (constructor.GenerateRoute(F, B).Count != 5)
                Assert.Fail("Wrong map 3");

            if (constructor.GenerateRoute(E, F).Count != 3)
                Assert.Fail("Wrong map 4");

            B.DisconnectFrom(E);

            try
            {
                constructor.GenerateRoute(A, F);
                Assert.Fail("Nodes are not connected but still no Exception while generating path!");
            }
            catch (RouteConstructionError)
            {
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

        [Test]
        public void ArmyMovement()
        {
            //A--B
            NodeTests A = new NodeTests(new Vector2(0, 0));
            NodeTests B = new NodeTests(new Vector2(0, 10));
            A.ConnectTo(B);

            List<ArmyLocationPointBase> locationPoints = new List<ArmyLocationPointBase>() {A,B, A.GetLinks()[0]};

            A.OrderArmyToMove(B, 100);

            var originalPersInNodeA = A.GetTotalArmyAmount();
            var originalPersInNodeB = B.GetTotalArmyAmount();

            float linkLenght = A.GetLinks()[0].GetLinkLenght();
            float expectedMovingTimeMs = ((linkLenght + locationPoints.Count + A.GetTotalArmyAmount())
                / (float)Constants.UnitSpeedPerSecond) * 1000.0f;
            var movingTimeMs = 0;

            //test if units moved from node
            var stepMs = 30;
            
            while (movingTimeMs < expectedMovingTimeMs)
            {
                foreach (var point in locationPoints)
                    point.Update(stepMs);
                
                movingTimeMs += stepMs;
            }
            
            if (B.GetTotalArmyAmount() != originalPersInNodeA + originalPersInNodeB)
                Assert.Fail("Not all units made it to the B node");
        }
        
        [Test]
        public void ArmyMovement2()
        {
            NodeTests A = new NodeTests(new Vector2(0, 0));
            NodeTests B = new NodeTests(new Vector2(0, 10));
            NodeTests C = new NodeTests(new Vector2(0, 20));
            NodeTests D = new NodeTests(new Vector2(20, 0));
            NodeTests E = new NodeTests(new Vector2(20, 1));
            NodeTests F = new NodeTests(new Vector2(20, 20));

            A.ConnectTo(B);
            B.ConnectTo(C);
            B.ConnectTo(E);
            E.ConnectTo(D);
            E.ConnectTo(F);
            
            //A--B--C
            //xxx|xxx
            //D--E--F

            A.SetArmyAmount(10);
            E.SetArmyAmount(0);
            F.SetArmyAmount(10);

            List<Link> connectingLinks = new List<Link>();
            connectingLinks.AddRange(B.GetLinks());
            connectingLinks.AddRange(E.GetLinks());
            connectingLinks.Remove(E.GetLinkWith(B));
            List<ArmyLocationPointBase> locationPoints = new List<ArmyLocationPointBase>() { A, B, C, D, E, F };
             
            float totalHeight = 0;
            foreach (var link in connectingLinks)
            {
                totalHeight += link.GetLinkLenght();
                locationPoints.Add(link);
            }

            float expectedMovingTimeMs = ((totalHeight + locationPoints.Count + A.GetTotalArmyAmount())
                / (float)Constants.UnitSpeedPerSecond) * 1000.0f;
            var movingTimeMs = 0;

            //test if units moved from node
            var stepMs = 67;


            var originalPersInNodeA = A.GetTotalArmyAmount();
            var originalPersInNodeF = F.GetTotalArmyAmount();

            A.OrderArmyToMove(F, 100);

            while (movingTimeMs < expectedMovingTimeMs)
            {
                foreach (var point in locationPoints)
                    point.Update(stepMs);

                movingTimeMs += stepMs;
            }


            if (F.GetTotalArmyAmount() != originalPersInNodeA + originalPersInNodeF)
                Assert.Fail("Not all units made it to the B node. " + F.GetTotalArmyAmount() + "  out of: " + (originalPersInNodeA + originalPersInNodeF));
        }

        [Test]
        public void ArmyFight()
        {
            new Constants();
            new MainThreadController();

            //create players
            Player bluePlayer = new Player("BluePlayer", Color.blue);

            Player redPlayer = new Player("RedPlayer", Color.red);
            //A--B
            Node A = new Node(new Vector2(0, 0));
            Node B = new Node(new Vector2(0, 100));
            
            A.ConnectTo(B);

            List<ArmyLocationPointBase> locationPoints = new List<ArmyLocationPointBase>() { A, B, A.GetLinkWith(B) };

            var stepMs = 40;

            for (int i = 0; i < 2; ++i)
            {
                A.SetOwner(redPlayer);
                B.SetOwner(bluePlayer);
                A.SetArmyAmount(100);
                B.SetArmyAmount(10);
                A.OrderArmyToMove(B, 100);
                
                if (i == 1)
                    B.OrderArmyToMove(A, 100);                 

                float linkLenght = A.GetLinks()[0].GetLinkLenght();
                float expectedMovingTimeMs = ((linkLenght + locationPoints.Count + A.GetTotalArmyAmount()) / (float)Constants.UnitSpeedPerSecond) * 1000.0f;
                var movingTimeMs = 0;

                while (movingTimeMs < expectedMovingTimeMs)
                {
                    foreach (var point in locationPoints)
                        point.Update(stepMs);

                    movingTimeMs += stepMs;
                }
                
                if (B.GetTotalArmyAmount() != 90 || B.GetOwner() != redPlayer)
                    Assert.Fail();
            }
        }

        [Test]
        public void FactoryNode()
        {
            new MainThreadController(); 
            //create players
            Player bluePlayer = new Player("BluePlayer", Color.blue);
            FactoryNode A = new FactoryNode(new Vector2(0, 0));
            
            A.SetOwner(bluePlayer);
            A.SetArmyAmount(0);

            float timeToSpawnOneUnitMs = Constants.FactoryNodeTimeToSpawnOneUnitInSeconds * 1000;
            float msTillFull = timeToSpawnOneUnitMs*Constants.FactoryNodeStopSpawningAfterReachingNumber;

            float stepMs = 50;
            float spawningTimeMs = 0;

            while (spawningTimeMs < msTillFull + stepMs)
            {
                A.Update(stepMs);

                spawningTimeMs += stepMs;
            }
            
            if (A.GetTotalArmyAmount() != Constants.FactoryNodeStopSpawningAfterReachingNumber)
                Assert.Fail();
        }
    }
}
