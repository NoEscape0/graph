using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using GraphGame.Models;
using NUnit.Framework;
using UnityEngine;

namespace GraphGame.Tests
{
    [TestFixture]
    [Category("Link")]
    internal class LinkTests
    {
        [Test]
        public void DefaultLinkStateOnCreation()
        {
            Node node1 = new Node(new Vector2(0, 1));
            Node node2 = new Node(new Vector2(2, 2));

            Link link = new Link(node1, node2);
            var connectedNodes = link.Connections.AsEnumerable();

            if (!connectedNodes.Contains(node1) || !connectedNodes.Contains(node2))
                Assert.Fail("Nodes dont say they are connected");
            
            Assert.Pass();
        }
    }
}