using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using GraphGame.Models;
using NUnit.Framework;
using UnityEngine;

namespace GraphGame.Tests
{
    [TestFixture]
    [Category("SaveLoadMap")]
    public class GameMapTests
    {
        [Test]
        public void SaveLoadForNodes()
        {
            Node A = new Node(new Vector2(0, 0));
            Node B = new Node(new Vector2(0, 10));
            Node C = new Node(new Vector2(0, 20));
            Node D = new Node(new Vector2(20, 0));
            Node E = new Node(new Vector2(20, 1));
            Node F = new Node(new Vector2(20, 20));

            A.ConnectTo(B);
            B.ConnectTo(C);
            B.ConnectTo(E);
            E.ConnectTo(D);
            E.ConnectTo(F);

            //A--B--C
            //xxx|xxx
            //D--E--F

            A.SetArmyAmount(0);
            E.SetArmyAmount(0);
            F.SetArmyAmount(0);

            List<Link> connectingLinks = new List<Link>();
            connectingLinks.AddRange(B.GetLinks());
            connectingLinks.AddRange(E.GetLinks());
            connectingLinks.Remove(E.GetLinkWith(B));
            List<ArmyLocationPointBase> locationPoints = new List<ArmyLocationPointBase>() { A, B, C, D, E, F };

            GameMap map = new GameMap();
            map.ArmyLocationPoints = locationPoints;

            GameMap loadedMap = saveAndLoad(map);
            
            if (loadedMap.ArmyLocationPoints.Count != map.ArmyLocationPoints.Count)
                Assert.Fail();

            if (loadedMap.StartLocations.Count != map.StartLocations.Count)
                Assert.Fail();

            Assert.Pass();
        }

        [Test]
        public void SaveLoadForFactoryNodes()
        {
            FactoryNode A = new FactoryNode(new Vector2(0, 0));
            FactoryNode B = new FactoryNode(new Vector2(0, 10));

            A.ConnectTo(B);

            List<Link> connectingLinks = new List<Link>();
            connectingLinks.AddRange(A.GetLinks());
            List<ArmyLocationPointBase> locationPoints = new List<ArmyLocationPointBase>() { A, B };
            foreach (var link in connectingLinks)
                locationPoints.Add(link);

            GameMap map = new GameMap();
            map.ArmyLocationPoints = locationPoints;
            map.ClaimStartLocation(0, A);

            GameMap loadedMap = saveAndLoad(map);

            if (loadedMap == map)
                Assert.Fail();

            if (loadedMap.ArmyLocationPoints.Count != map.ArmyLocationPoints.Count)
                Assert.Fail();

            if (loadedMap.StartLocations.Count != map.StartLocations.Count)
                Assert.Fail();

            Assert.Pass();
        }

        [Test]
        public void SaveLoadForPowerUpNodes()
        {
            PowerUpNode A = new PowerUpNode(new Vector2(0, 0), PowerUpType.Reset);
            PowerUpNode B = new PowerUpNode(new Vector2(0, 10), PowerUpType.Reset);

            A.ConnectTo(B);

            List<Link> connectingLinks = new List<Link>();
            connectingLinks.AddRange(A.GetLinks());
            List<ArmyLocationPointBase> locationPoints = new List<ArmyLocationPointBase>() { A, B };
            foreach (var link in connectingLinks)
                locationPoints.Add(link);

            GameMap map = new GameMap();
            map.ArmyLocationPoints = locationPoints;

            GameMap loadedMap = saveAndLoad(map);

            if (loadedMap == map)
                Assert.Fail();
            
            if (loadedMap.StartLocations.Count != map.StartLocations.Count)
                Assert.Fail();

            Assert.Pass();
        }

        private GameMap saveAndLoad(GameMap map)
        {
            Constants.MapsAndScreenshotsPath = Application.persistentDataPath;
            string testMapName = "_testSaveLoadMap_";
            MapSaveLoadHelper.Save(map, testMapName);

            GameMap loadedMap = MapSaveLoadHelper.Load(testMapName);
            MapSaveLoadHelper.DeleteMap(testMapName);
            return loadedMap;
        }
    }
}