using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using GraphGame.Controllers;
using GraphGame.Models;
using NUnit.Framework;
using UnityEngine;

namespace GraphGame.Tests
{
    [TestFixture]
    [Category("AISettings")]
    public class AITests
    {
        private static string mapsForAiTestsFolderName = "/MapsForTests";

        private static string AIDifficultyTestMapName = "test_mediumMapForTwo";
        private static string AIRunningTimeTestMapName = "test_AiRunningTime";

        private static int maxInGameSecondsToFinishMap = 60*20; //20 minutes

        [Test]
        public void AIDifficultyTest()
        {
            float realTimeRunningTime;
            float runningTimeWithView;
            if (runMap(AIDifficultyTestMapName, out realTimeRunningTime, out runningTimeWithView))
            {
                Assert.Pass();
            }

            Assert.Fail();
        }

        [Test]
        public void AIRunningTimeTest()
        {
            float realTimeRunningTime;
            float runningTimeWithView;
            if (runMap(AIRunningTimeTestMapName, out realTimeRunningTime, out runningTimeWithView))
            {
                Assert.Pass("test running processor time: " + realTimeRunningTime + ";     would be with normal game speed: " + runningTimeWithView);
            }
            else
            {
                Assert.Fail();
            }
        }
        
        [Test]
        public void AIAverageRunningTimeTest()
        {
            float totalRealTime = 0;
            int numberOfTries = 10;

            for (int i = 0; i < numberOfTries; ++i)
            {
                float realTimeRunningTime;
                float runningTimeWithView;

                if (!runMap(AIRunningTimeTestMapName, out realTimeRunningTime, out runningTimeWithView))
                    Assert.Fail();

                totalRealTime += realTimeRunningTime;
            }

            var averageTime = totalRealTime/numberOfTries;
            
            Assert.Pass("Average real time: " + averageTime);
        }

        private bool runMap(string name, out float realTimeRunningTime, out float runningTimeWithView)
        {
            runningTimeWithView = -1;
            realTimeRunningTime = -1;
            Stopwatch watch = new Stopwatch();
            watch.Start();

            new Constants();
            Constants.MapsAndScreenshotsPath += mapsForAiTestsFolderName;
            var gameSessionController = new GameSessionController(true, true);
            var map = MapSaveLoadHelper.Load(name);

            for (int i = 0; i < map.StartLocations.Count; ++i)
                map.StartLocations[i].AIDifficulty = (i == 0) ? AIDifficulty.Average : AIDifficulty.Easy;

            gameSessionController.CreateSession(map);

            var stepSec = 1.0f / 60;
            int runTimeSec = maxInGameSecondsToFinishMap;

            for (float i = 0; i < runTimeSec; i += stepSec)
            {
                gameSessionController.Update(stepSec);

                if ((int)i % 10 == 0)
                {
                    //check that stronger player won
                    bool finished = true;
                    Player firstLocSetOwner = map.StartLocations[0].FactoryNodes.First().GetOwner();
                    //check if finished
                    var factoryNodes = map.ArmyLocationPoints.Where(x => x is FactoryNode);
                    foreach (var factoryPoint in factoryNodes)
                    {
                        var factory = factoryPoint as FactoryNode;
                        if (!factory.HasOwner() || factory.GetOwner() != firstLocSetOwner)
                        {
                            finished = false;
                            break;
                        }
                    }

                    if (finished)
                    {
                        runningTimeWithView = i;
                        watch.Stop();
                        realTimeRunningTime = (float)watch.Elapsed.TotalSeconds;
                        return true;
                    }
                }
            }

            return false;
        }
    }
}