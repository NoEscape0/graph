using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using GraphGame.Models;

namespace GraphGame
{
    public class Constants
    {
        public AISettings AiSettings = new AISettings();

        public const int NeutoralNodeSpawnArmyMaxAmount = 10;
        public const int NeutoralNodeSpawnArmyMinAmount = 5;

        public static float GameSpeed = 1.0f;
        public static int UnitSpeedPerSecond = 6; //6
        public static float FactoryNodeTimeToSpawnOneUnitInSeconds = 1.0f;
        public static int SendingArmyByWaypointTreshHold = 5;

        public const int FactoryNodeStopSpawningAfterReachingNumber = 100;

        public const int StartFactoryArmyAmount = 30;

        public const float LinkPiecesInOneUnityUnit = 0.7f;

        //Players
        public const string LocalPlayerName = "You";

        public const int FirstArmyMovePercentage = 25;
        public const int SecondArmyMovePercentage = 50;
        public const int ThirdArmyMovePercentage = 100;

        public static string MapsAndScreenshotsPath;

        //Editor
        public const int GridStep = 10;

        //Campaign
        public const float CampaignTypingDelay = 0.08f;

        //threading
        public bool RunMovementLogicInSeparateThread = false;
        public bool RunAILogicInAdditionalThread = false;

        //debug
        public bool ShowLog = true;

        public class EditorHotKeys
        {
            public const KeyCode SelectMultipleItemsKey = KeyCode.LeftShift;
            public const KeyCode DeselectAllKey = KeyCode.D;
            public const KeyCode DeleteSelectedKey = KeyCode.Delete;
        }

        public class GlobalHotKeys
        {
            public const KeyCode ScreenSHotKey = KeyCode.F12;
        }

        private static Constants instance;

        public Constants()
        {
            instance = this;
            if (Application.isEditor)
                MapsAndScreenshotsPath = "Assets/GraphGame/Maps";
            else
                MapsAndScreenshotsPath = Application.dataPath + "/Maps";
        }

        public static Constants GetInstance()
        {
            return instance;
        }
    }

    public class AISettings
    {
        public class AIDifficultyConstants
        {
            public AIDifficulty Difficulty;
            public float UpdateLogicEverySec = 2;
            public int MinimulArmyAmountToAttack = 5;
            public int SafeNodeSendToAttackPercentage = 100;
            public int UnSafeNodeSendToAttackPercentage = 70;
            public int RequiredArmyAmountForDefense = 30; //the higher this value the better AISettings at defending

            public int MinimalArmyAmountToUseReset = 40;
        }

        public List<Color> AIColors = new List<Color>()
        {
            new Color(244.0f/255f, 67.0f/255f, 54.0f/255f),
            new Color(81.0f/255f, 45.0f/255f, 168.0f/255f),
            new Color(25.0f/255f, 118.0f/255f, 210.0f/255f),
            new Color(0, 121.0f/255f, 107.0f/255f),
            new Color(213.0f/255f, 0, 0),
            new Color(253.0f/255f, 216.0f/255f, 53.0f/255f),
            new Color(244.0f/255f, 81.0f/255f, 30.0f/255f),
            new Color(109.0f/255f, 76.0f/255f, 65.0f/255f),
            new Color(84.0f/255f, 110.0f/255f, 122.0f/255f)
        };

        public readonly AISettings.AIDifficultyConstants Difficulty_VeryDifficult = new AIDifficultyConstants()
        {
            Difficulty = AIDifficulty.VeryDifficult,
            UpdateLogicEverySec = 2,
            MinimulArmyAmountToAttack = 5,
            SafeNodeSendToAttackPercentage = 100,
            UnSafeNodeSendToAttackPercentage = 70,
            RequiredArmyAmountForDefense = 50
        };

        public readonly AIDifficultyConstants Difficulty_Difficult = new AIDifficultyConstants()
        {
            Difficulty = AIDifficulty.Difficult,
            UpdateLogicEverySec = 2.5f,
            MinimulArmyAmountToAttack = 10,
            SafeNodeSendToAttackPercentage = 90,
            UnSafeNodeSendToAttackPercentage = 60,
            RequiredArmyAmountForDefense = 40
        };

        public readonly AIDifficultyConstants Difficulty_Average = new AIDifficultyConstants()
        {
            Difficulty = AIDifficulty.Average,
            UpdateLogicEverySec = 3,
            MinimulArmyAmountToAttack = 15,
            SafeNodeSendToAttackPercentage = 80,
            UnSafeNodeSendToAttackPercentage = 50,
            RequiredArmyAmountForDefense = 30,
        };

        public readonly AIDifficultyConstants Difficulty_Easy = new AIDifficultyConstants()
        {
            Difficulty = AIDifficulty.Easy,
            UpdateLogicEverySec = 4,
            MinimulArmyAmountToAttack = 20,
            SafeNodeSendToAttackPercentage = 70,
            UnSafeNodeSendToAttackPercentage = 40,
            RequiredArmyAmountForDefense = 20
        };

        public readonly AIDifficultyConstants Difficulty_VeryEasy = new AIDifficultyConstants()
        {
            Difficulty = AIDifficulty.VeryEasy,
            UpdateLogicEverySec = 5,
            MinimulArmyAmountToAttack = 35,
            SafeNodeSendToAttackPercentage = 60,
            UnSafeNodeSendToAttackPercentage = 30,
            RequiredArmyAmountForDefense = 10
        };
    }
}
