using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GraphGame.Models
{
    public class Pool<T>
    {
        public delegate void PoolAction(T poolObject);

        private PoolAction deactivateObjectAction;
        private PoolAction activateObjectAction;
        private List<T> objects = new List<T>();

        public Pool(PoolAction DeactivateObjectAction, PoolAction ActivateObjectAction)
        {
            deactivateObjectAction = DeactivateObjectAction;
            activateObjectAction = ActivateObjectAction;
        }

        public void AddToPool(T poolObj)
        {
            if (deactivateObjectAction != null)
                deactivateObjectAction(poolObj);

            objects.Add(poolObj);
        }

        public int PoolObjectsCount()
        {
            return objects.Count;
        }

        public T GetFromPool()
        {
            if (objects.Count == 0)
                throw new Exception("Pool is empty");

            var objectToReturn = objects.First();
            objects.Remove(objectToReturn);

            if (activateObjectAction != null)
                activateObjectAction(objectToReturn);

            return objectToReturn;
        }
    }
}