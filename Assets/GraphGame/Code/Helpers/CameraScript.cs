using System;
using UnityEngine;
using System.Collections;
using System.Threading;
using GraphGame.Controllers;
using Vectrosity;

namespace GraphGame.Views
{
    public class CameraScript : MonoBehaviour
    {
        public float MinOrtho = 30.0f;
        public float MaxOrtho = 100.0f;
        public float ZoomSpeed = 20;

        [HideInInspector]
        public bool EnableCameraMoveAndZoom = true;
        [HideInInspector]
        public bool EnableGrid = false;

        //moving
        private Vector3 draggingDelta;
        private bool isDragging;
        private Vector3 mousePosAtDragStart;
        private bool bordersEnabled = false;
        private Vector2 leftBotBorder, rightTopBorder;
        //zoom
        private Camera cam;
        private float defaultZoom;
        //grid
        private Grid grid;

        private static CameraScript instance;

        public static CameraScript GetInstance()
        {
            return instance;
        }

        public void Focus(Vector2 position)
        {
            transform.position = new Vector3(position.x, position.y, transform.position.z);
        }

        public void ResetZoom()
        {
            cam.orthographicSize = defaultZoom;
        }

        public void SetBorders(Vector2 leftBottomPoint, Vector2 rightTopPoint)
        {
            this.leftBotBorder = leftBottomPoint;
            this.rightTopBorder = rightTopPoint;
            bordersEnabled = true;
        }

        public void RemoveBorders()
        {
            bordersEnabled = false;
        }

        private void Awake()
        {
            cam = Camera.main;
            defaultZoom = cam.orthographicSize;
            grid = new Grid(cam);
            instance = this;
        }

        private void Update()
        {
            if (Input.GetKeyDown(Constants.GlobalHotKeys.ScreenSHotKey))
            {
                var path = Constants.MapsAndScreenshotsPath;
                var fileName = string.Format("Screenshot_{0}_{1}_{2}__{3}_{4}_{5}", DateTime.Now.Day, DateTime.Now.Month,
                    DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Millisecond);

                var fullName = path + "/" + fileName + ".png";
                Application.CaptureScreenshot(fullName, 2);

                StartCoroutine(showScreenshotTakenPopupAfterItsTaken(fullName));
            }

            if (EnableCameraMoveAndZoom && !UIController.GetInstance().IsMouseOverUI())
            {

                float scroll = Input.GetAxis("Mouse ScrollWheel");
                if (scroll != 0.0f)
                {
                    cam.orthographicSize -= scroll*ZoomSpeed;
                    cam.orthographicSize = Mathf.Clamp(cam.orthographicSize, MinOrtho, MaxOrtho);
                }

                /*
                if (Input.touchCount >= 2)
                {
                    Vector2 touch0, touch1;
                    float distance;
                    touch0 = Input.GetTouch(0).position;
                    touch1 = Input.GetTouch(1).position;
                    distance = Vector2.Distance(touch0, touch1);
                }
                */ //TODO

                //cam movement
                if (GameSessionController.GetInstance().IsSessionCreated() ||
                    EditorSessionController.GetInstance().IsSessionCreated() &&
                    (UIController.GetInstance().MapEditor.IsShown() || UIController.GetInstance().GamePanel.IsShown()))
                {
                    if (Input.GetMouseButton(0))
                    {
                        var mouseWorldPos = cam.ScreenToWorldPoint(Input.mousePosition);
                        draggingDelta = mouseWorldPos - cam.transform.position;

                        if (isDragging == false)
                        {
                            isDragging = true;
                            mousePosAtDragStart = mouseWorldPos;
                        }
                    }
                    else
                        isDragging = false;

                    if (isDragging)
                        cam.transform.position = mousePosAtDragStart - draggingDelta;
                }

                if (bordersEnabled)
                {
                    float properX = Mathf.Clamp(cam.transform.position.x, leftBotBorder.x, rightTopBorder.x);
                    float properY = Mathf.Clamp(cam.transform.position.y, leftBotBorder.y, rightTopBorder.y);
                    cam.transform.position = new Vector3(properX, properY, transform.position.z);
                }
            }

            if (EnableGrid)
            {
                if (!grid.IsShown())
                    grid.ShowGrid();

                grid.UpdateGrid();
            }
            else
            {
                if (grid.IsShown())
                    grid.HideGrid();
            }
        }

        private IEnumerator showScreenshotTakenPopupAfterItsTaken(string pathToScreenshot)
        {
            int filesCount = MapSaveLoadHelper.GetFilesCountInSavedScreenShotsFolder();
            for (int i = 0; i < 1000; ++i)
            {
                yield return new WaitForSeconds(0.1f);
                if (filesCount != MapSaveLoadHelper.GetFilesCountInSavedScreenShotsFolder())
                {
                    string infoText = "Path: " + pathToScreenshot;
                    UIController.GetInstance().MessagePanel.SetupAndShow("Screenshot taken", infoText, PopupTextType.Info);
                    break;
                }
            }
        }

        private void OnDrawGizmos()
        {
            if (!bordersEnabled)
                return;

            Gizmos.color = Color.blue;
            Gizmos.DrawLine(new Vector3(leftBotBorder.x, leftBotBorder.y),
                new Vector3(leftBotBorder.x, rightTopBorder.y));
            Gizmos.DrawLine(new Vector3(leftBotBorder.x, leftBotBorder.y),
                new Vector3(rightTopBorder.x, leftBotBorder.y));

            Gizmos.DrawLine(new Vector3(rightTopBorder.x, rightTopBorder.y),
                new Vector3(leftBotBorder.x, rightTopBorder.y));
            Gizmos.DrawLine(new Vector3(rightTopBorder.x, rightTopBorder.y),
                new Vector3(rightTopBorder.x, leftBotBorder.y));
        }
    }
}