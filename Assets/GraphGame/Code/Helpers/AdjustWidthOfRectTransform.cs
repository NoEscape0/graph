using System;
using UnityEngine;
using System.Collections;

public class AdjustWidthOfRectTransform : MonoBehaviour
{
    //object should be stretched Left
    public RectTransform Target;
    public float WidthXTimesLargerThenHeight = 1;

    void Start()
    {
        adjustRectTransformSize();
    }

    private void adjustRectTransformSize()
    {
        var rect = rectTransformToScreenSpace(Target);
        Vector2 size = new Vector2(rect.height * WidthXTimesLargerThenHeight, Target.sizeDelta.y);
        Target.sizeDelta = size;
    }

    private Rect rectTransformToScreenSpace(RectTransform transform)
    {
        Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
        return new Rect((Vector2)transform.position - (size * 0.5f), size);
    }
}
