using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading;
using UnityEngine.UI;
using ThreadPriority = System.Threading.ThreadPriority;

namespace GraphGame
{
    public class InGameLogger : MonoBehaviour
    {
        [SerializeField]
        private Text OutputText;

        private static InGameLogger instance;

        private int messagesCount;

        public void Log(string message)
        {
            Debug.Log(message);
            if (Constants.GetInstance().ShowLog)
                OutputText.text = string.Format("{0}: ", messagesCount) + message;
            messagesCount++;
        }

        public static InGameLogger GetInstance()
        {
            return instance;
        }
        
        private void Awake()
        {
            OutputText.text = "";
            messagesCount = 0;
            instance = this;
        }
    }
}
