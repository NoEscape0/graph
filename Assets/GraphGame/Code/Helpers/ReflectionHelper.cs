using System;
using UnityEngine;
using System.Collections;
using System.Linq.Expressions;
using System.Reflection;

namespace GraphGame
{
    public static class ReflectionHelper
    {
        /// <summary>
        /// usage: GetPropertyName(() => yourVariable)
        /// </summary>
        public static string GetPropertyName<T>(Expression<Func<T>> expr)
        {
            var body = ((MemberExpression) expr.Body);
            return body.Member.Name;
        }
    }
}