using System;
using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;
using System.Security;

namespace GraphGame.Models
{
    [Serializable]
    public class SerializebleVector2 : ISerializable
    {
        public Vector2 Vector;

        public SerializebleVector2(Vector2 vector)
        {
            Vector = vector;
        }

        public SerializebleVector2(float x, float y)
        {
            Vector = new Vector2(x, y);
        }

        public SerializebleVector2(SerializationInfo info, StreamingContext context)
        {
            float x, y;

            x = (float) info.GetValue(ReflectionHelper.GetPropertyName(() => Vector.x), Vector.x.GetType());
            y = (float) info.GetValue(ReflectionHelper.GetPropertyName(() => Vector.y), Vector.y.GetType());

            Vector = new Vector2(x, y);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(ReflectionHelper.GetPropertyName(() => Vector.x), Vector.x, Vector.x.GetType());
            info.AddValue(ReflectionHelper.GetPropertyName(() => Vector.y), Vector.y, Vector.y.GetType());
        }
    }
}