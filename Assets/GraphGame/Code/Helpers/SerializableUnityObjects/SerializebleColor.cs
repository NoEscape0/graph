using System;
using UnityEngine;
using System.Collections;

namespace GraphGame.Models
{
    [Serializable]
    public class SerializebleColor
    {
        public SerializebleColor()
        {
            r = g = b = a = 0;
        }

        public Color GetColor()
        {
            return new Color(r, g, b, a);
        }

        public void SetColor(Color color)
        {
            r = color.r;
            g = color.g;
            b = color.b;
            a = color.a;
        }

        private float r, g, b, a;
    }
}