using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using GraphGame.Models;

namespace GraphGame
{
    public static class MapSaveLoadHelper
    {
        private const string extention = ".txt";

        public static void Save(GameMap map, string name)
        {
            string fullPath = Constants.MapsAndScreenshotsPath + "/" + name + extention;
            Serialize(map, fullPath);
        }

        public static GameMap Load(string name)
        {
            string fullPath = Constants.MapsAndScreenshotsPath + "/" + name + extention;
            return DeserializeFromPath<GameMap>(fullPath);
        }

        public static GameMap Load(TextAsset mapAsset)
        {
            return DeserializeFromTextAsset<GameMap>(mapAsset);
        }

        public static int GetFilesCountInSavedScreenShotsFolder()
        {
            checkIfExternalFolderExist();
            return Directory.GetFiles(Constants.MapsAndScreenshotsPath).Length;
        }

        public static List<string> GetSavedMapsNamesList()
        {
            checkIfExternalFolderExist();

            string[] files = Directory.GetFiles(Constants.MapsAndScreenshotsPath);
            List<string> fileNames = new List<string>();

            foreach (string file in files)
            {
                var fileName = Path.GetFileName(file);
                if (Path.GetExtension(fileName) != extention)
                    continue;

                fileNames.Add(Path.GetFileNameWithoutExtension(fileName));
            }

            return fileNames;
        }

        public static void DeleteMap(string name)
        {
            string fullPath = Constants.MapsAndScreenshotsPath + "/" + name + extention;
            File.Delete(fullPath);
        }

        private static void checkIfExternalFolderExist()
        {
            if (Application.isEditor)
                return;

            Directory.CreateDirectory(Constants.MapsAndScreenshotsPath);
        }

        private static void Serialize(object obj, string path)
        {
            FileStream fs = new FileStream(path, FileMode.Create);
            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, obj);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }

        private static TData DeserializeFromPath<TData>(string path)
        {
            object outputFile;

            FileStream fs = new FileStream(path, FileMode.Open);
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                outputFile = formatter.Deserialize(fs);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }

            return (TData)outputFile;
        }

        private static TData DeserializeFromTextAsset<TData>(TextAsset asset)
        {
            Stream stream = new MemoryStream(asset.bytes);
            BinaryFormatter formatter = new BinaryFormatter();
            TData myInstance = (TData)formatter.Deserialize(stream);
            return myInstance;
        }
    }
}