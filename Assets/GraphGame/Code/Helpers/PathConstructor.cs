using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GraphGame.Models
{
    public class PathConstructor
    {
        public List<ArmyLocationPointBase> GenerateRoute(NodeBase start, NodeBase finish)
        {
            return generateRoute(start, finish);
        }

        public List<ArmyLocationPointBase> GenerateRoute(NodeBase start, NodeBase finish, List<KeyValuePair<NodeBase, int>> alreadyWaightedNodes)
        {
            return generateRoute(start, finish, alreadyWaightedNodes);
        }

        private List<ArmyLocationPointBase> generateRoute(NodeBase start, NodeBase finish, List<KeyValuePair<NodeBase, int>> alreadyWaightedNodes = null)
        {
            var weightedNodes = (alreadyWaightedNodes == null)? WeightAllNodesBasingOnHowFarTheyAreFrom(start) : alreadyWaightedNodes;

            // If we didnt set weight to the finish node it means that finish and start are not connected
            if (weightedNodes.Count(x => x.Key == finish) == 0)
                throw new RouteConstructionError();

            List<NodeBase> nodesPath = new List<NodeBase>();
            NodeBase currentNode = finish;
            while (true)
            {
                if (nodesPath.Contains(currentNode))
                    break; //Some error

                nodesPath.Add(currentNode);
                if (currentNode == start)
                    break;

                NodeBase nextNode = null;
                foreach (NodeBase node in currentNode.GetConnectedNodes())
                {
                    var nodeWeight = weightedNodes.Single(x => x.Key == node).Value;
                    float nextNodeWeight = float.MaxValue;

                    if (nextNode != null)
                        nextNodeWeight = weightedNodes.Single(x => x.Key == nextNode).Value;

                    if (nextNode == null || nodeWeight < nextNodeWeight)
                        nextNode = node;
                }

                currentNode = nextNode;
            }

            //insert links between nodes
            List<ArmyLocationPointBase> pathWithLinks = new List<ArmyLocationPointBase>();

            NodeBase previewsNode = null;
            foreach (var node in nodesPath)
            {
                if (previewsNode != null)
                    pathWithLinks.Add(previewsNode.GetLinks().Single(x => x.Connections.AsEnumerable().Contains(node)));
                pathWithLinks.Add(node);
                previewsNode = node;
            }

            List<ArmyLocationPointBase> normalOrder = new List<ArmyLocationPointBase>();
            foreach (var pwl in pathWithLinks)
            {
                normalOrder.Insert(0, pwl);
            }

            if (normalOrder.Count == 0)
                Debug.LogError("HERE3");
            return normalOrder;
        }

        public List<KeyValuePair<NodeBase, int>> WeightAllNodesBasingOnHowFarTheyAreFrom(NodeBase node)
        {
            var weightedNodes = new List<KeyValuePair<NodeBase, int>>();
            weightedNodes.Add(new KeyValuePair<NodeBase, int>(node, 0));

            bool performWave = true;
            // Wave
            while (performWave)
            {
                performWave = false;
                // If at least one node have weightless connection then we continue
                for (int i = 0; i < weightedNodes.Count; ++i)
                {
                    weightUnweightedNodes(weightedNodes[i].Key, weightedNodes);

                    if (isNodeConnectedWithWeightlessNode(weightedNodes[i].Key, weightedNodes))
                        performWave = true;
                }
            }

            return weightedNodes;
        }

        private bool isNodeConnectedWithWeightlessNode(NodeBase nodeWithConnections, List<KeyValuePair<NodeBase, int>> weightedNodes)
        {
            for (int i = 0; i < nodeWithConnections.GetConnectedNodes().Count; ++i)
            {
                if (weightedNodes.Count(x => x.Key == nodeWithConnections.GetConnectedNodes()[i]) == 0)
                    return true;
            }
            return false;
        }

        private void weightUnweightedNodes(NodeBase nodeWithConnections, List<KeyValuePair<NodeBase, int>> weightedNodes)
        {
            for (int i = 0; i < nodeWithConnections.GetConnectedNodes().Count; ++i)
            {
                NodeBase currentConnectedNode = nodeWithConnections.GetConnectedNodes()[i];
                if (weightedNodes.Count(x => x.Key == currentConnectedNode) == 0)
                {
                    int thisNodeWeight = weightedNodes.Single(x => x.Key == nodeWithConnections).Value;

                    var linkConnectingThem =
                        nodeWithConnections.GetLinks()
                            .Single(x => x.Connections.AsEnumerable().Contains(currentConnectedNode));
                    int nodeWeight = thisNodeWeight + linkConnectingThem.GetLinkLenght();
                    weightedNodes.Add(new KeyValuePair<NodeBase, int>(currentConnectedNode, nodeWeight));
                }
            }
        }
    }

    public class RouteConstructionError : Exception
    {
        public override string Message
        {
            get
            {
                return "Cant constuct a path";
            }
        }
    }
}